/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.fragment.dialog

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface.OnClickListener
import android.os.Bundle
import android.util.Log
import android.view.ContextThemeWrapper
import android.view.View
import android.view.Window
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.tandeminnovation.appbase.R

class ErrorDialogFragment : DialogFragment() {

    val closeButtonlistener: OnClickListener = OnClickListener { dialog, which ->
        try {

            dialog.dismiss()
        } catch (e: Exception) {

            Log.e(TAG, "closeButtonlistener", e)
        }
    }

    @SuppressLint("InflateParams")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val showTitle = arguments!!.getBoolean(SHOW_TITLE)
        val dialogThemeResx = arguments!!.getInt(DIALOG_THEME, android.R.style.Theme_DeviceDefault_Light_Dialog)

        val builder = AlertDialog.Builder(ContextThemeWrapper(this.activity, dialogThemeResx))
        builder.setCancelable(true)

        if (showTitle) {

            val titleResx = arguments!!.getInt(TITLE)
            builder.setTitle(titleResx)
        }

        val inflater = this.activity!!.layoutInflater
        var content: View?

        if (arguments!!.containsKey(LAYOUT)) {

            // custom layout
            val layoutResx = arguments!!.getInt(LAYOUT)
            content = inflater.inflate(layoutResx, null)
        } else {

            // error text layout
            val errorTextResx = arguments!!.getInt(ERROR_TEXT)
            content = inflater.inflate(R.layout.error_dialog, null)
            val errorText = content!!.findViewById<TextView>(R.id.error_text)
            errorText.setText(errorTextResx)
        }

        addBehavior(content)

        builder.setView(content)

        val closeButtonTextResx = arguments!!.getInt(CLOSE_BUTTON_TEXT)

        builder.setNeutralButton(closeButtonTextResx, closeButtonlistener)

        val dialog = builder.create()

        if (!showTitle) {

            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        }

        return dialog
    }

    fun addBehavior(view: View) {

    }

    companion object {

        private val TAG = "ErrorDialogFragment"

        val SHOW_TITLE = "showTitle"
        val TITLE = "title"
        val LAYOUT = "layout"
        val ERROR_TEXT = "error_text"
        val CLOSE_BUTTON_TEXT = "close_button_text"
        val DIALOG_THEME = "dialogTheme"

        fun newInstanceWithLayout(layoutResx: Int, showTitle: Boolean, titleResx: Int?, closeButtonTextResx: Int?, dialogThemeResx: Int?): ErrorDialogFragment {

            val frag = ErrorDialogFragment()

            val args = Bundle()
            args.putInt(LAYOUT, layoutResx)
            args.putBoolean(SHOW_TITLE, showTitle)

            if (showTitle) {

                args.putInt(TITLE, titleResx!!)
            }

            if (dialogThemeResx != null) {

                args.putInt(DIALOG_THEME, dialogThemeResx)
            }

            args.putInt(CLOSE_BUTTON_TEXT, closeButtonTextResx ?: R.string.hint_close)

            frag.arguments = args

            return frag
        }

        fun newInstanceWithText(errorTextResx: Int, showTitle: Boolean, titleResx: Int?, closeButtonTextResx: Int?, dialogThemeResx: Int?): ErrorDialogFragment {

            val frag = ErrorDialogFragment()

            val args = Bundle()
            args.putInt(ERROR_TEXT, errorTextResx)
            args.putBoolean(SHOW_TITLE, showTitle)

            if (showTitle) {

                args.putInt(TITLE, titleResx!!)
            }

            if (dialogThemeResx != null) {

                args.putInt(DIALOG_THEME, dialogThemeResx)
            }

            args.putInt(CLOSE_BUTTON_TEXT, closeButtonTextResx ?: R.string.hint_close)

            frag.arguments = args

            return frag
        }
    }
}
