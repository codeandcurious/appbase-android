/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.fragment.dialog

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import android.widget.DatePicker
import com.tandeminnovation.appbase.helper.AssetsHelper
import java.util.*


class DatePickerDialogFragment : androidx.fragment.app.DialogFragment(), DatePickerDialog.OnDateSetListener {

    private var onDateSet: OnDateSetListener? = null

    var date: Date? = null

    // getDatePicker() is only supported in api 11. only used to set font
    @SuppressLint("NewApi")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        // Use the current date as the default date in the picker
        val calendar = Calendar.getInstance()
        if (date != null) {

            calendar.time = date
        }

        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)

        // Create a new instance of DatePickerDialog and return it
        val dialog = DatePickerDialog(activity!!, this, year, month, day)

        // getDatePicker() is only supported in api 11. only used to set font. ignore in older versions
        AssetsHelper.getTypeFace(dialog.context, AssetsHelper.ROBOTO_LIGHT)?.let { AssetsHelper.setFont(dialog.datePicker, it) }
        AssetsHelper.getTypeFace(dialog.context, AssetsHelper.ROBOTO_LIGHT)?.let { AssetsHelper.setFont(dialog.getButton(Dialog.BUTTON_POSITIVE), it) }

        return dialog
    }

    override fun onDateSet(view: DatePicker, year: Int, month: Int, day: Int) {

        val c = Calendar.getInstance()
        c.set(year, month, day)

        this.date = c.time

        if (onDateSet != null) {

            onDateSet!!.onDateSet(this.date)
        }
    }

    fun setOnDateSet(onDateSet: OnDateSetListener) {

        this.onDateSet = onDateSet
    }

    interface OnDateSetListener {

        fun onDateSet(date: Date?)
    }
}

