/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.helper

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.tandeminnovation.appbase.base.EnumValueBase

/**
 * Created by firetrap on 29/11/2015.
 */
abstract class NavigationHelperBase {

    enum class NavigationMode(val value: Int) : EnumValueBase {

        Replace(1),
        Add(2),
        Dialog(3);
    }

    private lateinit var getContext: () -> Context

    fun setup(context: () -> Context) {
        this.getContext = context
    }

    fun navigateToApp(packageName: String) {
        val intent: Intent?
        val manager = getContext.invoke().packageManager
        try {

            intent = manager.getLaunchIntentForPackage(packageName)

            if (intent == null) {

                throw PackageManager.NameNotFoundException()
            }

            intent.addCategory(Intent.CATEGORY_LAUNCHER)
            getContext.invoke().startActivity(intent)
        } catch (e: PackageManager.NameNotFoundException) {

            nrError("Error when trying to navigate to main activity", TAG, e)
        }
    }

    fun navigateBackTo(appCompatActivity: AppCompatActivity, fragmentTag: String) {

        appCompatActivity.supportFragmentManager.popBackStack(fragmentTag, 0)
    }

    fun navigateBackToChild(fragment: Fragment, fragmentTag: String?) {

        fragment.childFragmentManager.popBackStack(fragmentTag, 0)
    }

    fun navigateBackToInclusive(appCompatActivity: AppCompatActivity, fragmentTag: String) {

        appCompatActivity.supportFragmentManager.popBackStack(fragmentTag, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }

    fun navigateBackToChildInclusive(fragment: Fragment, fragmentTag: String?) {

        fragment.childFragmentManager.popBackStack(fragmentTag, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }

    companion object {

        private const val TAG = "NavigationHelperBase"
    }
}
