/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.helper

import android.os.Bundle
import android.os.Parcelable
import java.text.ParseException
import java.util.*
import android.os.Parcel
import java.io.Serializable


/**
 * Created by Rui on 31/01/2016.
 */
object BundleHelper {

    /**
     * Has key boolean.
     *
     * @param item the item
     * @param key  the key
     * @return the boolean
     */
    fun hasKey(item: Bundle, key: String): Boolean {

        return item.containsKey(key)
    }

    /**
     * Gets string.
     *
     * @param item the item
     * @param key  the key
     * @return the string
     */
    fun getString(item: Bundle, key: String): String {
        var result: String? = null

        if (hasKey(item, key)) {

            result = item.getString(key)
        }

        return result ?: String()
    }

    /**
     * Gets nullable string.
     *
     * @param item the item
     * @param key  the key
     * @return the string
     */
    fun getNullableString(item: Bundle, key: String): String? {
        var result: String? = null

        if (hasKey(item, key)) {

            result = item.getString(key)
        }

        return result
    }

    /**
     * Gets double.
     *
     * @param item the item
     * @param key  the key
     * @return the double
     */
    fun getDouble(item: Bundle, key: String): Double {

        var value = 0.0

        if (hasKey(item, key)) {

            value = item.getDouble(key)
        }

        return value
    }

    /**
     * Gets float.
     *
     * @param item the item
     * @param key  the key
     * @return the float
     */
    fun getFloat(item: Bundle, key: String): Float {

        var value = 0f

        if (hasKey(item, key)) {

            value = item.getDouble(key).toFloat()
        }

        return value
    }

    /**
     * Gets boolean.
     *
     * @param item the item
     * @param key  the key
     * @return the boolean
     */
    fun getBoolean(item: Bundle, key: String): Boolean {

        var value = false

        if (hasKey(item, key)) {

            value = item.getBoolean(key)
        }

        return value
    }

    /**
     * Gets int.
     *
     * @param item the item
     * @param key  the key
     * @return the int
     */
    fun getInt(item: Bundle, key: String): Int {

        var value = 0

        if (hasKey(item, key)) {

            value = item.getInt(key)
        }

        return value
    }

    /**
     * Gets int list.
     *
     * @param item the item
     * @param key  the key
     * @return the int list
     */
    fun getIntList(item: Bundle, key: String): List<Int>? {

        var result: List<Int>? = null

        if (hasKey(item, key)) {

            result = item.getIntegerArrayList(key)
        }

        return result
    }

    /**
     * Gets nullable long list.
     *
     * @param item the item
     * @param key  the key
     * @return the nullable long list
     */
    fun getNullableLongList(item: Bundle, key: String): List<Long>? {

        var result: MutableList<Long>? = null

        if (hasKey(item, key)) {

            result = ArrayList()

            for (`val` in item.getLongArray(key)!!) {

                result.add(`val`)
            }
        }

        return result
    }

    /**
     * Gets long list.
     *
     * @param item the item
     * @param key  the key
     * @return the long list
     */
    fun getLongList(item: Bundle, key: String): List<Long>? {

        var result: MutableList<Long>? = null

        if (hasKey(item, key)) {

            result = ArrayList()

            for (`val` in item.getLongArray(key)!!) {

                result.add(`val`)
            }
        }

        return result
    }

    /**
     * Gets long.
     *
     * @param item the item
     * @param key  the key
     * @return the long
     */
    fun getLong(item: Bundle, key: String): Long {

        var value: Long = 0

        if (hasKey(item, key)) {

            value = item.getLong(key)
        }

        return value
    }

    /**
     * Get binary byte [ ].
     *
     * @param item the item
     * @param key  the key
     * @return the byte [ ]
     */
    fun getBinary(item: Bundle, key: String): ByteArray? {

        var value: ByteArray? = null

        if (hasKey(item, key)) {

            value = item.getByteArray(key)
        }

        return value
    }

    /**
     * Gets char.
     *
     * @param item the item
     * @param key  the key
     * @return the char
     */
    fun getChar(item: Bundle, key: String): Char {

        var value: Char = 0.toChar()

        if (hasKey(item, key)) {

            value = item.getString(key)!![0]
        }

        return value
    }

    /**
     * Gets uuid.
     *
     * @param item the item
     * @param key  the key
     * @return the uuid
     */
    fun getUUID(item: Bundle, key: String): UUID? {

        var value: UUID? = null

        if (hasKey(item, key)) {

            value = UUID.fromString(item.getString(key))
        }

        return value
    }

    /**
     * Gets string list.
     *
     * @param item the item
     * @param key  the key
     * @return the string list
     */
    fun getStringList(item: Bundle, key: String): List<String>? {
        var result: List<String>? = null
        if (hasKey(item, key)) {

            result = item.getStringArrayList(key)
        }

        return result
    }

    /**
     * Gets string list.
     *
     * @param item the item
     * @param key  the key
     * @return the string list
     */
    fun getDateList(item: Bundle, key: String): MutableList<Date>? {
        var result: MutableList<Date>? = null
        if (hasKey(item, key)) {

            val listItems = item.getStringArrayList(key)
            result = mutableListOf()
            listItems?.forEach {
                val date = DateHelper.defaultDateFormat.parse(it)
                result.add(date)
            }
        }

        return result
    }

    /**
     * Gets nullable int.
     *
     * @param item the item
     * @param key  the key
     * @return the nullable int
     */
    fun getNullableInt(item: Bundle, key: String): Int? {

        var result: Int? = null

        if (hasKey(item, key)) {

            result = item.getInt(key)
        }

        return result
    }

    /**
     * Gets nullable boolean.
     *
     * @param item the item
     * @param key  the key
     * @return the nullable boolean
     */
    fun getNullableBoolean(item: Bundle, key: String): Boolean? {

        var result: Boolean? = null

        if (hasKey(item, key)) {

            result = item.getBoolean(key)
        }

        return result
    }

    /**
     * Gets nullable long.
     *
     * @param item the item
     * @param key  the key
     * @return the nullable long
     */
    fun getNullableLong(item: Bundle, key: String): Long? {

        var result: Long? = null

        if (hasKey(item, key)) {

            result = item.getLong(key)
        }

        return result
    }

    /**
     * Gets nullable float.
     *
     * @param item the item
     * @param key  the key
     * @return the nullable float
     */
    fun getNullableFloat(item: Bundle, key: String): Float? {

        var result: Float? = null

        if (hasKey(item, key)) {

            result = item.getDouble(key).toFloat()
        }

        return result
    }

    /**
     * Gets nullable double.
     *
     * @param item the item
     * @param key  the key
     * @return the nullable double
     */
    fun getNullableDouble(item: Bundle, key: String): Double? {

        var result: Double? = null

        if (hasKey(item, key)) {

            result = item.getDouble(key)
        }

        return result
    }

    /**
     * Gets nullable char.
     *
     * @param item the item
     * @param key  the key
     * @return the nullable char
     */
    fun getNullableChar(item: Bundle, key: String): Char {

        var value: Char? = null

        if (hasKey(item, key)) {

            value = item.getString(key)!![0]
        }

        return value!!
    }

    /**
     * Gets date.
     *
     * @param item the item
     * @param key  the key
     * @return the date
     */
    fun getDate(item: Bundle, key: String): Date? {
        var result: Date? = null
        try {
            val dateStr = getString(item, key)
            if (!dateStr.isNullOrEmpty()) {

                result = DateHelper.defaultDateFormat.parse(dateStr)// DateHelper.parseDate(dateStr, DATE_FORMAT);
            }
        } catch (e: ParseException) {
        }

        return result
    }

    /**
     * Gets parcelable.
     *
     * @param <TParcelable> the type parameter
     * @param item          the item
     * @param key           the key
     * @return the parcelable
    </TParcelable> */
    fun <TParcelable : Parcelable> getParcelable(item: Bundle, key: String): TParcelable? {

        var result: TParcelable? = null

        if (hasKey(item, key)) {

            result = item.getParcelable(key)
        }

        return result
    }

    /**
     * Gets parcelable.
     *
     * @param <TParcelable> the type parameter
     * @param item          the item
     * @param key           the key
     * @return the parcelable
    </TParcelable> */
    fun getSerializable(item: Bundle, key: String): Serializable? {

        var result: Serializable? = null

        if (hasKey(item, key)) {

            result = item.getSerializable(key)
        }

        return result
    }

    /**
     * Put.
     *
     * @param bundle the bundle
     * @param key    the key
     * @param value  the value
     */
    fun put(bundle: Bundle, key: String, value: String?) {

        value?.let { bundle.putString(key, it) }
    }

    /**
     * Put.
     *
     * @param bundle the bundle
     * @param key    the key
     * @param value  the value
     */
    fun put(bundle: Bundle, key: String, value: Int?) {

        value?.let { bundle.putInt(key, it) }
    }

    /**
     * Put.
     *
     * @param bundle the bundle
     * @param key    the key
     * @param value  the value
     */
    fun put(bundle: Bundle, key: String, value: Int) {

        value?.let { bundle.putInt(key, it) }
    }

    /**
     * Put.
     *
     * @param bundle the bundle
     * @param key    the key
     * @param value  the value
     */
    fun put(bundle: Bundle, key: String, value: Long?) {

        value?.let { bundle.putLong(key, it) }
    }

    /**
     * Put.
     *
     * @param bundle the bundle
     * @param key    the key
     * @param value  the value
     */
    fun put(bundle: Bundle, key: String, value: Boolean?) {

        value?.let { bundle.putBoolean(key, it) }
    }

    /**
     * Put.
     *
     * @param bundle the bundle
     * @param key    the key
     * @param value  the value
     */
    fun put(bundle: Bundle, key: String, value: ByteArray?) {

        bundle.putByteArray(key, value)
    }

    /**
     * Put.
     *
     * @param bundle the bundle
     * @param key    the key
     * @param value  the value
     */
    fun put(bundle: Bundle, key: String, value: Char?) {

        value?.let { bundle.putChar(key, it) }
    }

    /**
     * Put.
     *
     * @param bundle the bundle
     * @param key    the key
     * @param value  the value
     */
    fun put(bundle: Bundle, key: String, value: Double?) {

        value?.let { bundle.putDouble(key, it) }
    }

    /**
     * Put.
     *
     * @param bundle the bundle
     * @param key    the key
     * @param value  the value
     */
    fun put(bundle: Bundle, key: String, value: UUID?) {

        value?.let { bundle.putString(key, it.toString()) }
    }

    /**
     * Put.
     *
     * @param bundle the bundle
     * @param key    the key
     * @param value  the value
     */
    fun put(bundle: Bundle, key: String, value: List<Long>?) {

        var values: LongArray? = null

        if (value != null) {

            values = LongArray(value.size)

            for (i in value.indices) {

                values[i] = value[i]
            }
        }

        bundle.putLongArray(key, values)
    }

    /**
     * PutDateList.
     *
     * @param bundle the bundle
     * @param key    the key
     * @param value  the value
     */
    fun putDateList(bundle: Bundle, key: String, value: List<Date>?) {
        var values: ArrayList<String>? = null
        if (value != null) {

            values = arrayListOf()
            value.forEach {

                val dateStr = DateHelper.defaultDateFormat.format(it)// DateHelper.formatDate(value, DATE_FORMAT);
                values.add(dateStr)
            }
        }

        bundle.putStringArrayList(key, values)
    }

    /**
     * PutStringList.
     *
     * @param bundle the bundle
     * @param key    the key
     * @param value  the value
     */
    fun putStringList(bundle: Bundle, key: String, value: List<String>?) {
        var values: ArrayList<String>? = null
        if (value != null) {

            values = ArrayList(value)
        }

        bundle.putStringArrayList(key, values)
    }

    /**
     * Put.
     *
     * @param bundle the bundle
     * @param key    the key
     * @param value  the value
     */
    fun put(bundle: Bundle, key: String, value: Float?) {

        value?.let { bundle.putFloat(key, it) }

    }

    /**
     * Put.
     *
     * @param <TParcelable> the type parameter
     * @param bundle        the bundle
     * @param key           the key
     * @param value         the value
    </TParcelable> */
    fun <TParcelable : Parcelable> put(bundle: Bundle, key: String, value: Parcelable?) {

        bundle.putParcelable(key, value)
    }

    fun getBundleSizeInBytes(bundle: Bundle): Int {
        val parcel = Parcel.obtain()
        val size: Int

        parcel.writeBundle(bundle)
        size = parcel.dataSize()
        parcel.recycle()

        return size
    }

    /**
     * Put.
     *
     * @param bundle the bundle
     * @param key    the key
     * @param value  the value
     */
    fun put(bundle: Bundle, key: String, value: Serializable?) {

        value?.let { bundle.putSerializable(key, it) }
    }
}