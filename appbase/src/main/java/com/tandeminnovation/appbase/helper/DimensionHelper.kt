/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.helper

import android.content.Context
import android.content.res.Resources
import android.util.DisplayMetrics
import androidx.annotation.Dimension

/**
 * The type Dimension helper.
 */
object DimensionHelper {

    /**
     * Dp to px int.
     *
     * @param context the context
     * @param dp      the dp
     * @param xAxis   the x axis
     * @return the int
     */
    fun dpToPx(context: Context, dp: Int, xAxis: Boolean): Int {

        val displayMetrics = context.resources.displayMetrics
        return Math.round(dp * ((if (xAxis) displayMetrics.xdpi else displayMetrics.ydpi) / DisplayMetrics.DENSITY_DEFAULT))
    }

    /**
     * Px to dp int.
     *
     * @param context the context
     * @param px      the px
     * @param xAxis   the x axis
     * @return the int
     */
    fun pxToDp(context: Context, px: Int, xAxis: Boolean): Int {

        val displayMetrics = context.resources.displayMetrics
        return Math.round(px / ((if (xAxis) displayMetrics.xdpi else displayMetrics.ydpi) / DisplayMetrics.DENSITY_DEFAULT))
    }


}

@JvmOverloads @Dimension(unit = Dimension.PX) fun Number.dpToPx(metrics: DisplayMetrics = Resources.getSystem().displayMetrics): Float {
    return toFloat() * metrics.density
}

@JvmOverloads @Dimension(unit = Dimension.PX) fun Number.dp(metrics: DisplayMetrics = Resources.getSystem().displayMetrics): Int {
    return (toInt() * metrics.density).toInt()
}

@JvmOverloads @Dimension(unit = Dimension.SP) fun Number.spToPx(metrics: DisplayMetrics = Resources.getSystem().displayMetrics): Float {
    return toFloat() * metrics.scaledDensity
}