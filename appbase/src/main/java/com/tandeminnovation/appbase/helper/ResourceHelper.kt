/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.helper

import android.content.Context
import android.content.res.ColorStateList
import android.content.res.Resources
import android.graphics.drawable.Drawable
import android.os.Build
import android.util.TypedValue
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.annotation.IntegerRes
import androidx.annotation.Nullable
import com.tandeminnovation.appbase.R

/**
 * The type Resource helper.
 */


fun getText(context: Context, stringResx: Int): String {

    return context.resources.getText(stringResx) as String
}

@Suppress("DEPRECATION")
@ColorInt
fun getColor(context: Context, @ColorRes colorResx: Int, @Nullable theme: Resources.Theme? = null): Int {
    return when {
        Build.VERSION.SDK_INT >= Build.VERSION_CODES.M -> context.resources.getColor(colorResx, theme)
        else -> context.resources.getColor(colorResx)
    }
}

fun getInteger(context: Context, @IntegerRes intResx: Int): Int {

    return context.resources.getInteger(intResx)
}

fun getThemeAccentColor(context: Context): Int {
    val value = TypedValue()
    context.theme.resolveAttribute(R.attr.colorAccent, value, true)

    return value.data
}

fun getThemePrimaryDarkColor(context: Context): Int {
    val value = TypedValue()
    context.theme.resolveAttribute(R.attr.colorPrimaryDark, value, true)

    return value.data
}

fun getThemePrimaryColor(context: Context): Int {
    val value = TypedValue()
    context.theme.resolveAttribute(R.attr.colorPrimary, value, true)

    return value.data
}

fun getThemeBackgroundColor(context: Context): Int {
    val value = TypedValue()
    context.theme.resolveAttribute(R.attr.background, value, true)

    return value.data
}

@Suppress("DEPRECATION")
fun getColorSelector(context: Context, @ColorRes colorStateResx: Int, @Nullable theme: Resources.Theme?): ColorStateList {
    return when {
        Build.VERSION.SDK_INT >= Build.VERSION_CODES.M -> context.resources.getColorStateList(colorStateResx, theme)
        else -> return context.resources.getColorStateList(colorStateResx)
    }
}

@Suppress("DEPRECATION")
fun getDrawable(context: Context, drawableResx: Int, @Nullable theme: Resources.Theme? = null): Drawable {
    return when {
        Build.VERSION.SDK_INT >= Build.VERSION_CODES.M -> context.resources.getDrawable(drawableResx, theme)
        else -> context.resources.getDrawable(drawableResx)
    }
}

fun getDimension(context: Context, dimenResx: Int): Float {

    return context.resources.getDimension(dimenResx)
}
