/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.helper

import android.util.Base64
import com.tandeminnovation.appbase.extension.whenNotNull
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.lang.IllegalArgumentException
import java.text.ParseException
import java.util.*
import kotlin.reflect.KClass

/**
 * The type Json helper.
 */
object JsonHelper {

    const val TAG = "JsonHelper"

    /**
     * Has key boolean.
     *
     * @param jsonObject the jsonObject
     * @param key  the key
     * @return the boolean
     */
    fun hasKey(jsonObject: JSONObject, key: String): Boolean {

        return jsonObject.has(key) && !jsonObject.isNull(key)
    }

    fun parseString(jsonObject: JSONObject, key: String): String {
        var result = String()
        try {

            if (hasKey(jsonObject, key)) {

                result = jsonObject.getString(key)
            }
        } catch (e: JSONException) {

            nrError("Error parsing $key", TAG, e)
        }

        return result
    }

    fun parseNullableString(jsonObject: JSONObject, key: String): String? {
        var result: String? = null
        try {

            if (hasKey(jsonObject, key)) {

                result = jsonObject.getString(key)
            }
        } catch (e: JSONException) {

            nrError("Error parsing $key", TAG, e)
        }

        return result
    }

    /**
     * Parse enum t.
     *
     * @param <T>      the type parameter
     * @param jsonObject     the jsonObject
     * @param key      the key
     * @return the t</T>
     * */
    inline fun <reified T : Enum<T>> parseEnum(jsonObject: JSONObject, key: String): T? {
        var result: T? = null
        try {

            if (hasKey(jsonObject, key)) {

                result = enumValueOf<T>(jsonObject.getString(key))
            }
        } catch (e: JSONException) {

            nrError("Error parsing $key", TAG, e)
        } catch (e: IllegalArgumentException) {
            nrError("Value does not exist $key", TAG, e)
        }

        return result
    }

    inline fun <reified T : Enum<T>> parseStringEnum(jsonObject: JSONObject, key: String): T? {
        var result: T? = null
        try {

            if (hasKey(jsonObject, key)) {

                for (t: Enum<T> in enumValues<T>()) {

                    if (t.toString().equals(jsonObject.getString(key), false)) {

                        result = enumValueOf<T>(t.name)
                        break
                    }
                }
            }
        } catch (e: JSONException) {

            nrError("Error parsing $key", TAG, e)
        }

        return result
    }

    inline fun <reified T : Enum<T>> parseEnumList(jsonObject: JSONObject, key: String): List<T>? {
        val stringList = parseStringList(jsonObject, key)
        var result: MutableList<T>? = null
        if (stringList != null) {

            result = ArrayList()
            for (s in stringList) {

                try {

                    if (hasKey(jsonObject, key)) {

                        val input = parseStringEnum<T>(jsonObject, key)
                        whenNotNull(input) { t ->

                            result.add(t)
                        }
                    }
                } catch (e: JSONException) {

                    nrError("Error parsing $key", TAG, e)
                }
            }
        }

        return result
    }

    inline fun <reified T : Enum<T>> parseStringEnumList(jsonObject: JSONObject, key: String): List<T>? {
        val stringList = parseStringList(jsonObject, key)
        var result: MutableList<T>? = null
        if (stringList != null) {

            result = ArrayList()
            for (s in stringList) {

                try {
                    for (t: Enum<T> in enumValues<T>()) {

                        if (t.toString().equals(s, false)) {

                            result.add(enumValueOf(t.name))
                        }
                    }

                } catch (e: JSONException) {

                    nrError("Error parsing $key", TAG, e)
                }
            }
        }

        return result
    }

    fun getEnumValues(enumClass: KClass<out Enum<*>>): Array<out Enum<*>> = enumClass.java.enumConstants as Array<out Enum<*>>

    /**
     * Parse double double.
     *
     * @param jsonObject the jsonObject
     * @param key  the key
     * @return the double
     */
    fun parseDouble(jsonObject: JSONObject, key: String): Double {

        var value = 0.0

        try {

            if (hasKey(jsonObject, key)) {

                value = jsonObject.getDouble(key)
            }
        } catch (e: JSONException) {

            nrError("Error parsing $key", TAG, e)
        }

        return value
    }

    /**
     * Parse float float.
     *
     * @param jsonObject the jsonObject
     * @param key  the key
     * @return the float
     */
    fun parseFloat(jsonObject: JSONObject, key: String): Float {

        var value = 0f

        try {

            if (hasKey(jsonObject, key)) {

                value = jsonObject.getDouble(key).toFloat()
            }
        } catch (e: JSONException) {

            nrError("Error parsing $key", TAG, e)
        }

        return value
    }

    /**
     * Parse boolean boolean.
     *
     * @param jsonObject the jsonObject
     * @param key  the key
     * @return the boolean
     */
    fun parseBoolean(jsonObject: JSONObject, key: String): Boolean {

        var value = false

        try {

            if (hasKey(jsonObject, key)) {

                value = jsonObject.getBoolean(key)
            }
        } catch (e: JSONException) {

            nrError("Error parsing $key", TAG, e)
        }

        return value
    }

    /**
     * Parse int int.
     *
     * @param jsonObject the jsonObject
     * @param key  the key
     * @return the int
     */
    fun parseInt(jsonObject: JSONObject, key: String): Int {

        var value = 0

        try {

            if (hasKey(jsonObject, key)) {

                value = jsonObject.getInt(key)
            }
        } catch (e: JSONException) {

            nrError("Error parsing $key", TAG, e)
        }

        return value
    }

    /**
     * Parse int list list.
     *
     * @param jsonObject the jsonObject
     * @param key  the key
     * @return the list
     */
    fun parseIntList(jsonObject: JSONObject, key: String): List<Int>? {
        var result: MutableList<Int>? = null
        try {

            if (hasKey(jsonObject, key)) {

                result = ArrayList()

                val jsonList = jsonObject.getJSONArray(key)

                for (i in 0 until jsonList.length()) {

                    result.add(jsonList.getInt(i))
                }
            }
        } catch (e: JSONException) {

            nrError("Error parsing $key", TAG, e)
        }

        return result
    }

    /**
     * Parse nullable long list list.
     *
     * @param jsonObject the jsonObject
     * @param key  the key
     * @return the list
     */
    fun parseNullableLongList(jsonObject: JSONObject, key: String): List<Long>? {

        var result: MutableList<Long>? = null

        try {

            if (hasKey(jsonObject, key)) {

                result = ArrayList()

                val jsonList = jsonObject.getJSONArray(key)

                for (i in 0 until jsonList.length()) {

                    result.add(jsonList.getLong(i))
                }
            }
        } catch (e: JSONException) {

            nrError("Error parsing $key", TAG, e)
        }

        return result
    }

    /**
     * Parse long list list.
     *
     * @param jsonObject the jsonObject
     * @param key  the key
     * @return the list
     */
    fun parseLongList(jsonObject: JSONObject, key: String): List<Long>? {

        var result: MutableList<Long>? = null

        try {

            if (hasKey(jsonObject, key)) {

                result = ArrayList()

                val jsonList = jsonObject.getJSONArray(key)

                for (i in 0 until jsonList.length()) {

                    result.add(jsonList.getLong(i))
                }
            }
        } catch (e: JSONException) {

            nrError("Error parsing $key", TAG, e)
        }

        return result
    }

    /**
     * Parse long long.
     *
     * @param jsonObject the jsonObject
     * @param key  the key
     * @return the long
     */
    fun parseLong(jsonObject: JSONObject, key: String): Long {

        var value: Long = 0

        try {

            if (hasKey(jsonObject, key)) {

                value = jsonObject.getLong(key)
            }
        } catch (e: JSONException) {

            nrError("Error parsing $key", TAG, e)
        }

        return value
    }

    /**
     * Parse binary byte [ ].
     *
     * @param jsonObject the jsonObject
     * @param key  the key
     * @return the byte [ ]
     */
    fun parseBinary(jsonObject: JSONObject, key: String): ByteArray? {

        var value: ByteArray? = null

        try {

            if (hasKey(jsonObject, key)) {

                val valueBase64 = jsonObject.getString(key)
                value = Base64.decode(valueBase64, Base64.DEFAULT)
            }
        } catch (e: JSONException) {

            nrError("Error parsing $key", TAG, e)
        }

        return value
    }

    /**
     * Parse char char.
     *
     * @param jsonObject the jsonObject
     * @param key  the key
     * @return the char
     */
    fun parseChar(jsonObject: JSONObject, key: String): Char {

        var value: Char = 0.toChar()

        try {

            if (hasKey(jsonObject, key)) {

                value = jsonObject.getString(key)[0]
            }
        } catch (e: JSONException) {

            nrError("Error parsing $key", TAG, e)
        }

        return value
    }

    /**
     * Parse uuid uuid.
     *
     * @param jsonObject the jsonObject
     * @param key  the key
     * @return the uuid
     */
    fun parseUUID(jsonObject: JSONObject, key: String): UUID? {

        var value: UUID? = null

        try {

            if (hasKey(jsonObject, key)) {

                value = UUID.fromString(jsonObject.getString(key))
            }
        } catch (e: JSONException) {

            nrError("Error parsing $key", TAG, e)
        }

        return value
    }

    /**
     * Parse string list list.
     *
     * @param jsonObject the jsonObject
     * @param key  the key
     * @return the list
     */
    fun parseStringList(jsonObject: JSONObject, key: String): List<String>? {
        var result: MutableList<String>? = null
        try {

            if (hasKey(jsonObject, key)) {

                result = ArrayList()

                val jsonList = jsonObject.getJSONArray(key)

                for (i in 0 until jsonList.length()) {

                    result.add(jsonList.getString(i))
                }
            }
        } catch (e: JSONException) {

            nrError("Error parsing $key", TAG, e)
        }

        return result
    }

    /**
     * Parse nullable int integer.
     *
     * @param jsonObject the jsonObject
     * @param key  the key
     * @return the integer
     */
    fun parseNullableInt(jsonObject: JSONObject, key: String): Int? {

        var result: Int? = null

        try {

            if (hasKey(jsonObject, key)) {

                result = jsonObject.getInt(key)
            }
        } catch (ex: JSONException) {

            nrError("Error parsing $key", TAG, ex)
        }

        return result
    }

    /**
     * Parse nullable boolean boolean.
     *
     * @param jsonObject the jsonObject
     * @param key  the key
     * @return the boolean
     */
    fun parseNullableBoolean(jsonObject: JSONObject, key: String): Boolean? {

        var result: Boolean? = null

        try {

            if (hasKey(jsonObject, key)) {

                result = jsonObject.getBoolean(key)
            }
        } catch (e: JSONException) {

            nrError("Error parsing $key", TAG, e)
        }

        return result
    }

    /**
     * Parse nullable long long.
     *
     * @param jsonObject the jsonObject
     * @param key  the key
     * @return the long
     */
    fun parseNullableLong(jsonObject: JSONObject, key: String): Long? {

        var result: Long? = null

        try {

            if (hasKey(jsonObject, key)) {

                result = jsonObject.getLong(key)
            }
        } catch (ex: JSONException) {

            nrError("Error parsing $key", TAG, ex)
        }

        return result
    }

    /**
     * Parse nullable float float.
     *
     * @param jsonObject the jsonObject
     * @param key  the key
     * @return the float
     */
    fun parseNullableFloat(jsonObject: JSONObject, key: String): Float? {
        var result: Float? = null
        try {

            if (hasKey(jsonObject, key)) {

                result = jsonObject.getDouble(key).toFloat()
            }
        } catch (e: JSONException) {

            nrError("Error parsing $key", TAG, e)
        }

        return result
    }

    /**
     * Parse nullable double double.
     *
     * @param jsonObject the jsonObject
     * @param key  the key
     * @return the double
     */
    fun parseNullableDouble(jsonObject: JSONObject, key: String): Double? {
        var result: Double? = null
        try {

            if (hasKey(jsonObject, key)) {

                result = jsonObject.getDouble(key)
            }
        } catch (ex: JSONException) {

            nrError("Error parsing $key", TAG, ex)
        }

        return result
    }

    /**
     * Parse nullable char char.
     *
     * @param jsonObject the jsonObject
     * @param key  the key
     * @return the char
     */
    fun parseNullableChar(jsonObject: JSONObject, key: String): Char {
        var value: Char? = null
        try {

            if (hasKey(jsonObject, key)) {

                value = jsonObject.getString(key)[0]
            }
        } catch (e: JSONException) {

            nrError("Error parsing $key", TAG, e)
        }

        return value!!
    }

    /**
     * Parse date date.
     *
     * @param jsonObject the jsonObject
     * @param key  the key
     * @return the date
     */
    fun parseDate(jsonObject: JSONObject, key: String): Date {
        var result = Date()
        try {

            val dateStr = parseString(jsonObject, key)
            if (dateStr.isNotEmpty()) {

                result = DateHelper.defaultDateFormat.parse(dateStr)
            }
        } catch (e: ParseException) {

            nrError("Error parsing $key", TAG, e)
        }

        return result
    }

    /**
     * Parse date date.
     *
     * @param jsonObject   the jsonObject
     * @param key    the key
     * @param format the format
     * @return the date
     */
    fun parseDate(jsonObject: JSONObject, key: String, format: String): Date {
        var result = Date()
        try {
            val dateStr = parseString(jsonObject, key)
            if (dateStr.isNotEmpty()) {

                result = DateHelper.parseDate(dateStr, format)
            }
        } catch (e: ParseException) {

            nrError("Error parsing $key", TAG, e)
        }

        return result
    }

    /**
     * Parse date date.
     *
     * @param jsonObject the jsonObject
     * @param key  the key
     * @return the date
     */
    fun parseNullableDate(jsonObject: JSONObject, key: String): Date? {
        var result: Date? = null
        try {

            val dateStr = parseString(jsonObject, key)
            if (dateStr.isNotEmpty()) {

                result = DateHelper.defaultDateFormat.parse(dateStr)
            }
        } catch (e: ParseException) {

            nrError("Error parsing $key", TAG, e)
        }

        return result
    }

    /**
     * Parse date date.
     *
     * @param jsonObject   the jsonObject
     * @param key    the key
     * @param format the format
     * @return the date
     */
    fun parseNullableDate(jsonObject: JSONObject, key: String, format: String): Date? {
        var result: Date? = null
        try {
            val dateStr = parseString(jsonObject, key)
            if (dateStr.isNotEmpty()) {

                result = DateHelper.parseDate(dateStr, format)
            }
        } catch (e: ParseException) {

            nrError("Error parsing $key", TAG, e)
        }

        return result
    }

    /**
     * Parse date date.
     *
     * @param jsonObject   the jsonObject
     * @param key    the key
     * @param format the format
     * @return the date
     */
    fun parseNullableDate(jsonObject: JSONObject, key: String, format: String, locale: Locale): Date? {
        var result: Date? = null
        try {
            val dateStr = parseString(jsonObject, key)
            if (dateStr.isNotEmpty()) {

                result = DateHelper.parseDate(dateStr, format, locale)
            }
        } catch (e: ParseException) {

            nrError("Error parsing $key", TAG, e)
        }

        return result
    }

    /**
     * Format string.
     *
     * @param value the value
     * @return the string
     */
    fun format(value: String): String {

        return value
    }

    /**
     * Format long.
     *
     * @param value the value
     * @return the long
     */
    fun format(value: Long): Long {

        return value
    }

    /**
     * Format boolean.
     *
     * @param value the value
     * @return the boolean
     */
    fun format(value: Boolean): Boolean {

        return value
    }

    /**
     * Format string.
     *
     * @param value the value
     * @return the string
     */
    fun format(value: ByteArray): String {

        return Base64.encodeToString(value, Base64.DEFAULT)
    }

    /**
     * Format char.
     *
     * @param value the value
     * @return the char
     */
    fun format(value: Char): Char {

        return value
    }

    /**
     * Format string.
     *
     * @param <T>   the type parameter
     * @param value the value
     * @return the string
    </T> */
    fun <T : Enum<T>> format(value: Enum<T>): String {

        return value.name
    }

    fun <T : Enum<T>> formatStringEnum(value: Enum<T>): String {

        return value.toString()
    }

    /**
     * Format integer.
     *
     * @param value the value
     * @return the int
     */
    fun format(value: Int?): Int? {

        return value
    }

    /**
     * Format double.
     *
     * @param value the value
     * @return the double
     */
    fun format(value: Double?): Double? {

        return value
    }

    /**
     * Format string.
     *
     * @param value the value
     * @return the string
     */
    fun format(value: Date?): String? {
        var result: String? = null
        if (value != null) {

            result = DateHelper.defaultDateFormat.format(value)
        }

        return result
    }

    /**
     * Format string.
     *
     * @param value  the value
     * @param format the format
     * @return the string
     */
    fun format(value: Date?, format: String): String? {
        var result: String? = null
        if (value != null) {

            result = DateHelper.formatDate(value, format)
        }

        return result
    }

    /**
     * Format string.
     *
     * @param value the value
     * @return the string
     */
    fun format(value: UUID): String {

        return value.toString()
    }

    /**
     * Format json array.
     *
     * @param value the value
     * @return the json array
     */
    fun format(value: List<Long>): JSONArray {

        return JSONArray(value)
    }

    /**
     * Format float.
     *
     * @param value the value
     * @return the float
     */
    fun format(value: Float?): Float? {

        return value
    }

    /**
     * To json array json array.
     *
     * @param ids the ids
     * @return the json array
     */
    fun toJsonArray(ids: List<Int>): JSONArray {
        val jsonList = JSONArray()
        for (i in ids.indices) {

            jsonList.put(ids[i])
        }

        return jsonList
    }

    fun isJsonValid(json: String): Boolean {
        try {

            JSONObject(json)
        } catch (ex: JSONException) {

            try {

                JSONArray(json)
            } catch (ex1: JSONException) {

                return false
            }
        }

        return true
    }

}