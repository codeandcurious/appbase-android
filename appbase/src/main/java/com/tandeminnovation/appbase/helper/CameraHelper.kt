/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.helper

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.provider.MediaStore
import java.io.File
import java.io.IOException

/**
 * The type Camera helper.
 */
object CameraHelper {

    private const val REQUEST_IMAGE_CAPTURE = 1888
    private const val REQUEST_TAKE_PHOTO = 1889

    /**
     * Has camera boolean.
     *
     * @param context the context
     * @return the boolean
     */
    fun hasCamera(context: Context): Boolean {

        return context.packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)
    }

    /**
     * Has flash boolean.
     *
     * @param context the context
     * @return the boolean
     */
    fun hasFlash(context: Context): Boolean {

        return context.packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)
    }

    /**
     * Take picture file.
     *
     * @param activity     the activity
     * @param fullsize     the fullsize
     * @param publicFolder the public folder
     * @return the file
     */
    fun takePicture(activity: Activity, fullsize: Boolean, publicFolder: Boolean): File? {

        // Create the File where the photo should go on full size photo
        var photoFile: File? = null

        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(activity.packageManager) != null) {

            if (fullsize) {

                try {

                    photoFile = FileHelper.createImageTempFile(activity, publicFolder)
                } catch (ex: IOException) {
                    // ApiError occurred while creating the File
                }

                // Continue only if the File was successfully created
                if (photoFile != null) {

                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile))
                    activity.startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO)
                }
            } else {

                activity.startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }

        return photoFile
    }

    /**
     * Take picture thumbnail.
     *
     * @param activity the activity
     */
    fun takePictureThumbnail(activity: Activity) {

        takePicture(activity, false, false)
    }

    /**
     * Take picture full size file.
     *
     * @param activity     the activity
     * @param publicFolder the public folder
     * @return the file
     */
    fun takePictureFullSize(activity: Activity, publicFolder: Boolean): File? {

        return takePicture(activity, true, publicFolder)
    }

    /**
     * On activity result get picture thumbnail bitmap.
     *
     * @param requestCode the request code
     * @param resultCode  the result code
     * @param data        the data
     * @return the bitmap
     */
    fun onActivityResultGetPictureThumbnail(requestCode: Int, resultCode: Int, data: Intent): Bitmap? {

        var result: Bitmap? = null

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {

            val extras = data.extras
            result = extras!!.get("data") as Bitmap
        } else if (requestCode == REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {

            val extras = data.extras
            result = extras!!.get("data") as Bitmap
        }

        return result
    }

    /**
     * On activity result was picture successfull boolean.
     *
     * @param requestCode the request code
     * @param resultCode  the result code
     * @param data        the data
     * @return the boolean
     */
    fun onActivityResultWasPictureSuccessfull(requestCode: Int, resultCode: Int, data: Intent): Boolean {

        var result = false

        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {

            result = true
        }

        return result
    }
}
