/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.helper

import android.content.Context
import android.content.res.Resources
import android.graphics.Color
import android.graphics.drawable.Drawable
import androidx.annotation.Nullable
import androidx.core.graphics.drawable.DrawableCompat

/**
 * The type Color helper.
 */
object ColorHelper {

    /**
     * Parse int.
     *
     * @param color the color
     * @return the int
     */
    fun parse(color: String): Int {

        return Color.parseColor(color)
    }

    /**
     * Tint drawable drawable.
     *
     * @param context   the context
     * @param drawable  the drawable
     * @param colorResX the color res x
     * @return the drawable
     */
    fun tintDrawable(context: Context, drawable: Drawable, colorResX: Int, @Nullable theme: Resources.Theme? = null): Drawable {
        var result: Drawable = drawable
        result = DrawableCompat.wrap(result)
        DrawableCompat.setTint(result.mutate(), getColor(context, colorResX, theme))

        return result
    }
}
