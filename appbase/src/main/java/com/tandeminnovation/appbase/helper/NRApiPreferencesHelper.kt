/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.helper

import android.content.Context
import java.util.*

/**
 * The type Nrapi preferences helper.
 */
class NRApiPreferencesHelper : PreferencesHelper() {
    companion object {

        private val DEVICE_ID = "NR_Device_Id"

        /**
         * Gets device id.
         *
         * @param cont the cont
         * @return the device id
         */
        fun getDeviceId(cont: Context): String? {
            var deviceId = PreferencesHelper.getPreferences(cont, DEVICE_ID, "")
            if (deviceId.isNullOrEmpty()) {

                deviceId = UUID.randomUUID().toString()
                PreferencesHelper.setPreferences(cont, DEVICE_ID, deviceId)
            }

            return deviceId
        }
    }
}
