/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.helper

import android.annotation.SuppressLint
import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Point
import android.location.LocationManager
import android.net.wifi.WifiManager
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import android.provider.Settings
import android.telephony.TelephonyManager
import android.view.WindowManager
import androidx.core.hardware.fingerprint.FingerprintManagerCompat
import com.tandeminnovation.appbase.extension.whenNotNull
import java.io.File
import java.io.FileFilter
import java.util.regex.Pattern

/**
 * The type Device helper.
 */
object DeviceHelper {

    private var numCores: Int? = null
    private var screenSize: Point? = null

    /**
     * The constant CPU_ARCHITECTURE_ARM.
     */
    var CPU_ARCHITECTURE_ARM = 0
    /**
     * The constant CPU_ARCHITECTURE_X86.
     */
    var CPU_ARCHITECTURE_X86 = 1
    /**
     * The constant CPU_ARCHITECTURE_X86_X64.
     */
    var CPU_ARCHITECTURE_X86_X64 = 2
    /**
     * The constant CPU_ARCHITECTURE_MIPS.
     */
    var CPU_ARCHITECTURE_MIPS = 3
    /**
     * The constant CPU_ARCHITECTURE_MIPS_64.
     */
    var CPU_ARCHITECTURE_MIPS_64 = 4

    /**
     * Gets device maker.
     *
     * @return the device maker
     */
    val deviceMaker: String
        get() = Build.BRAND

    /** A build ID string meant for displaying to the user */
    val displayId: String
        get() = Build.DISPLAY

    /**
     * Gets device model.
     *
     * @return the device model
     */
    val deviceModel: String
        get() = Build.MODEL

    /**
     * Gets device model version.
     *
     * @return the device model version
     */
    val deviceModelVersion: String
        get() = Build.DEVICE

    /**
     * Gets platform version.
     *
     * @return the platform version
     */
    val platformVersion: String
        get() = Build.VERSION.RELEASE

    /**
     * Gets cpu architecture.
     *
     * @return the cpu architecture
     */
    val cpuArchitecture: Int
        get() {

            val cpuArch = Build.CPU_ABI2

            return if (cpuArch.contains("arm")) {

                CPU_ARCHITECTURE_ARM
            } else if (cpuArch.equals("x86", ignoreCase = true)) {

                CPU_ARCHITECTURE_X86
            } else if (cpuArch.equals("x86_64", ignoreCase = true)) {

                CPU_ARCHITECTURE_X86_X64
            } else if (cpuArch.equals("mips", ignoreCase = true)) {

                CPU_ARCHITECTURE_MIPS
            } else {

                CPU_ARCHITECTURE_MIPS_64
            }

        }

    /**
     * Gets the number of cores available in this device, across all processors.
     * Requires: Ability to peruse the filesystem at "/sys/devices/system/cpu"
     *
     * @return The number of cores, or 1 if failed to get result
     */
    fun getNumCores(): Int {

        if (numCores != null) {

            return numCores!!
        }

        // Private Class to display only CPU devices in the directory listing
        class CpuFilter : FileFilter {

            override fun accept(pathname: File): Boolean {

                // Check if filename is "cpu", followed by a single digit number
                return Pattern.matches("cpu[0-9]+", pathname.name)
            }
        }

        try {
            // Get directory containing CPU info
            val dir = File("/sys/devices/system/cpu/")
            // Filter to only list the devices we care about
            val files = dir.listFiles(CpuFilter())
            // Return the number of cores (virtual CPU devices)
            numCores = files.size
        } catch (e: Exception) {
            // Default to return 1 core
            numCores = 1
        }

        return numCores!!
    }

    /**
     * Gets device id.
     *
     * @param context the context
     * @return the device id
     */
    @SuppressLint("HardwareIds")
    fun deviceId(context: Context): String {

        return Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
    }

    @SuppressLint("MissingPermission", "HardwareIds")
    private fun bluetoothMacAddress(): String {
        var result = String()
        whenNotNull(BluetoothAdapter.getDefaultAdapter()) {

            result = it.address
        }
        return result
    }

    @SuppressLint("HardwareIds")
    private fun wifiMacAddress(context: Context): String {
        var result = String()
        whenNotNull(context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager?) {

            result = it.connectionInfo.macAddress
        }
        return result
    }

    @SuppressLint("MissingPermission", "HardwareIds")
    private fun imei(context: Context): String {
        var result = String()
        whenNotNull(context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager?) {

            result = it.deviceId
        }

        return result
    }

    /**
     * Gets screen width.
     *
     * @param context the context
     * @return the screen width
     */
    @SuppressLint("NewApi")
    fun screenWidth(context: Context): Int? {

        var result: Int? = null

        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay

        val size = Point()
        display.getSize(size)
        result = size.x

        return result
    }

    /**
     * Gets screen orientation.
     *
     * @param context the context
     * @return the screen orientation
     */
    fun screenOrientation(context: Context): Int {

        return context.resources.configuration.orientation
    }

    /**
     * Gets screen size.
     *
     * @param context the context
     * @return the screen size
     */
    @SuppressLint("NewApi")
    fun screenSize(context: Context): Point? {

        if (screenSize == null) {

            val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            val display = wm.defaultDisplay

            val size = Point()
            display.getSize(size)
            screenSize = size
        }

        return screenSize
    }

    /**
     * Gets mobile operator.
     *
     * @param context the context
     * @return the mobile operator
     */
    fun mobileOperator(context: Context): String? {
        var result: String? = null
        val telephony = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        result = telephony.networkOperatorName

        return result
    }

    /**
     * Is package installed boolean.
     *
     * @param packageName the package name
     * @param context     the context
     * @return the boolean
     */
    fun isPackageInstalled(packageName: String, context: Context): Boolean {
        return try {

            context.packageManager.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES)
            true
        } catch (e: PackageManager.NameNotFoundException) {

            false
        }
    }

    /***
     * Checks wether the user has GPS enabled. Must add permission android.permission.ACCESS_FINE_LOCATION
     * @param context the context
     * @return true if enabled
     */
    fun isGpsEnabled(context: Context): Boolean {

        val manager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    @SuppressLint("MissingPermission")
    fun performHapticFeedback(context: Context) {
        when {
            Build.VERSION.SDK_INT >= 26 -> {

                (context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator).vibrate(VibrationEffect.createOneShot(150, VibrationEffect.DEFAULT_AMPLITUDE))
            }
            else -> {

                (context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator).vibrate(150)
            }
        }
    }

    @SuppressLint("MissingPermission")
    fun isFingerprintSupported(context: Context): Boolean {

        return FingerprintManagerCompat.from(context).isHardwareDetected
    }

    @SuppressLint("MissingPermission")
    fun hasEnrolledFingerprints(context: Context): Boolean {

        return FingerprintManagerCompat.from(context).hasEnrolledFingerprints()
    }
}