/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.helper

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.tandeminnovation.appbase.extension.whenNotNull

/**
 * The type Connection helper.
 */
object ConnectionHelper {

    /**
     * Is network available boolean.
     *
     * @param context the context
     * @return the boolean
     */
    fun isNetworkAvailable(context: Context): Boolean {
        var result = false
        val activeNetwork = getActiveNetwork(context)
        whenNotNull(activeNetwork) { networkInfo: NetworkInfo ->
            // connected to the internet

            result = networkInfo.isConnected
        }

        return result
    }

    @SuppressLint("MissingPermission")
    private fun getActiveNetwork(context: Context): NetworkInfo? {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager


        return connectivityManager.activeNetworkInfo
    }
}
