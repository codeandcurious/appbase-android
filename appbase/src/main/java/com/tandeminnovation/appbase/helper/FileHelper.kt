/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.helper

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.os.ParcelFileDescriptor
import android.provider.OpenableColumns
import android.util.Base64
import android.webkit.MimeTypeMap
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import java.io.*
import java.text.SimpleDateFormat
import java.util.*

/**
 * The type File helper.
 */
object FileHelper {

    private val JPG_EXT = ".jpg"
    private var _authorityProvider: String = String()

    /**
     * Setup should be called in the app start so all operations can access the authority for file operations
     * @param authorityProvider the authority provider which has to be declared in manifest (app.build.id.provider)
     */
    fun setup(authorityProvider: String) {
        this._authorityProvider = authorityProvider
    }

    /**
     * Creates a file. This function can only be called after the user chooses a location for the file
     * @param appCompatActivity the context activity
     * @param fileUri The file Uri
     * @param fileContent The file content to be saved
     * @param mimeType The mimeType for the file
     * @param autoOpenFile if true will open the downloaded file after completion
     * @return null if ok, exception if fails
     */
    fun downloadFile(appCompatActivity: AppCompatActivity, fileUri: Uri, fileContent: String, mimeType: String, autoOpenFile: Boolean = false): Exception? {
        val fos = appCompatActivity.contentResolver.openOutputStream(fileUri)
        return try {
            fos?.write(fileContent.toByteArray())
            fos?.flush()
            fos?.close()
            if (autoOpenFile) openFileExternally(appCompatActivity, fileUri, mimeType)
            else null
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            e
        }
    }

    /**
     * Creates a file. This function can only be called after the user chooses a location for the file
     * @param appCompatActivity the context activity
     * @param fileUri The file Uri
     * @param encodedBase64 The file encoded in Base64
     * @param mimeType The mimeType for the file
     * @param autoOpenFile if true will open the downloaded file after completion
     * @return null if ok, exception if fails
     */
    fun downloadEncodedFileInBase64(appCompatActivity: AppCompatActivity, fileUri: Uri, encodedBase64: String, mimeType: String, autoOpenFile: Boolean = false): Exception? {
        val fos = appCompatActivity.contentResolver.openOutputStream(fileUri)
        return try {
            fos?.write(Base64.decode(encodedBase64, 0))
            fos?.flush()
            fos?.close()
            if (autoOpenFile) openFileExternally(appCompatActivity, fileUri, mimeType)
            else null
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            e
        }
    }

    /**
     * Opens a file in an external app
     * @param appCompatActivity the context activity
     * @param fileUri The file Uri
     * @param mimeType The mimeType for the file
     * @return null if ok, exception if fails
     */
    fun openFileExternally(appCompatActivity: AppCompatActivity, fileUri: Uri, mimeType: String): Exception? {
        return try {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.setDataAndType(fileUri, mimeType)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
                intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
            }
            appCompatActivity.startActivity(intent)
            null
        } catch (e: ActivityNotFoundException) {
            e.printStackTrace()
            e
        }
    }

    /**
     * Opens a file in an external app
     * @param appCompatActivity the context activity
     * @param file The file
     * @param mimeType The mimeType for the file
     * @return null if ok, exception if fails
     */
    fun openFileExternally(appCompatActivity: AppCompatActivity, file: File, mimeType: String): Exception? {
        return try {
            val intent = Intent(Intent.ACTION_VIEW)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                val uri = FileProvider.getUriForFile(appCompatActivity, _authorityProvider, file)
                intent.setDataAndType(uri, mimeType)
                intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
                intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
            } else {
                intent.setDataAndType(Uri.parse(file.toString()), mimeType)
            }
            appCompatActivity.startActivity(intent)
            null
        } catch (e: ActivityNotFoundException) {
            e.printStackTrace()
            e
        }
    }

    /**
     * Opens an external app to download the file, like Chrome
     * @param context Context
     * @param url the file url
     * @return null if ok, exception if fails
     */
    fun openFileFromUrl(context: Context, url: String): Exception? {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        return try {
            context.startActivity(intent)
            null
        } catch (e: ActivityNotFoundException) {
            e
        }
    }

    /**
     * Find the file name
     * @param context Context
     * @param url the file url
     * @return the file name if found or an empty string
     */
    fun getFileName(context: Context, uri: Uri?): String {
        var fileName = String()
        uri?.let { returnUri ->
            context.contentResolver.query(returnUri, null, null, null, null)
        }?.use { cursor ->
            val nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
            cursor.moveToFirst()
            fileName = cursor.getString(nameIndex)
        }

        return fileName
    }

    fun setCameraImageUri(context: Context, fileName: String): Uri {
        val folder = File("${context.getExternalFilesDir(Environment.DIRECTORY_DCIM)}")
        folder.mkdirs()
        val imageUri: Uri
        val file = File(folder, fileName)
        if (file.exists())
            file.delete()
        file.createNewFile()
        imageUri = FileProvider.getUriForFile(context, _authorityProvider, file)
        return imageUri
    }

    /**
     * Clears an internal app folder
     * @param appCompatActivity The context activity
     * @param pathName the path which will be used to clear the cache. Only paths inside the application package can be reached
     */
    fun clearInternalCache(appCompatActivity: AppCompatActivity, pathName: String) {
        val folder = File(pathName)
        try {
            if (folder.isDirectory) folder.delete()
        } catch (t: Throwable) {
            t.printStackTrace()
        }
    }


    @Throws(IOException::class)
    fun getBitmapFromUri(context: Context, uri: Uri): Bitmap {
        val parcelFileDescriptor: ParcelFileDescriptor? = context.contentResolver.openFileDescriptor(uri, "r")
        val fileDescriptor: FileDescriptor? = parcelFileDescriptor?.fileDescriptor
        val image: Bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor)
        parcelFileDescriptor?.close()
        return image
    }

    /**
     * Write to file.
     *
     * @param context the context
     * @param data    the data
     * @param path    the path
     * @throws IOException the io exception
     */
    @Throws(IOException::class)
    fun writeToFile(context: Context, data: String, path: String) {

        val file = File(path)
        file.createNewFile()

        if (!file.isFile) {

            throw IllegalArgumentException("Should not be a directory: $file")
        }

        if (!file.canWrite()) {

            throw IllegalArgumentException("File cannot be written: $file")
        }

        val output = BufferedWriter(FileWriter(file))

        try {

            output.write(data)
        } catch (e: IOException) {

            throw e
        } finally {

            output.close()

            // Notify the system about the new file
            try {

                val mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(getExtension(path))
                MediaScannerConnection.scanFile(context, arrayOf(file.absolutePath), arrayOf(mimeType), null)
            } catch (e2: Exception) {
                // Ignore
            }

        }
    }

    /**
     * Gets extension.
     *
     * @param path the path
     * @return the extension
     */
    fun getExtension(path: String): String {

        val index = path.lastIndexOf(".")

        return path.substring(Math.min(path.length, index + 1))
    }

    /**
     * Exists boolean.
     *
     * @param path the path
     * @return the boolean
     */
    fun exists(path: String): Boolean {

        return File(path).exists()
    }

    /**
     * Create image temp file file.
     *
     * @param context      the context
     * @param publicFolder the public folder
     * @return the file
     * @throws IOException the io exception
     */
    @SuppressLint("SimpleDateFormat")
    @Throws(IOException::class)
    fun createImageTempFile(context: Context, publicFolder: Boolean): File {

        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = PathHelper.getPictureFolder(context, publicFolder)

        return File.createTempFile(imageFileName, JPG_EXT, storageDir)
    }
}