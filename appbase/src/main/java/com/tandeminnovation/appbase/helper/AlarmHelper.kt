/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.helper

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context

class AlarmHelper {

    companion object {

        fun setAlarm(context: Context, pendingIntent: PendingIntent, intervalInMillis: Long, repeat: Boolean) {

            val alarm = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager

            if (repeat) {

                alarm.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + intervalInMillis, intervalInMillis, pendingIntent)
            } else {

                alarm.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + intervalInMillis, pendingIntent)
            }
        }

        fun resetAlarm(context: Context, pendingIntent: PendingIntent) {

            val alarm = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            alarm.cancel(pendingIntent)
        }
    }
}