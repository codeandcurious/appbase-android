/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.helper

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.tandeminnovation.appbase.R

class AnimationHelper {

    companion object {

        fun animateFragmentFromRight(activity: AppCompatActivity, containerId: Int, fragment: Fragment) {

            activity.supportFragmentManager.beginTransaction().addToBackStack(null).setCustomAnimations(R.animator.enter_from_right, R.animator.exit_from_left).replace(containerId, fragment).commit()
        }

        fun animateFragmentFromLeft(activity: AppCompatActivity, containerId: Int, fragment: Fragment) {

            activity.supportFragmentManager.beginTransaction().addToBackStack(null).setCustomAnimations(R.animator.enter_from_left, R.animator.exit_from_right).replace(containerId, fragment).commit()
        }

        fun flipToCenter(front: View, back: View, duration: Int, animationFinishedListener: AnimatorListenerAdapter) {

            val animator = front.animate()
            animator.startDelay = 0
            animator.alpha(1f)
            animator.rotationY(90f)
            animator.scaleX(3.0f)
            animator.scaleY(3.0f)
            animator.translationX(370 - front.x)
            animator.translationY(602 - front.y)
            animator.duration = (duration / 2).toLong()
            animator.setListener(object : AnimatorListenerAdapter() {

                override fun onAnimationEnd(animation: Animator) {

                    front.visibility = View.GONE
                    back.rotationY = -90f
                    back.visibility = View.VISIBLE
                    back.animate().setDuration((duration / 2).toLong()).rotationY(0f).setListener(animationFinishedListener)
                }
            })
        }

        fun animateToBottom(view: View, duration: Int) {

            val animator = view.animate()
            animator.translationY(700f)
            animator.duration = duration.toLong()
            animator.setListener(object : AnimatorListenerAdapter() {

                override fun onAnimationEnd(animation: Animator) {

                    view.visibility = View.GONE
                    view.animate().translationY(0f).setDuration(0).setListener(null)
                }
            })
        }
    }
}
