/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.helper

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.provider.MediaStore
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.tandeminnovation.appbase.enumeration.MediaLocation


/**
 * The type Intent helper.
 */
object IntentHelper {


    /**
     * Indicates whether the specified action can be used as an intent. This
     * method queries the package manager for installed packages that can
     * respond to an intent with the specified action. If no suitable package is
     * found, this method returns false.
     *
     * @param context The application's environment.
     * @param action  The Intent action to check for availability.
     * @return True if an Intent with the specified action can be sent and responded to, false otherwise.
     */
    fun isIntentAvailable(context: Context, action: String): Boolean {
        val packageManager = context.packageManager
        val intent = Intent(action)
        val list = packageManager.queryIntentActivities(
                intent,
                PackageManager.MATCH_DEFAULT_ONLY
        )
        return list.size > 0
    }

    /**
     * Send email.
     *
     * @param context                 the context
     * @param toResxId                the to resx id
     * @param subjectResxId           the subject resx id
     * @param contentResxId           the content resx id
     * @param chooserTitleResxId      the chooser title resx id
     * @param noClientInstalledResxId the no client installed resx id
     */
    fun sendEmail(context: Context, toResxId: Int, subjectResxId: Int?, contentResxId: Int?, chooserTitleResxId: Int?, noClientInstalledResxId: Int?) {

        val to = getText(context, toResxId)
        val subject = if (subjectResxId == null) null else getText(context, subjectResxId)
        val content = if (contentResxId == null) null else getText(context, contentResxId)
        val chooserTitle = if (chooserTitleResxId == null) null else getText(context, chooserTitleResxId)
        val noClientInstalled = if (noClientInstalledResxId == null) null else getText(context, noClientInstalledResxId)

        sendEmail(context, to, subject, content, chooserTitle, noClientInstalled)
    }

    /**
     * Send email.
     *
     * @param context           the context
     * @param to                the to
     * @param subject           the subject
     * @param content           the content
     * @param chooserTitle      the chooser title
     * @param noClientInstalled the no client installed
     */
    fun sendEmail(context: Context, to: String, subject: String?, content: String?, chooserTitle: String?, noClientInstalled: String?) {

        val emailIntent = Intent(Intent.ACTION_SEND)
        emailIntent.type = "text/html"
        emailIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(to))
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
        emailIntent.putExtra(Intent.EXTRA_TEXT, content)

        try {

            context.startActivity(Intent.createChooser(emailIntent, chooserTitle))
        } catch (ex: ActivityNotFoundException) {

            Toast.makeText(context, noClientInstalled, Toast.LENGTH_SHORT).show()
        }

    }

    /**
     * Create email intent.
     *
     * @param context            the context
     * @param toResxId           the to resx id
     * @param subjectResxId      the subject resx id
     * @param contentResxId      the content resx id
     * @param chooserTitleResxId the chooser title resx id
     * @return the intent
     */
    fun createEmail(context: Context, toResxId: Int, subjectResxId: Int?, contentResxId: Int?, chooserTitleResxId: Int?): Intent {

        val to = getText(context, toResxId)
        val subject = if (subjectResxId == null) null else getText(context, subjectResxId)
        val content = if (contentResxId == null) null else getText(context, contentResxId)
        val chooserTitle = if (chooserTitleResxId == null) null else getText(context, chooserTitleResxId)

        return createEmail(context, to, subject, content, chooserTitle)
    }

    /**
     * Create email intent.
     *
     * @param context      the context
     * @param to           the to
     * @param subject      the subject
     * @param content      the content
     * @param chooserTitle the chooser title
     * @return the intent
     */
    fun createEmail(context: Context, to: String, subject: String?, content: String?, chooserTitle: String?): Intent {

        val emailIntent = Intent(Intent.ACTION_SEND)
        emailIntent.type = "text/html"
        emailIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(to))
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
        emailIntent.putExtra(Intent.EXTRA_TEXT, content)


        return Intent.createChooser(emailIntent, chooserTitle)
    }

    /**
     * Open url.
     *
     * @param context                 the context
     * @param urlResx                 the url resx
     * @param noClientInstalledResxId the no client installed resx id
     */
    fun openUrl(context: Context, urlResx: Int, noClientInstalledResxId: Int?) {

        openUrl(context, getText(context, urlResx), noClientInstalledResxId)
    }

    /**
     * Open url.
     *
     * @param context                 the context
     * @param url                     the url
     * @param noClientInstalledResxId the no client installed resx id
     */
    fun openUrl(context: Context, url: String, noClientInstalledResxId: Int?) {
        val noClientInstalled = if (noClientInstalledResxId == null) null else getText(context, noClientInstalledResxId)
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(url)

        try {

            context.startActivity(i)
        } catch (ex: ActivityNotFoundException) {

            Toast.makeText(context, noClientInstalled, Toast.LENGTH_SHORT).show()
        }
    }

    fun createShareUrl(chooserTitle: String, url: String): Intent {
        val share = Intent(android.content.Intent.ACTION_SEND)
        share.type = "text/plain"
        // Add data to the intent, the receiving app will decide
        // what to do with it.
        share.putExtra(Intent.EXTRA_TEXT, url)

        return Intent.createChooser(share, chooserTitle)
    }

    fun shareUrl(context: Context, chooserTitleResId: Int, url: String, noClientInstalledResId: Int) {

        val intent = this.createShareUrl(getText(context, chooserTitleResId), url)
        shareUrl(context, intent, getText(context, noClientInstalledResId))
    }

    fun shareUrl(context: Context, chooserTitle: String, url: String, noClientInstalledResId: Int) {

        val intent = this.createShareUrl(chooserTitle, url)
        shareUrl(context, intent, getText(context, noClientInstalledResId))
    }

    fun shareUrl(context: Context, chooserTitleResId: Int, url: String, noClientInstalledMessage: String) {

        val intent = this.createShareUrl(getText(context, chooserTitleResId), url)
        shareUrl(context, intent, noClientInstalledMessage)
    }

    fun shareUrl(context: Context, chooserTitle: String, url: String, noClientInstalledMessage: String) {

        val intent = this.createShareUrl(chooserTitle, url)
        shareUrl(context, intent, noClientInstalledMessage)
    }

    fun shareUrl(context: Context, intent: Intent, noClientInstalledMessage: String) {
        try {

            context.startActivity(intent)
        } catch (ex: ActivityNotFoundException) {

            Toast.makeText(context, noClientInstalledMessage, Toast.LENGTH_SHORT).show()
        }
    }


    /**
     * Create url intent.
     *
     * @param context the context
     * @param urlResx the url resx
     * @return the intent
     */
    fun createUrl(context: Context, urlResx: Int): Intent {

        return createUrl(context, getText(context, urlResx))
    }

    /**
     * Create url intent.
     *
     * @param context the context
     * @param url     the url
     * @return the intent
     */
    fun createUrl(context: Context, url: String): Intent {
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(url)


        return i
    }


    /**
     * Dial.
     *
     * @param context           the context
     * @param phoneNumber       the phone number
     * @param noClientInstalled the no client installed
     */
    fun dial(context: Context, phoneNumber: String, noClientInstalled: String) {

        val uri = "tel:" + phoneNumber.trim { it <= ' ' }
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse(uri)

        try {

            context.startActivity(intent)
        } catch (ex: ActivityNotFoundException) {

            Toast.makeText(context, noClientInstalled, Toast.LENGTH_SHORT).show()
        }

    }

    /**
     * Dial.
     *
     * @param context                the context
     * @param phoneNumber            the phone number
     * @param noClientInstalledRexId the no client installed rex id
     */
    fun dial(context: Context, phoneNumber: String, noClientInstalledRexId: Int) {

        val uri = "tel:" + phoneNumber.trim { it <= ' ' }
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse(uri)

        try {

            context.startActivity(intent)
        } catch (ex: ActivityNotFoundException) {

            Toast.makeText(context, noClientInstalledRexId, Toast.LENGTH_SHORT).show()
        }

    }

    /**
     * Create dial intent.
     *
     * @param context     the context
     * @param phoneNumber the phone number
     * @return the intent
     */
    fun createDial(context: Context, phoneNumber: String): Intent {
        val uri = "tel:" + phoneNumber.trim { it <= ' ' }
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse(uri)

        return intent
    }

    /**
     * Directions.
     *
     * @param context           the context
     * @param sourceLat         the source lat
     * @param sourceLong        the source long
     * @param destinationLat    the destination lat
     * @param destinationLong   the destination long
     * @param noClientInstalled the no client installed message
     */
    fun directions(context: Context, sourceLat: Double?, sourceLong: Double?, destinationLat: Double, destinationLong: Double, noClientInstalled: String) {

        var uriString = "http://maps.google.com/maps?"

        if (sourceLat != null && sourceLong != null) {

            uriString = uriString + String.format("saddr=%s,%s&", sourceLat, sourceLong)
        }

        uriString = uriString + String.format("daddr=%s,%s", destinationLat, destinationLong)

        val uri = Uri.parse(uriString)
        val intent = Intent(android.content.Intent.ACTION_VIEW, uri)

        try {

            context.startActivity(intent)
        } catch (ex: ActivityNotFoundException) {

            Toast.makeText(context, noClientInstalled, Toast.LENGTH_SHORT).show()
        }

    }

    /**
     * Directions.
     *
     * @param context                the context
     * @param sourceLat              the source lat
     * @param sourceLong             the source long
     * @param destinationLat         the destination lat
     * @param destinationLong        the destination long
     * @param noClientInstalledRexId the no client installed rex id
     */
    fun directions(context: Context, sourceLat: Double?, sourceLong: Double?, destinationLat: Double, destinationLong: Double, noClientInstalledRexId: Int) {

        var uriString = "http://maps.google.com/maps?"

        if (sourceLat != null && sourceLong != null) {

            uriString = uriString + String.format("saddr=%s,%s&", sourceLat, sourceLong)
        }

        uriString = uriString + String.format("daddr=%s,%s", destinationLat, destinationLong)

        val uri = Uri.parse(uriString)
        val intent = Intent(android.content.Intent.ACTION_VIEW, uri)

        try {

            context.startActivity(intent)
        } catch (ex: ActivityNotFoundException) {

            Toast.makeText(context, noClientInstalledRexId, Toast.LENGTH_SHORT).show()
        }

    }

    /**
     * Create directions intent.
     *
     * @param context         the context
     * @param sourceLat       the source lat
     * @param sourceLong      the source long
     * @param destinationLat  the destination lat
     * @param destinationLong the destination long
     * @return the intent
     */
    fun createDirections(context: Context, sourceLat: Double?, sourceLong: Double?, destinationLat: Double, destinationLong: Double): Intent {

        var uriString = "http://maps.google.com/maps?"

        if (sourceLat != null && sourceLong != null) {

            uriString = uriString + String.format("saddr=%s,%s&", sourceLat, sourceLong)
        }

        uriString = uriString + String.format("daddr=%s,%s", destinationLat, destinationLong)

        val uri = Uri.parse(uriString)

        return Intent(Intent.ACTION_VIEW, uri)
    }

    fun createShareDirections(context: Context, destinationLat: Double, destinationLong: Double): Intent {
        val uri = "https://www.google.com/maps/search/?api=1&query=$destinationLat,$destinationLong"
        val intent = Intent(android.content.Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(android.content.Intent.EXTRA_TEXT, uri)

        return intent
    }

    fun shareDirections(context: Context, destinationLat: Double, destinationLong: Double, intentChooserTitle: String) {
        val uri = "https://www.google.com/maps/search/?api=1&query=$destinationLat,$destinationLong"
        val intent = Intent(android.content.Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(android.content.Intent.EXTRA_TEXT, uri)
        context.startActivity(Intent.createChooser(intent, intentChooserTitle))
    }

    fun shareDirections(context: Context, destinationLat: Double, destinationLong: Double, intentChooserTitleResX: Int) {
        shareDirections(context, destinationLat, destinationLong, getText(context, intentChooserTitleResX))
    }

    /**
     * Opens a system chooser with the provided methods to search for image media
     * @param appCompatActivity the context activity
     * @param imageUri the uri for the camera image
     * @param intentTitle the title for the bottom sheet
     * @param requestCode the request code for the intent result
     * @param mediaLocation the media location which will be presented in the system bottom sheet. Verify the permissions before choosing this value
     * @param mimeType the mime type for the file explorer image/'*' for all images, application/pdf for pdf's
     */
    fun openChooser(appCompatActivity: AppCompatActivity, imageUri: Uri, intentTitle: String, requestCode: Int, mediaLocation: MediaLocation, mimeType: String = "image/*") {
        val intentList = mutableListOf<Intent>()

        // Camera
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
        intentList.add(cameraIntent)

        // Gallery
        val fileExplorerIntent = Intent()
        fileExplorerIntent.type = mimeType
        fileExplorerIntent.action = Intent.ACTION_GET_CONTENT

        // Chooser
        val chooserIntent: Intent
        when (mediaLocation) {
            MediaLocation.Camera -> chooserIntent = Intent.createChooser(cameraIntent, intentTitle)
            MediaLocation.FileExplorer -> chooserIntent = Intent.createChooser(fileExplorerIntent, intentTitle)
            MediaLocation.Both -> {
                chooserIntent = Intent.createChooser(fileExplorerIntent, intentTitle)
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentList.toTypedArray())
            }
        }

        appCompatActivity.startActivityForResult(chooserIntent, requestCode)
    }

    /**
     * Opens a system chooser with the provided methods to search for image media
     * @param fragment the context fragments
     * @param imageUri the uri for the camera image. Use #[FileHelper.setCameraImageUri] to have a valid camera Uri if needed
     * @param intentTitle the title for the bottom sheet
     * @param requestCode the request code for the intent result
     * @param mediaLocation the media location which will be presented in the system bottom sheet. Verify the permissions before choosing this value
     * @param mimeType the mime type for the file explorer image/'*' for all images, application/pdf for pdf's
     */
    fun openChooser(fragment: Fragment, imageUri: Uri? = null, intentTitle: String, requestCode: Int, mediaLocation: MediaLocation, mimeType: String = "image/*") {
        val intentList = mutableListOf<Intent>()

        // Camera
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
        intentList.add(cameraIntent)

        // Gallery
        val fileExplorerIntent = Intent()
        fileExplorerIntent.type = mimeType
        fileExplorerIntent.action = Intent.ACTION_GET_CONTENT

        // Chooser
        val chooserIntent: Intent

        when (mediaLocation) {
            MediaLocation.Camera -> chooserIntent = Intent.createChooser(cameraIntent, intentTitle)
            MediaLocation.FileExplorer -> chooserIntent = Intent.createChooser(fileExplorerIntent, intentTitle)
            MediaLocation.Both -> {
                chooserIntent = Intent.createChooser(fileExplorerIntent, intentTitle)
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentList.toTypedArray())
            }
        }

        fragment.startActivityForResult(chooserIntent, requestCode)
    }

    /**
     * Opens a system chooser with the provided methods to search for image media
     * @param fragment the context fragments
     * @param imageUri the uri for the camera image. Use #[FileHelper.setCameraImageUri] to have a valid camera Uri if needed
     * @param intentTitle the title for the bottom sheet
     * @param requestCode the request code for the intent result
     * @param mediaLocation the media location which will be presented in the system bottom sheet. Verify the permissions before choosing this value
     * @param mimeType the mime type for the file explorer image/'*' for all images, application/pdf for pdf's
     */
    fun openChooser(fragment: Fragment, imageUri: Uri? = null, intentTitle: String, requestCode: Int, mediaLocation: MediaLocation, mimeType: String = "image/*", extraMimeTypes: Array<String>? = null) {
        val intentList = mutableListOf<Intent>()

        // Camera
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
        intentList.add(cameraIntent)

        // Gallery
        val fileExplorerIntent = Intent()
        fileExplorerIntent.type = mimeType
        extraMimeTypes?.let {
            fileExplorerIntent.putExtra(Intent.EXTRA_MIME_TYPES, it)
        }
        fileExplorerIntent.action = Intent.ACTION_GET_CONTENT

        // Chooser
        val chooserIntent: Intent

        when (mediaLocation) {
            MediaLocation.Camera -> chooserIntent = Intent.createChooser(cameraIntent, intentTitle)
            MediaLocation.FileExplorer -> chooserIntent = Intent.createChooser(fileExplorerIntent, intentTitle)
            MediaLocation.Both -> {
                chooserIntent = Intent.createChooser(fileExplorerIntent, intentTitle)
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentList.toTypedArray())
            }
        }

        fragment.startActivityForResult(chooserIntent, requestCode)
    }

    /**
     * Opens an intent for the user
     * @param appCompatActivity the context activity
     * @param fileName The default name for the file
     * @param mimeType The mimeType for the file
     * @param requestCode The request code for the intent result
     */
    fun openIntentForFileDownload(appCompatActivity: AppCompatActivity, fileName: String, mimeType: String, requestCode: Int) {
        val intent = Intent(Intent.ACTION_CREATE_DOCUMENT)
        intent.type = mimeType
        intent.putExtra(Intent.EXTRA_TITLE, fileName)
        appCompatActivity.startActivityForResult(intent, requestCode)
    }


    /**
     * Opens an intent for the user
     * @param fragment the context fragment
     * @param fileName The default name for the file
     * @param mimeType The mimeType for the file
     * @param requestCode The request code for the intent result
     */
    fun openIntentForFileDownload(fragment: Fragment, fileName: String, mimeType: String, requestCode: Int) {
        val intent = Intent(Intent.ACTION_CREATE_DOCUMENT)
        intent.type = mimeType
        intent.putExtra(Intent.EXTRA_TITLE, fileName)
        fragment.startActivityForResult(intent, requestCode)
    }
}
