/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.helper

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Application
import android.app.Application.ActivityLifecycleCallbacks
import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.Context.TELEPHONY_SERVICE
import android.content.res.Configuration
import android.net.wifi.WifiManager
import android.os.Bundle
import android.provider.Settings.Secure
import android.telephony.TelephonyManager
import com.tandeminnovation.appbase.BuildConfig
import com.tandeminnovation.appbase.extension.whenNotNull


class AppHelper private constructor() {

    private var resumed: Int = 0
    private var paused: Int = 0

    private val lifecycleCallbackListener = object : ActivityLifecycleCallbacks {

        override fun onActivityStopped(activity: Activity) {

        }

        override fun onActivityStarted(activity: Activity) {
        }

        override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {

        }

        override fun onActivityResumed(activity: Activity) {

            ++resumed
        }

        override fun onActivityPaused(activity: Activity) {

            ++paused
            info("application is in foreground: " + (resumed > paused), "test")
        }

        override fun onActivityDestroyed(activity: Activity) {

        }

        override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {

        }
    }

    val isApplicationInForeground: Boolean
        get() = resumed > paused


    fun listenToAppStatusChanges(app: Application) {

        app.registerActivityLifecycleCallbacks(lifecycleCallbackListener)
    }

    companion object {

        lateinit var instance: AppHelper
            private set

        @SuppressLint("PrivateApi")
        private fun getSystemProperty(name: String): String? {
            var result: String? = null
            try {
                val systemPropertyClazz = Class.forName("android.os.SystemProperties")
                result = systemPropertyClazz.getMethod("get", String::class.java).invoke(systemPropertyClazz, name) as String

            } catch (e: java.lang.Exception) {

                debug("Error getting system property", "AppHelper-getSystemProperty")
            }

            return result
        }

        fun getAppVersionName(context: Context): String {
            var result = ""
            try {

                result = context.packageManager.getPackageInfo(getPackage(context), 0).versionName
            } catch (e: Exception) {

                nrError("getAppVersionName", "AppHelper", e)
            }

            return result
        }


        @Suppress("DEPRECATION")
        fun getAppVersionCode(context: Context): String {
            var result = ""
            try {

                result = context.packageManager.getPackageInfo(getPackage(context), 0).versionCode.toString()
            } catch (e: Exception) {

                nrError("getAppVersionCode", "AppHelper", e)
            }

            return result
        }

        fun getPackage(context: Context): String {

            return context.applicationContext.packageName
        }

        fun isRunning(ctx: Activity?): Boolean {

            return ctx != null && ctx.hasWindowFocus()
        }

        fun isHardwareKeyboardAvailable(ctx: Context): Boolean {

            return ctx.resources.configuration.keyboard == Configuration.KEYBOARD_QWERTY
        }

        fun wasInstalledFromPlayStore(context: Context): Boolean {
            val installer = context.packageManager.getInstallerPackageName(context.packageName)

            return installer != null && installer.startsWith("com.android.vending")
        }

        fun isRunningOnEmulator(): Boolean {
            var goldfish = false
            whenNotNull(getSystemProperty("ro.hardware")) { property ->

                goldfish = property.contains("goldfish")
            }
            var emu = false
            whenNotNull(getSystemProperty("ro.kernel.qemu")) { property ->

                emu = property.isNotEmpty()
            }
            var sdk = false
            whenNotNull(getSystemProperty("ro.product.model")) { property ->

                sdk = (property == "sdk")
            }

            return emu || goldfish || sdk
        }
    }
}
