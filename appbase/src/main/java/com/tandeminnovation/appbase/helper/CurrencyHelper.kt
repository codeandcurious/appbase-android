/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.helper

import android.content.Context
import com.tandeminnovation.appbase.R
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

/**
 * The type Currency helper.
 */
object CurrencyHelper {

    private const val DEFAULT_CURRENCY_FORMAT = "0.00##"
    private var currencies: MutableList<Currency>? = null

    /**
     * Gets default currency.
     *
     * @return the default currency
     */
    val defaultCurrency: Currency
        get() = Currency.getInstance(Locale.getDefault())

    /**
     * Gets string.
     *
     * @param value                the value
     * @param currency             the currency
     * @param addDigitsIfNecessary the add digits if necessary
     * @return the string
     */
    @JvmOverloads
    fun getString(value: Double?, currency: Currency?, addDigitsIfNecessary: Boolean = false): String {

        var result = ""
        var format = DEFAULT_CURRENCY_FORMAT
        var symbol = ""

        if (currency != null) {

            symbol = currency.getSymbol(Locale.US)
            val decimalDigits = currency.defaultFractionDigits
            format = "0"

            for (i in 0 until decimalDigits) {

                if (i == 0) {

                    format += "."
                }

                format += "0"
            }

            if (addDigitsIfNecessary) {

                if (decimalDigits > 0) {

                    format += "##"
                } else {

                    format += ".####"
                }
            }
        }

        if (value != null) {

            val df = DecimalFormat(format)

            result = String.format("%s%s", symbol, df.format(value))
        }

        return result
    }

    fun formatValue(value: Double, locale: Locale): String {

        return NumberFormat.getNumberInstance(locale).format(value)
    }

    fun formatValue(value: String, locale: Locale): String {

        return NumberFormat.getNumberInstance(locale).format(value.toDouble())
    }

    /**
     * Are equal boolean.
     *
     * @param first  the first
     * @param second the second
     * @return the boolean
     */
    fun areEqual(first: Currency?, second: Currency?): Boolean {

        return first != null && second != null && first.currencyCode.equals(second.currencyCode, ignoreCase = true)
    }

    /**
     * Gets currency list.
     *
     * @param context the context
     * @return the currency list
     */
    fun getCurrencyList(context: Context): List<Currency>? {

        if (currencies == null) {

            currencies = ArrayList()
            val codes = context.resources.getStringArray(R.array.currency_codes)
            for (code in codes) {

                try {

                    // Avoid crashing if one of the listed currencies doesn't exist on the device
                    val currency = Currency.getInstance(code)
                    currencies!!.add(currency)
                } catch (e: Exception) {

                    info("Unknown currency code: $code","CurrencyHelper")
                }
            }
        }

        return currencies
    }
}