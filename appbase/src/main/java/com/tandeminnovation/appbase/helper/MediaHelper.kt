/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.helper

import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import java.io.File

/**
 * The type Media helper.
 */
object MediaHelper {

    /**
     * Play sound.
     *
     * @param context the context
     * @param resXId   the resXId
     */
    fun playSound(context: Context, resXId: Int) {
        val mediaPlayer = MediaPlayer.create(context, resXId)
        mediaPlayer.setOnCompletionListener { mp -> mp.release() }

        mediaPlayer.start()
    }

    /**
     * Add picture to gallery.
     *
     * @param context  the context
     * @param filePath the file path
     */
    fun addPictureToGallery(context: Context, filePath: String) {
        val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
        val file = File(filePath)
        val contentUri = Uri.fromFile(file)
        mediaScanIntent.data = contentUri
        context.sendBroadcast(mediaScanIntent)
    }
}