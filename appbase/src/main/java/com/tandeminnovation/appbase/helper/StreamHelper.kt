/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.helper

import com.tandeminnovation.appbase.extension.readListAndClose
import com.tandeminnovation.appbase.extension.readStringAndClose
import java.io.InputStream
import java.nio.charset.Charset

/**
 * The type Stream helper.
 */
object StreamHelper {

    private val DEFAULT_CHARSET = Charsets.UTF_8

    /**
     *
     * @param stream InputStream?
     * @param format Charset
     * @return String?
     */
    @JvmOverloads
    fun inputStreamToString(stream: InputStream?, format: Charset = DEFAULT_CHARSET): String? {
        var result: String? = null
        if (stream != null) {

            result = stream.readStringAndClose(format)
        }

        return result
    }

    /**
     *
     * @param stream InputStream?
     * @param format Charset
     * @return List<String>?
     */
    @JvmOverloads
    fun inputStreamToStringList(stream: InputStream?, format: Charset = DEFAULT_CHARSET): List<String>? {
        var result = emptyList<String>()
        if (stream != null) {

            result = stream.readListAndClose(format)
        }

        return result
    }
}