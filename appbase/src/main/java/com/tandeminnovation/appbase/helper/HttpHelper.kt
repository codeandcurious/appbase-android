/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.helper

import com.tandeminnovation.appbase.entity.NameValuePair
import okhttp3.*
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import java.io.IOException
import java.io.InputStream
import java.io.UnsupportedEncodingException
import java.net.UnknownHostException
import java.net.UnknownServiceException
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager


@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class HttpHelper private constructor() {

    var isGzipEnabled = false

    /* Properties */
    var httpClient: OkHttpClient = OkHttpClient.Builder()
        .readTimeout(READ_TIMEOUT.toLong(), TimeUnit.SECONDS)
        .writeTimeout(WRITE_TIMEOUT.toLong(), TimeUnit.SECONDS)
        .connectTimeout(CONNECTION_TIMEOUT.toLong(), TimeUnit.SECONDS).build()


    /**
     * Build url string.
     *
     * @param url               the url
     * @param queryStringParams the query string params
     * @return the string
     */
    fun buildUrl(url: String, vararg queryStringParams: NameValuePair = emptyArray()): String {

        val urlBuilder = url.toHttpUrlOrNull()?.newBuilder()

        if (queryStringParams.isNotEmpty()) {

            for (pair in queryStringParams) {

                urlBuilder?.addQueryParameter(pair.name, pair.value)
            }
        }

        return urlBuilder?.build().toString()
    }

    private fun buildBaseRequest(url: String, vararg headerParams: NameValuePair = emptyArray()): Request.Builder {

        val requestBuilder = Request.Builder().url(url)

        if (headerParams.isNotEmpty()) {

            for (pair in headerParams) {

                requestBuilder.header(pair.name, pair.value)
            }
        }

        if (isGzipEnabled) {

            requestBuilder.header(ACCEPT_ENCODING, GZIP_DEFLATE)
        }

        return requestBuilder
    }

    private fun buildPost(url: String, jsonContent: String, vararg headerParams: NameValuePair): Request.Builder {
        val postBody = buildJsonRequestBody(jsonContent)

        return buildPost(url, postBody, *headerParams)
    }

    private fun buildPost(url: String, body: RequestBody, vararg headerParams: NameValuePair): Request.Builder {
        val builder = buildBaseRequest(url, *headerParams)
        builder.post(body)


        return builder
    }

    public fun ignoreCertificates(builder: OkHttpClient.Builder): OkHttpClient.Builder {
        debug("Ignoring SSL Certificate", TAG)
        try {

            // Create a trust manager that does not validate certificate chains
            val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {

                override fun getAcceptedIssuers(): Array<X509Certificate> {

                    return emptyArray()
                }

                @Throws(CertificateException::class)
                override fun checkClientTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) {
                }

                @Throws(CertificateException::class)
                override fun checkServerTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) {
                }
            })

            // Install the all-trusting trust manager
            val sslContext = SSLContext.getInstance("SSL")
            sslContext.init(null, trustAllCerts, java.security.SecureRandom())
            // Create an ssl socket factory with our all-trusting manager
            val sslSocketFactory = sslContext.socketFactory

            builder.sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
            builder.hostnameVerifier { _, _ -> true }
        } catch (e: Exception) {
            nrError("Exception while configuring IgnoreSslCertificate", TAG, e)
        }

        return builder
    }

    @Throws(UnsupportedEncodingException::class)
    private fun buildGet(url: String, vararg headerParams: NameValuePair): Request.Builder {

        return buildBaseRequest(url, *headerParams)
    }

    private fun buildPut(url: String, jsonContent: String, vararg headerParams: NameValuePair): Request.Builder {
        val putBody = buildJsonRequestBody(jsonContent)

        return buildPut(url, putBody, *headerParams)
    }

    private fun buildPut(url: String, body: RequestBody, vararg headerParams: NameValuePair): Request.Builder {
        val builder = buildBaseRequest(url, *headerParams)
        builder.put(body)

        return builder
    }

    @Throws(UnsupportedEncodingException::class)
    private fun buildDelete(url: String, vararg headerParams: NameValuePair): Request.Builder {

        val builder = buildBaseRequest(url, *headerParams)
        builder.delete()
        return builder
    }

    /**
     * Post http response.
     *
     * @param url          the url
     * @param body         the body
     * @param headerParams the header params
     * @return the http response
     * @throws IOException the io exception
     */
    @Throws(IOException::class, UnknownServiceException::class, UnknownHostException::class)
    fun post(url: String, body: RequestBody, vararg headerParams: NameValuePair): HttpResponse {

        val requestBuilder = buildPost(url, body, *headerParams)
        return post(requestBuilder)
    }

    /**
     * Post http response.
     *
     * @param url          the url
     * @param jsonContent  the json content
     * @param headerParams the header params
     * @return the http response
     * @throws IOException the io exception
     */
    @Throws(IOException::class, UnknownServiceException::class, UnknownHostException::class)
    fun post(url: String, jsonContent: String, vararg headerParams: NameValuePair): HttpResponse {

        val requestBuilder = buildPost(url, jsonContent, *headerParams)

        return post(requestBuilder)
    }

    @Throws(IOException::class, UnknownServiceException::class, UnknownHostException::class)
    private fun post(requestBuilder: Request.Builder): HttpResponse {
        val response = httpClient.newCall(requestBuilder.build()).execute()

        return HttpResponse(response)
    }

    /**
     * Put http response.
     *
     * @param url          the url
     * @param body         the body
     * @param headerParams the header params
     * @return the http response
     * @throws IOException the io exception
     */
    @Throws(IOException::class, UnknownServiceException::class, UnknownHostException::class)
    fun put(url: String, body: RequestBody, vararg headerParams: NameValuePair): HttpResponse {

        val requestBuilder = buildPut(url, body, *headerParams)
        return put(requestBuilder)
    }

    /**
     * Put http response.
     *
     * @param url          the url
     * @param jsonContent  the json content
     * @param headerParams the header params
     * @return the http response
     * @throws IOException the io exception
     */
    @Throws(IOException::class, UnknownServiceException::class, UnknownHostException::class)
    fun put(url: String, jsonContent: String, vararg headerParams: NameValuePair): HttpResponse {

        val requestBuilder = buildPut(url, jsonContent, *headerParams)
        return put(requestBuilder)
    }

    @Throws(IOException::class, UnknownServiceException::class, UnknownHostException::class)
    private fun put(requestBuilder: Request.Builder): HttpResponse {
        val response = httpClient.newCall(requestBuilder.build()).execute()

        return HttpResponse(response)
    }

    /**
     * Get http response.
     *
     * @param url          the url
     * @param headerParams the header params
     * @return the http response
     * @throws IOException the io exception
     */
    @Throws(IOException::class, UnknownServiceException::class, UnknownHostException::class)
    operator fun get(url: String, vararg headerParams: NameValuePair): HttpResponse {
        val requestBuilder = buildGet(url, *headerParams)
        val response = httpClient.newCall(requestBuilder.build()).execute()

        return HttpResponse(response)
    }

    /**
     * Get http response.
     *
     * @param url          the url
     * @param etag         the etag
     * @param headerParams the header params
     * @return the http response
     * @throws IOException the io exception
     */
    @Throws(IOException::class, UnknownServiceException::class, UnknownHostException::class)
    operator fun get(url: String, etag: String?, vararg headerParams: NameValuePair): HttpResponse {
        val requestBuilder = buildGet(url, *headerParams)
        if (!etag.isNullOrEmpty()) {

            requestBuilder.header(IF_NONE_MATCH, etag)
        }

        val response = httpClient.newCall(requestBuilder.build()).execute()

        return HttpResponse(response)
    }

    /**
     * Delete http response.
     *
     * @param url          the url
     * @param headerParams the header params
     * @return the http response
     * @throws IOException the io exception
     */
    @Throws(IOException::class, UnknownServiceException::class)
    fun delete(url: String, vararg headerParams: NameValuePair): HttpResponse {
        val requestBuilder = buildDelete(url, *headerParams)
        val response = httpClient.newCall(requestBuilder.build()).execute()

        return HttpResponse(response)
    }

    /**
     * Build json request body request body.
     *
     * @param jsonContent the json content
     * @return the request body
     */
    fun buildJsonRequestBody(jsonContent: String): RequestBody {

        return RequestBody.create(JSON_MEDIA_TYPE, jsonContent)
    }

    /**
     * Build multipart form request body request body.
     *
     * @param jsonContent the json content
     * @param parts       the parts
     * @return the request body
     */
    fun buildMultipartFormRequestBody(jsonContent: String, vararg parts: MultipartFormDataPart): RequestBody {

        val builder = MultipartBody.Builder()
        builder.setType(MultipartBody.FORM)

        for (part in parts) {

            part.populatePart(builder)
        }

        return builder.build()
    }

    /**
     * The type Http response.
     */
    class HttpResponse
    /**
     * Instantiates a new Http response.
     *
     * @param resp the resp
     * @throws IOException the io exception
     */
    @Throws(IOException::class)
    constructor(resp: Response) {

        /**
         * The Stream.
         */
        var stream: InputStream? = null

        /**
         * The Code.
         */
        var code: Int = 0

        /**
         * The Etag.
         */
        var etag: String? = null

        /**
         * Is not modified boolean.
         *
         * @return the boolean
         */
        val isNotModified: Boolean
            get() = code == HTTP_CODE_NOT_MODIFIED

        init {

            code = resp.code
            etag = resp.header(ETAG)

            if (resp.body != null) {

                stream = resp.body!!.byteStream()
            }
        }
    }

    /**
     * The type Multipart form data part.
     */
    class MultipartFormDataPart {

        /**
         * The Name.
         */
        var name: String

        /**
         * The Value.
         */
        var value: String

        /**
         * The Body.
         */
        internal var body: RequestBody? = null

        /**
         * Instantiates a new Multipart form data part.
         *
         * @param name  the name
         * @param value the value
         */
        constructor(name: String, value: String) {

            this.name = name
            this.value = value
        }

        /**
         * Instantiates a new Multipart form data part.
         *
         * @param name     the name
         * @param filename the filename
         * @param body     the body
         */
        constructor(name: String, filename: String, body: RequestBody) {

            this.name = name
            this.value = filename
            this.body = body
        }

        /**
         * Populate part.
         *
         * @param builder the builder
         */
        fun populatePart(builder: MultipartBody.Builder) {

            if (body == null) {

                builder.addFormDataPart(name, value)
            } else {

                builder.addFormDataPart(name, value, body!!)
            }
        }
    }

    companion object {

        const val TAG = "HttpHelper"

        const val HTTP_CODE_OK = 200
        const val HTTP_CODE_CREATED = 201
        const val HTTP_CODE_ACCEPTED = 202
        const val HTTP_CODE_NOT_AUTHORITATIVE = 203
        const val HTTP_CODE_NO_CONTENT = 204
        const val HTTP_CODE_RESET = 205
        const val HTTP_CODE_PARTIAL = 206
        const val HTTP_CODE_MULT_CHOICE = 300
        const val HTTP_CODE_MOVED_PERM = 301
        const val HTTP_CODE_MOVED_TEMP = 302
        const val HTTP_CODE_SEE_OTHER = 303
        const val HTTP_CODE_NOT_MODIFIED = 304
        const val HTTP_CODE_USE_PROXY = 305
        const val HTTP_CODE_BAD_REQUEST = 400
        const val HTTP_CODE_UNAUTHORIZED = 401
        const val HTTP_CODE_PAYMENT_REQUIRED = 402
        const val HTTP_CODE_FORBIDDEN = 403
        const val HTTP_CODE_NOT_FOUND = 404
        const val HTTP_CODE_BAD_METHOD = 405
        const val HTTP_CODE_NOT_ACCEPTABLE = 406
        const val HTTP_CODE_PROXY_AUTH = 407
        const val HTTP_CODE_CLIENT_TIMEOUT = 408
        const val HTTP_CODE_CONFLICT = 409
        const val HTTP_CODE_GONE = 410
        const val HTTP_CODE_LENGTH_REQUIRED = 411
        const val HTTP_CODE_PRECON_FAILED = 412
        const val HTTP_CODE_ENTITY_TOO_LARGE = 413
        const val HTTP_CODE_REQ_TOO_LONG = 414
        const val HTTP_CODE_UNSUPPORTED_TYPE = 415
        const val HTTP_CODE_INTERNAL_ERROR = 500
        const val HTTP_CODE_NOT_IMPLEMENTED = 501
        const val HTTP_CODE_BAD_GATEWAY = 502
        const val HTTP_CODE_UNAVAILABLE = 503
        const val HTTP_CODE_GATEWAY_TIMEOUT = 504
        const val HTTP_CODE_VERSION = 505

        const val UTF8 = "UTF-8"
        val JSON_MEDIA_TYPE = "application/json; charset=utf-8".toMediaTypeOrNull()

        private const val AUTHORIZATION = "Authorization"
        private const val USER_AGENT = "User-Agent"
        private const val ACCEPT_ENCODING = "Accept-Encoding"
        private const val GZIP_DEFLATE = "gzip, deflate"

        private const val ETAG = "ETag"
        private const val IF_NONE_MATCH = "If-None-Match"

        private const val WRITE_TIMEOUT = 30
        private const val READ_TIMEOUT = 30
        private const val CONNECTION_TIMEOUT = 60

        private var addedLoggingInterceptor = false

        var instance: HttpHelper = HttpHelper()
    }
}