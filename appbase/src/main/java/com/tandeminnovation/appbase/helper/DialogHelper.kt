/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.helper

import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.text.Spannable
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import com.tandeminnovation.appbase.ui.fragment.CommonDialogFragment

/**
 * The type Dialog helper.
 */
/**
 * Instantiates a new Dialog helper.
 */
class DialogHelper {

    companion object {

        private const val TAG = "DialogHelper"
        private var noGpsDialogActive = false

        /**
         * The constant DEFAULT_THEME.
         */
        private var DEFAULT_THEME = 16974130

        private val noGPSDialogCloseListener: DialogInterface.OnClickListener = DialogInterface.OnClickListener { dialog, which ->
            DialogHelper.noGpsDialogActive = false
            try {

                dialog.dismiss()
            } catch (exception: Exception) {

               nrError("noGPSDialogCloseListener", "DialogHelper", exception)
            }
        }

        private val onCloseButtonListener = DialogInterface.OnClickListener { dialog, which ->
            try {

                dialog.dismiss()
            } catch (exception: Exception) {

                nrError("closeButtonlistener", "DialogHelper", exception)
            }
        }

        /**
         * Dismiss dialog if app running boolean.
         *
         * @param appCompatActivity the appCompatActivity
         * @param dialog  the dialog
         * @return the boolean
         */
        fun dismissDialogIfAppRunning(appCompatActivity: AppCompatActivity, dialog: DialogInterface): Boolean {
            var result = false
            try {

                dialog.dismiss()
                result = true
            } catch (var4: Exception) {

                nrError( "dismissDialogIfAppRunning", "DialogHelper",var4)
            }

            return result
        }

        /**
         * Show no gps dialog boolean.
         *
         * @param appCompatActivity the appCompatActivity
         * @param theme   the theme
         * @return the boolean
         */
        fun showNoGpsDialog(appCompatActivity: AppCompatActivity, theme: Int?): Boolean {
            if (!noGpsDialogActive) {
                noGpsDialogActive = true
                showAlertDialog(appCompatActivity, com.tandeminnovation.appbase.R.string.error_title, com.tandeminnovation.appbase.R.string.error_no_GPS,
                        com.tandeminnovation.appbase.R.string.hint_close, noGPSDialogCloseListener, theme)
            }

            return noGpsDialogActive
        }

        /**
         * Show alert dialog.
         *
         * @param appCompatActivity                   the appCompatActivity
         * @param titleResource             the title resource
         * @param message                   the message
         * @param dismissButtonTextResource the dismiss button text resource
         * @param theme                     the theme
         */
        fun showAlertDialog(appCompatActivity: AppCompatActivity, titleResource: Int, message: Spannable, dismissButtonTextResource: Int, theme: Int?) {
            val builder = buildAlertDialogBuilder(appCompatActivity, titleResource, true, Integer.valueOf(dismissButtonTextResource), theme)
            builder.setMessage(message)
            showDialogFragment(appCompatActivity, createAlertDialogFragment(builder.create()), "")
        }

        /**
         * Show alert dialog.
         *
         * @param appCompatActivity                   the appCompatActivity
         * @param titleResource             the title resource
         * @param message                   the message
         * @param dismissButtonTextResource the dismiss button text resource
         * @param theme                     the theme
         */
        fun showAlertDialog(appCompatActivity: AppCompatActivity, titleResource: Int, message: String, dismissButtonTextResource: Int, theme: Int?) {
            val builder = buildAlertDialogBuilder(appCompatActivity, titleResource, true, Integer.valueOf(dismissButtonTextResource), theme)
            builder.setMessage(message)
            showDialogFragment(appCompatActivity, createAlertDialogFragment(builder.create()), "")
        }

        /**
         * Show alert dialog.
         *
         * @param appCompatActivity                   the appCompatActivity
         * @param titleResource             the title resource
         * @param messageResource           the message resource
         * @param dismissButtonTextResource the dismiss button text resource
         * @param theme                     the theme
         */
        fun showAlertDialog(appCompatActivity: AppCompatActivity, titleResource: Int, messageResource: Int, dismissButtonTextResource: Int,
                            theme: Int?) {
            val builder = buildAlertDialogBuilder(appCompatActivity, titleResource, true, Integer.valueOf(dismissButtonTextResource), theme)
            builder.setMessage(messageResource)
            showDialogFragment(appCompatActivity, createAlertDialogFragment(builder.create()), "")
        }

        /**
         * Show alert dialog.
         *
         * @param appCompatActivity            the appCompatActivity
         * @param titleResource      the title resource
         * @param messageResource    the message resource
         * @param buttonTextResource the button text resource
         * @param clickListener      the click listener
         * @param theme              the theme
         */
        fun showAlertDialog(appCompatActivity: AppCompatActivity, titleResource: Int, messageResource: Int, buttonTextResource: Int,
                            clickListener: DialogInterface.OnClickListener, theme: Int?) {
            val builder = buildAlertDialogBuilder(appCompatActivity, titleResource, false, null, theme)
            builder.setMessage(messageResource)
            builder.setNeutralButton(buttonTextResource, clickListener)
            showDialogFragment(appCompatActivity, createAlertDialogFragment(builder.create()), "")
        }

        /**
         * Show alert dialog.
         *
         * @param appCompatActivity                    the appCompatActivity
         * @param titleResource              the title resource
         * @param messageResource            the message resource
         * @param positiveButtonTextResource the positive button text resource
         * @param negativeButtonTextResource the negative button text resource
         * @param positiveClickListener      the positive click listener
         * @param theme                      the theme
         */
        fun showAlertDialog(appCompatActivity: AppCompatActivity, titleResource: Int, messageResource: Int, positiveButtonTextResource: Int,
                            negativeButtonTextResource: Int, positiveClickListener: DialogInterface.OnClickListener, theme: Int?) {
            showAlertDialog(appCompatActivity, titleResource, messageResource, positiveButtonTextResource, negativeButtonTextResource,
                    positiveClickListener, onCloseButtonListener, theme)
        }

        /**
         * Show alert dialog.
         *
         * @param appCompatActivity                    the appCompatActivity
         * @param titleResource              the title resource
         * @param messageResource            the message resource
         * @param positiveButtonTextResource the positive button text resource
         * @param negativeButtonTextResource the negative button text resource
         * @param positiveClickListener      the positive click listener
         * @param negativeClickListener      the negative click listener
         * @param theme                      the theme
         */
        fun showAlertDialog(appCompatActivity: AppCompatActivity, titleResource: Int, messageResource: Int, positiveButtonTextResource: Int,
                            negativeButtonTextResource: Int, positiveClickListener: DialogInterface.OnClickListener,
                            negativeClickListener: DialogInterface.OnClickListener, theme: Int?) {
            val builder = buildAlertDialogBuilder(appCompatActivity, titleResource, false, null, theme)
            builder.setMessage(messageResource)
            builder.setPositiveButton(positiveButtonTextResource, positiveClickListener)
            builder.setNegativeButton(negativeButtonTextResource, negativeClickListener)
            showDialogFragment(appCompatActivity, createAlertDialogFragment(builder.create()), "")
        }

        /**
         * Show alert dialog.
         *
         * @param appCompatActivity                   the appCompatActivity
         * @param titleResource             the title resource
         * @param contentView               the content view
         * @param dismissButtonTextResource the dismiss button text resource
         * @param theme                     the theme
         */
        fun showAlertDialog(appCompatActivity: AppCompatActivity, titleResource: Int, contentView: View, dismissButtonTextResource: Int,
                            theme: Int?) {
            val builder = buildAlertDialogBuilder(appCompatActivity, titleResource, true, Integer.valueOf(dismissButtonTextResource), theme)
            builder.setView(contentView)
            showDialogFragment(appCompatActivity, createAlertDialogFragment(builder.create()), "")
        }

        /**
         * Show alert dialog.
         *
         * @param appCompatActivity            the appCompatActivity
         * @param titleResource      the title resource
         * @param contentView        the content view
         * @param buttonTextResource the button text resource
         * @param clickListener      the click listener
         * @param theme              the theme
         */
        fun showAlertDialog(appCompatActivity: AppCompatActivity, titleResource: Int, contentView: View, buttonTextResource: Int,
                            clickListener: DialogInterface.OnClickListener, theme: Int?) {
            val builder = buildAlertDialogBuilder(appCompatActivity, titleResource, false, null, theme)
            builder.setView(contentView)
            builder.setNeutralButton(buttonTextResource, clickListener)
            showDialogFragment(appCompatActivity, createAlertDialogFragment(builder.create()), "")
        }

        /**
         * Show alert dialog.
         *
         * @param appCompatActivity                    the appCompatActivity
         * @param titleResource              the title resource
         * @param contentView                the content view
         * @param positiveButtonTextResource the positive button text resource
         * @param negativeButtonTextResource the negative button text resource
         * @param positiveClickListener      the positive click listener
         * @param theme                      the theme
         */
        fun showAlertDialog(appCompatActivity: AppCompatActivity, titleResource: Int, contentView: View, positiveButtonTextResource: Int,
                            negativeButtonTextResource: Int, positiveClickListener: DialogInterface.OnClickListener, theme: Int?) {
            showAlertDialog(appCompatActivity, titleResource, contentView, positiveButtonTextResource, negativeButtonTextResource, positiveClickListener,
                    onCloseButtonListener, theme)
        }

        /**
         * Show alert dialog.
         *
         * @param appCompatActivity                    the appCompatActivity
         * @param titleResource              the title resource
         * @param contentView                the content view
         * @param positiveButtonTextResource the positive button text resource
         * @param negativeButtonTextResource the negative button text resource
         * @param positiveClickListener      the positive click listener
         * @param negativeClickListener      the negative click listener
         * @param theme                      the theme
         */
        fun showAlertDialog(appCompatActivity: AppCompatActivity, titleResource: Int, contentView: View, positiveButtonTextResource: Int,
                            negativeButtonTextResource: Int, positiveClickListener: DialogInterface.OnClickListener,
                            negativeClickListener: DialogInterface.OnClickListener, theme: Int?) {
            val builder = buildAlertDialogBuilder(appCompatActivity, titleResource, false, null, theme)
            builder.setView(contentView)
            builder.setPositiveButton(positiveButtonTextResource, positiveClickListener)
            builder.setNegativeButton(negativeButtonTextResource, negativeClickListener)
            showDialogFragment(appCompatActivity, createAlertDialogFragment(builder.create()), "")
        }

        /**
         * Create alert dialog fragment dialog fragment.
         *
         * @param dialog the dialog
         * @return the dialog fragment
         */
        fun createAlertDialogFragment(dialog: Dialog): DialogFragment {
            val dialogFragment = CommonDialogFragment()
            dialogFragment.setupDialog(dialog, DialogFragment.STYLE_NO_INPUT)

            return dialogFragment
        }

        /**
         * Show dialog fragment.
         *
         * @param currentActivity the current activity
         * @param dialog          the dialog
         * @param tag             the tag
         */
        fun showDialogFragment(currentActivity: AppCompatActivity, dialog: DialogFragment, tag: String) {
            currentActivity.runOnUiThread { dialog.show(currentActivity.supportFragmentManager, tag) }
        }

        /**
         * Show dialog fragment.
         *
         * @param currentActivity the current activity
         * @param dialog          the dialog
         * @param tag             the tag
         */
        fun showDialogFragment(currentActivity: androidx.fragment.app.FragmentActivity, dialog: androidx.fragment.app.DialogFragment,
                               tag: String) {
            currentActivity.runOnUiThread { dialog.show(currentActivity.supportFragmentManager, tag) }
        }

        /**
         * Build alert dialog builder alert dialog . builder.
         *
         * @param context                   the context
         * @param titleResource             the title resource
         * @param addDismissButton          the add dismiss button
         * @param dismissButtonTextResource the dismiss button text resource
         * @param theme                     the theme
         * @return the alert dialog . builder
         */
        fun buildAlertDialogBuilder(context: Activity, titleResource: Int, addDismissButton: Boolean, dismissButtonTextResource: Int?, theme: Int?): AlertDialog.Builder {
            val builder = if (theme == null) AlertDialog.Builder(context, DEFAULT_THEME) else AlertDialog.Builder(context, theme.toInt())
            builder.setCancelable(true)
            builder.setTitle(titleResource)
            if (addDismissButton) {

                builder.setNeutralButton(dismissButtonTextResource!!.toInt(), onCloseButtonListener)
            }

            return builder
        }

        /**
         * Dismiss dialog boolean.
         *
         * @param appCompatActivity the appCompatActivity
         * @param dialog  the dialog
         * @return the boolean
         */
        fun dismissDialog(appCompatActivity: AppCompatActivity, dialog: Dialog): Boolean {

            return dismissDialogIfAppRunning(appCompatActivity, dialog)
        }
    }
}
