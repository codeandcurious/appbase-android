/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.helper

import android.content.Context
import android.net.Uri
import android.os.Environment

import java.io.File

/**
 * The type Path helper.
 */
object PathHelper {

    /**
     * Gets picture folder.
     *
     * @param context      the context
     * @param publicFolder the public folder
     * @return the picture folder
     */
    fun getPictureFolder(context: Context, publicFolder: Boolean): File? {

        val result: File?

        if (publicFolder) {

            result = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
        } else {

            result = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        }

        return result
    }

    /**
     * Gets file action view path.
     *
     * @param file the file
     * @return the file action view path
     */
    fun getFileActionViewPath(file: File): String {

        return Uri.fromFile(file).toString()
    }
}