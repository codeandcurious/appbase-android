/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.helper

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.tandeminnovation.appbase.Security.AESCrypt
import com.tandeminnovation.appbase.extension.whenNotNull
import java.util.*

/**
 * The type Preferences helper.
 */
open class PreferencesHelper {
    companion object {

        private fun getPreferences(context: Context): SharedPreferences {

            return PreferenceManager.getDefaultSharedPreferences(context)
        }

        /**
         * Remove preferences.
         *
         * @param context the context
         * @param key  the key
         */
        fun removePreferences(context: Context, key: String) {
            getPreferences(context).edit { remove(key) }
        }

        /**
         * Gets preferences.
         *
         * @param context         the context
         * @param key          the key
         * @param defaultValue the default value
         * @return the preferences
         */
        fun getPreferences(context: Context, key: String, defaultValue: String?, password: String = String()): String? {

            return getPreferences(context, key, defaultValue as Any?, password)?.toString()
        }

        /**
         * Gets preferences.
         *
         * @param context         the context
         * @param key          the key
         * @param defaultValue the default value
         * @return the preferences
         */
        fun getPreferences(context: Context, key: String, defaultValue: Date?, password: String = String()): Date? {
            val storedPreference = this.getPreferences(context, key, defaultValue as Any?, password)?.toString()
            var date: Date? = null
            whenNotNull(storedPreference) { preference: String ->

                date = Date()
                date?.time = preference.toLong()
            }

            return date
        }

        /**
         * Gets preferences.
         *
         * @param context         the context
         * @param key          the key
         * @param defaultValue the default value
         * @return the preferences
         */
        fun getPreferences(context: Context, key: String, defaultValue: Boolean?, password: String = String()): Boolean? {

            return this.getPreferences(context, key, defaultValue as Any?, password)?.toString()?.toBoolean()
        }

        /**
         * Gets preferences.
         *
         * @param context         the context
         * @param key          the key
         * @param defaultValue the default value
         * @return the preferences
         */
        fun getPreferences(context: Context, key: String, defaultValue: Int?, password: String = String()): Int? {

            return this.getPreferences(context, key, defaultValue as Any?, password)?.toString()?.toInt()
        }

        /**
         * Gets preferences.
         *
         * @param context         the context
         * @param key          the key
         * @param defaultValue the default value
         * @return the preferences
         */
        fun getPreferences(context: Context, key: String, defaultValue: Long?, password: String = String()): Long? {

            return this.getPreferences(context, key, defaultValue as Any?, password)?.toString()?.toLong()
        }

        fun getPreferences(context: Context, key: String, defaultValue: Any?, password: String): Any? {
            var result: Any? = defaultValue
            val storedPreference = getPreferences(context).getString(key, String())
            when {
                !storedPreference.isNullOrEmpty() && password.isNotEmpty() -> {

                    val firstInstallTime = context.packageManager.getPackageInfo(context.packageName, 0).firstInstallTime
                    val ivBytes: ByteArray = "$firstInstallTime${context.packageName}".toByteArray().sliceArray(IntRange(0, 15))
                    val decrypted = AESCrypt.decrypt(ivBytes, password, storedPreference)
                    when {
                        !decrypted.isEmpty() -> result = decrypted
                    }
                }
                !storedPreference.isNullOrEmpty() -> result = storedPreference
            }

            return result
        }

        fun setPreferences(context: Context, key: String, value: Any, password: String = String()) {
            var result: String = when (value) {
                is Date -> value.time.toString()
                else -> value.toString()
            }
            when {
                !password.isEmpty() -> {

                    val firstInstallTime = context.packageManager.getPackageInfo(context.packageName, 0).firstInstallTime
                    val ivBytes: ByteArray = "$firstInstallTime${context.packageName}".toByteArray().sliceArray(IntRange(0, 15))
                    result = AESCrypt.encrypt(ivBytes, password, result)
                }
            }
            store(context, key, result)
        }

        private fun store(context: Context, key: String, result: String) {
            getPreferences(context).edit { putString(key, result) }
        }
    }


}

inline fun SharedPreferences.edit(func: SharedPreferences.Editor.() -> Unit) {
    val editor = edit()
    editor.func()
    editor.apply()
}
