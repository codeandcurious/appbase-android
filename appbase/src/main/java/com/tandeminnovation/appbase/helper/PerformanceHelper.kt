/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.helper

import java.util.concurrent.TimeUnit

/**
 * Created by firetrap on 19/06/16.
 */
/**
 * Instantiates a new Performance helper.
 */
class PerformanceHelper {

    private var startTime: Long = 0
    private var stopTime: Long = 0
    private val mIntervalsMap: LinkedHashMap<String, Long> = linkedMapOf()

    /**
     * Star time.
     */
    fun starTime() {
        startTime = System.currentTimeMillis()
    }

    /**
     * Stop time.
     */
    fun stopTime() {
        stopTime = System.currentTimeMillis()
    }

    /**
     * Adds a time interval
     */
    fun addInterval(intervalTag: String = "Interval ${mIntervalsMap.size}") {

        mIntervalsMap[intervalTag] = System.currentTimeMillis()
    }

    /**
     * Clear all times
     */
    fun clearSession() {
        startTime = 0
        stopTime = 0
        mIntervalsMap.clear()
    }

    /**
     * Gets elapsed time.
     *
     * @param timeUnit the time unit
     * @return the elapsed time
     */
    fun getElapsedTime(timeUnit: TimeUnit): Long {
        val elapsedTime = stopTime - startTime

        return convertTime(elapsedTime, timeUnit)
    }

    /**
     * Generate a report for all the saved times in a session
     */
    fun getFullTimeReport(timeUnit: TimeUnit): String {

        return StringBuilder().let {
            it.append("Elapsed Time: ${getElapsedTime(timeUnit)} \n")
            it.append("Start Time: ${convertTime(startTime, timeUnit)} \n")
            for (entry in mIntervalsMap.entries) {

                it.append("${entry.key}: ${convertTime(entry.value, timeUnit)} \n")
            }
            it.append("Stop time: ${convertTime(stopTime, timeUnit)} \n")
            it.toString()
        }
    }


    /**
     * Converts a time in a specified time unit
     */
    fun convertTime(time: Long, timeUnit: TimeUnit): Long {

        return when (timeUnit) {

            TimeUnit.DAYS -> TimeUnit.MILLISECONDS.toDays(time)

            TimeUnit.HOURS -> TimeUnit.MILLISECONDS.toHours(time)

            TimeUnit.MICROSECONDS -> TimeUnit.MILLISECONDS.toMicros(time)

            TimeUnit.MINUTES -> TimeUnit.MILLISECONDS.toMinutes(time)

            TimeUnit.NANOSECONDS -> TimeUnit.MILLISECONDS.toNanos(time)

            TimeUnit.SECONDS -> TimeUnit.MILLISECONDS.toSeconds(time)

            else -> time
        }
    }
}
