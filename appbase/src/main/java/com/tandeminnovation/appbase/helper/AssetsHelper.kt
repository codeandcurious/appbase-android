/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.helper

import android.content.Context
import android.graphics.Typeface
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import java.util.*

/**
 * The type Assets helper.
 */
object AssetsHelper {

    /**
     * The constant ROBOTO_LIGHT.
     */
    const val ROBOTO_LIGHT = "fonts/Roboto-Light.ttf"

    private val fonts = HashMap<String, Typeface>()

    /**
     * Gets type face.
     *
     * @param context  the context
     * @param fontPath the font path
     * @return the type face
     */
    fun getTypeFace(context: Context, fontPath: String): Typeface? {

        if (!fonts.containsKey(fontPath)) {

            val font = Typeface.createFromAsset(context.assets, fontPath)
            fonts[fontPath] = font
        }

        return fonts[fontPath]
    }

    /**
     * Sets font.
     *
     * @param view the view
     * @param font the font
     */
    fun setFont(view: View, font: Typeface) {

        if (view is ViewGroup) {
            for (i in 0 until view.childCount) {
                setFont(view.getChildAt(i), font)
            }
        } else if (view is TextView) {
            view.typeface = font
        }
    }
}
