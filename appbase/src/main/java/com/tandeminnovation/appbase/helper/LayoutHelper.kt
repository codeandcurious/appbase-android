/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.helper

import android.content.Context
import android.view.LayoutInflater
import android.view.View

/**
 * The type Layout helper.
 */
object LayoutHelper {

    /**
     * Inflate view view.
     *
     * @param context      the context
     * @param layoutResxId the layout resx id
     * @return the view
     */
    fun inflateView(context: Context, layoutResxId: Int): View {

        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        return inflateView(inflater, layoutResxId)
    }

    /**
     * Inflate view view.
     *
     * @param inflater     the inflater
     * @param layoutResxId the layout resx id
     * @return the view
     */
    fun inflateView(inflater: LayoutInflater, layoutResxId: Int): View {

        return inflater.inflate(layoutResxId, null, false)
    }
}
