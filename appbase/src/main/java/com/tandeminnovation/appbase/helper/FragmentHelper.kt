/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.helper

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction

/**
 * Created by Fabio Barreiros on 26/09/2015.
 */
class FragmentHelper private constructor() {

    /**
     * Replace fragment.
     *
     * @param appCompatActivity        the appCompatActivity
     * @param containerLayout the container layout
     * @param fragment        the fragment
     * @param fragmentTag     the fragment tag
     * @param animEnterResx   the anim enter resx
     * @param animExitResx    the anim exit resx
     */
    fun replaceFragment(appCompatActivity: AppCompatActivity, containerLayout: Int, fragment: Fragment, fragmentTag: String?, animEnterResx: Int?, animExitResx: Int?) {
        val activeFragment = getActiveFragment(appCompatActivity)
        try {

            val fragmentTransaction = getFragmentTransaction(appCompatActivity, fragmentTag, animEnterResx, animExitResx)
            fragmentTransaction.replace(containerLayout, fragment, fragmentTag).commit()

        } catch (exception: NullPointerException) {

            val errorTag = when {

                fragmentTag != null -> fragmentTag
                else -> "No fragment tag available"
            }
            nrError("ApiError replacing fragment", errorTag, exception)
        }

    }

    /**
     * Add fragment.
     *
     * @param appCompatActivity        the appCompatActivity
     * @param containerLayout the container layout
     * @param fragment        the fragment
     * @param fragmentTag     the fragment tag
     * @param animEnterResx   the anim enter resx
     * @param animExitResx    the anim exit resx
     */
    fun addFragment(appCompatActivity: AppCompatActivity, containerLayout: Int, fragment: Fragment, fragmentTag: String?, animEnterResx: Int?, animExitResx: Int?) {

        val fragmentTransaction = getFragmentTransaction(appCompatActivity, fragmentTag, animEnterResx, animExitResx)
        fragmentTransaction.add(containerLayout, fragment, fragmentTag).commit()
    }

    /**
     * Add fragment.
     *
     * @param rootFragment        the rootFragment
     * @param containerLayout the container layout
     * @param fragment        the fragment
     * @param fragmentTag     the fragment tag
     * @param animEnterResx   the anim enter resx
     * @param animExitResx    the anim exit resx
     */
    fun addChildFragment(rootFragment: Fragment, containerLayout: Int, fragment: Fragment, fragmentTag: String?, animEnterResx: Int?, animExitResx: Int?) {

        val fragmentTransaction = getChildFragmentTransaction(rootFragment, fragmentTag, animEnterResx, animExitResx)
        fragmentTransaction.add(containerLayout, fragment, fragmentTag).commit()
    }

    /**
     * Add fragment.
     *
     * @param rootFragment        the rootFragment
     * @param containerLayout the container layout
     * @param fragment        the fragment
     * @param fragmentTag     the fragment tag
     * @param animEnterResx   the anim enter resx
     * @param animExitResx    the anim exit resx
     */
    fun replaceChildFragment(rootFragment: Fragment, containerLayout: Int, fragment: Fragment, fragmentTag: String?, animEnterResx: Int?, animExitResx: Int?) {

        val fragmentTransaction = getChildFragmentTransaction(rootFragment, fragmentTag, animEnterResx, animExitResx)
        fragmentTransaction.replace(containerLayout, fragment, fragmentTag).commit()
    }


    /**
     * Remove fragment.
     *
     * @param appCompatActivity      the activity
     * @param fragment      the fragment
     * @param animEnterResx the anim enter resx
     * @param animExitResx  the anim exit resx
     */
    fun removeFragment(appCompatActivity: AppCompatActivity, fragment: Fragment, animEnterResx: Int?, animExitResx: Int?) {
        val fragmentManager = appCompatActivity.supportFragmentManager

        val fragmentTransaction = fragmentManager.beginTransaction()

        if (animEnterResx != null && animExitResx != null) {

            fragmentTransaction.setCustomAnimations(animEnterResx, animExitResx)
        }

        fragmentTransaction.remove(fragment).commit()
        fragmentManager.popBackStack()
    }

    private fun getFragmentTransaction(appCompatActivity: AppCompatActivity, fragmentTag: String?, animEnterResx: Int?, animExitResx: Int?): FragmentTransaction {
        val fragmentManager = appCompatActivity.supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()

        if (animEnterResx != null && animExitResx != null) {

            fragmentTransaction.setCustomAnimations(animEnterResx, 0, 0, animExitResx)
        }
        fragmentTransaction.addToBackStack(fragmentTag)

        return fragmentTransaction
    }

    private fun getChildFragmentTransaction(fragment: Fragment, fragmentTag: String?, animEnterResx: Int?, animExitResx: Int?): FragmentTransaction {
        val fragmentManager = fragment.childFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()

        if (animEnterResx != null && animExitResx != null) {

            fragmentTransaction.setCustomAnimations(animEnterResx, 0, 0, animExitResx)
        }
        fragmentTransaction.addToBackStack(fragmentTag)

        return fragmentTransaction
    }

    /**
     * Gets active fragment.
     *
     * @param appCompatActivity the appCompatActivity
     * @return the active fragment
     */
    fun getActiveFragment(appCompatActivity: AppCompatActivity): Fragment? {

        val fragmentManager = appCompatActivity.supportFragmentManager

        if (fragmentManager.backStackEntryCount == 0) {

            return null
        }
        val tag = fragmentManager.getBackStackEntryAt(fragmentManager.backStackEntryCount - 1).name

        return fragmentManager.findFragmentByTag(tag)
    }

    /**
     * Gets fragment by tag.
     *
     * @param appCompatActivity the appCompatActivity
     * @param TAG      the tag
     * @return the fragment by tag
     */
    fun getFragmentByTag(appCompatActivity: AppCompatActivity, TAG: String): Fragment? {
        val fragmentManager = appCompatActivity.supportFragmentManager

        return if (fragmentManager.backStackEntryCount == 0) null else fragmentManager.findFragmentByTag(TAG)
    }

    /**
     * Gets back stack entry.
     *
     * @param appCompatActivity the appCompatActivity
     * @param entryAt  the entry at
     * @return the back stack entry
     */
    fun getBackStackEntry(appCompatActivity: AppCompatActivity, entryAt: Int): Fragment? {

        val fragmentManager = appCompatActivity.supportFragmentManager
        val backEntry = fragmentManager.getBackStackEntryAt(entryAt)

        return fragmentManager.findFragmentByTag(backEntry.name)
    }

    /**
     * Gets last back stack entry.
     *
     * @param appCompatActivity the appCompatActivity
     * @return the last back stack entry
     */
    fun getLastBackStackEntry(appCompatActivity: AppCompatActivity): Fragment? {

        val fragmentManager = appCompatActivity.supportFragmentManager
        val backEntry = fragmentManager.getBackStackEntryAt(fragmentManager.backStackEntryCount - 1)

        return fragmentManager.findFragmentByTag(backEntry.name)
    }

    companion object {
        private const val TAG = "FragmentHelper"

        var instance: FragmentHelper = FragmentHelper()
    }
}