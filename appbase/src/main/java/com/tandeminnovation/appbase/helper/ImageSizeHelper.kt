/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.helper

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Configuration
import android.graphics.Point
import android.view.WindowManager

object ImageSizeHelper {

    private var screenSize: Point? = null

    private const val IMAGES_1080P = "1080p/"
    private const val IMAGES_720P = "720p/"
    private const val IMAGES_480P = "480p/"
    private const val IMAGES_240P = "240p/"
    private const val IMAGES_100P = "100p/"
    private const val IMAGES_THUMBNAIL = "thumbnail/"

    private const val SIZE_240P_LOWER_THRESHOLD = 360 // (480 + 240) /2
    private const val SIZE_480P_LOWER_THRESHOLD = 600 // (720 + 480) /2
    private const val SIZE_720P_LOWER_THRESHOLD = 900 // (1080 + 720) /2
    private const val SIZE_1080P_LOWER_THRESHOLD = 1080

    /**
     * Gets 1080 p image path.
     *
     * @param imagePath the image path
     * @return the 1080 p image path
     */
    fun get1080pImagePath(imagePath: String): String? {

        return getResImagePath(imagePath, IMAGES_1080P)
    }

    /**
     * Gets 720 p image path.
     *
     * @param imagePath the image path
     * @return the 720 p image path
     */
    fun get720pImagePath(imagePath: String): String? {

        return getResImagePath(imagePath, IMAGES_720P)
    }

    /**
     * Gets 480 p image path.
     *
     * @param imagePath the image path
     * @return the 480 p image path
     */
    fun get480pImagePath(imagePath: String): String? {

        return getResImagePath(imagePath, IMAGES_480P)
    }

    /**
     * Gets 240 p image path.
     *
     * @param imagePath the image path
     * @return the 240 p image path
     */
    fun get240pImagePath(imagePath: String): String? {

        return getResImagePath(imagePath, IMAGES_240P)
    }

    /**
     * Gets 100 p image path.
     *
     * @param imagePath the image path
     * @return the 100 p image path
     */
    fun get100pImagePath(imagePath: String): String? {

        return getResImagePath(imagePath, IMAGES_100P)
    }

    /**
     * Gets thumbnail image path.
     *
     * @param imagePath the image path
     * @return the thumbnail image path
     */
    fun getThumbnailImagePath(imagePath: String): String? {

        return getResImagePath(imagePath, IMAGES_THUMBNAIL)
    }

    /**
     * Gets optimal image path.
     *
     * @param context   the context
     * @param imagePath the image path
     * @return the optimal image path
     */
    fun getOptimalImagePath(context: Context, imagePath: String): String? {

        var result: String? = null

        val pathArray = getPathArray(imagePath)

        val screen = getScreenSize(context)
        val smallestDimension = Math.min(screen.x, screen.y)
        result = when {

            smallestDimension < SIZE_240P_LOWER_THRESHOLD -> get240pImagePath(imagePath)
            smallestDimension < SIZE_480P_LOWER_THRESHOLD -> get480pImagePath(imagePath)
            smallestDimension < SIZE_720P_LOWER_THRESHOLD -> get720pImagePath(imagePath)
            else -> get1080pImagePath(imagePath)
        }

        return result
    }

    private fun getPathArray(imagePath: String?): Array<String>? {
        var pathArray: Array<String>? = null
        if (imagePath != null && !imagePath.isEmpty()) {

            pathArray = arrayOf(imagePath.substring(0, imagePath.lastIndexOf("/") + 1), imagePath.substring(imagePath.lastIndexOf("/") + 1))
        }

        return pathArray
    }

    private fun getResImagePath(imagePath: String, imageSize: String): String? {
        var result: String? = null
        val pathArray = getPathArray(imagePath)
        if (pathArray != null && pathArray.size > 0) {

            result = getCorrectPath(pathArray[0]) + imageSize + pathArray[1]
        }
        return result
    }

    private fun getCorrectPath(path: String): String {
        return if (path[path.length - 1].toString().equals("/", ignoreCase = true))
            path
        else
            "$path/"

    }

    /**
     * Gets screen size.
     *
     * @param context the context
     * @return the screen size
     */
    @SuppressLint("NewApi")
    fun getScreenSize(context: Context): Point {
        if (screenSize == null) {

            val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            val display = windowManager.defaultDisplay

            val size = Point()
            display.getSize(size)

            screenSize = size
        }

        return screenSize as Point
    }

    /**
     * Gets screen layout size.
     *
     * @param context the context
     * @return the screen layout size
     */
    fun getScreenLayoutSize(context: Context): String {
        val screenSize = context.resources.configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK

        when (screenSize) {
            Configuration.SCREENLAYOUT_SIZE_XLARGE -> return IMAGES_1080P

            Configuration.SCREENLAYOUT_SIZE_LARGE -> return IMAGES_720P

            Configuration.SCREENLAYOUT_SIZE_NORMAL -> return IMAGES_480P

            Configuration.SCREENLAYOUT_SIZE_SMALL -> return IMAGES_480P

            else -> return IMAGES_720P
        }
    }

    private fun getResText(context: Context, textResx: Int?): String {

        return (if (textResx != null) context.resources.getText(textResx) else null) as String
    }
}