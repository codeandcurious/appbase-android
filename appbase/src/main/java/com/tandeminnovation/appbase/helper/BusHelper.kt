package com.tandeminnovation.appbase.helper

import com.tandeminnovation.appbase.base.ActionBase
import com.tandeminnovation.appbase.eventbus.action.InitializeAction
import com.tandeminnovation.appbase.eventbus.event.OnApiCallEvent
import com.tandeminnovation.appbase.eventbus.event.OnApiCallFinishedEvent
import com.tandeminnovation.appbase.eventbus.event.OnApiCallProgressUpdateEvent
import com.tandeminnovation.appbase.eventbus.event.OnInitializedEvent
import org.greenrobot.eventbus.EventBus
import java.util.*

/**
 * The type Bus helper.
 */
class BusHelper private constructor() {

//    private var instance: EventBus? = null
//
//    private val bus: EventBus
//        get() {
//            when (instance) {
//                null -> instance = EventBus.getDefault()
//            }
//
//            return instance!!
//        }

    // App specific

    private var initializationCount = 0

    /**
     * Is initialized boolean.
     *
     * @return the boolean
     */
    val isInitialized: Boolean
        get() = getSticky(OnInitializedEvent::class.java) != null
    private val apiCallCountByTag = HashMap<String, Int>()

    private val apiCallProgressUpdateSentDateByTag = HashMap<String, Long>()

    /**
     * On initialization started.
     */
    fun onInitializationStarted() {

        initializationCount++
    }

    /**
     * On initialized.
     */
    fun OnInitialized() {

        initializationCount--

        if (initializationCount == 0) {

            postSticky(OnInitializedEvent())
            debug("OnInitialized Event Sent", "BusHelper")
        }
    }

    /**
     * Has api call boolean.
     *
     * @param tag the tag
     * @return the boolean
     */
    @JvmOverloads
    fun hasAPICall(tag: String = DEFAULT_TAG): Boolean {

        val currentCount = apiCallCountByTag[tag]
        return currentCount != null && currentCount > 0
    }

    /**
     * On api call.
     *
     * @param tag the tag
     */
    fun onApiCall(tag: String) {

        onApiCall(null, tag)
    }

    /**
     * On api call.
     *
     * @param action the action
     * @param tag    the tag
     */
    @JvmOverloads
    fun onApiCall(action: ActionBase? = null, tag: String = DEFAULT_TAG) {

        var currentCount = apiCallCountByTag[tag]
        currentCount = currentCount ?: 0

        if (currentCount == 0) {

            postSticky(OnApiCallEvent(action!!, tag))
            debug("onApiCall Event Send", "BusHelper")
        }

        currentCount++
        apiCallCountByTag[tag] = currentCount
        debug("onApiCall Count $currentCount", "BusHelper")
    }

    /**
     * On api call finished.
     *
     * @param tag the tag
     */
    fun onApiCallFinished(tag: String) {

        onApiCallFinished(null, tag)
    }

    /**
     * On api call finished.
     *
     * @param action the action
     * @param tag    the tag
     */
    @JvmOverloads
    fun onApiCallFinished(action: ActionBase? = null, tag: String = DEFAULT_TAG) {

        var currentCount = apiCallCountByTag[tag]
        currentCount = currentCount ?: 0

        if (currentCount == 1) {

            if (apiCallCountByTag.values.size == 1) {

                removeSticky(OnApiCallEvent::class.java)
            }

            post(OnApiCallFinishedEvent(action!!, tag))
            debug("onApiCallFinished Event Send", "BusHelper")
        }

        currentCount--
        apiCallCountByTag.remove(tag)
        //        apiCallCountByTag.put(tag, currentCount);
        debug("onApiCallFinished Count $currentCount", "BusHelper")
    }

    // * force para enviar
    // * enviar sempre em 100%
    // * controlar a cadencia com timer aqui
    // * guardar a data do ultimo envio
    // * setter para update o default

    /**
     * On api call progress update.
     *
     * @param progress the progress
     */
    fun onApiCallProgressUpdate(progress: Int) {

        onApiCallProgressUpdate(null, DEFAULT_TAG, progress, false)
    }

    /**
     * On api call progress update.
     *
     * @param action   the action
     * @param progress the progress
     */
    fun onApiCallProgressUpdate(action: ActionBase, progress: Int) {

        onApiCallProgressUpdate(action, DEFAULT_TAG, progress, false)
    }

    /**
     * On api call progress update.
     *
     * @param action    the action
     * @param tag       the tag
     * @param progress  the progress
     * @param forceSend the force send
     */
    fun onApiCallProgressUpdate(action: ActionBase?, tag: String, progress: Int, forceSend: Boolean) {

        var lastSentDate = apiCallProgressUpdateSentDateByTag[tag]
        lastSentDate = lastSentDate ?: 0L

        if (forceSend || progress == 100 || lastSentDate + DEFAULT_PROGESS_UPDATE_FREQUENCY_IN_MILLIS < System.currentTimeMillis()) {

            post(OnApiCallProgressUpdateEvent(action!!, tag, progress))

            if (progress == 100) {

                apiCallProgressUpdateSentDateByTag.remove(tag)
                debug("onApiCallProgressUpdate Event Send and removed (reached 100)", "BusHelper")
            } else {

                apiCallProgressUpdateSentDateByTag[tag] = System.currentTimeMillis()
                debug("onApiCallProgressUpdate Event Send", "BusHelper")
            }
        }
    }

    companion object {

        private val bus: EventBus by lazy { EventBus.getDefault() }

        fun post(event: Any) {

            bus.post(event)
        }

        fun postSticky(event: Any) {

            bus.postSticky(event)
        }

        fun removeSticky(eventType: Class<*>) {

            bus.removeStickyEvent(eventType)
        }

        fun register(clazz: Any) {

            bus.register(clazz)
        }

        fun unregister(clazz: Any) {

            bus.unregister(clazz)
        }

        fun isRegistered(clazz: Any): Boolean {
            return bus.isRegistered(clazz)
        }

        fun getSticky(eventType: Class<*>): Any? {

            return bus.getStickyEvent(eventType)
        }

        fun sendOnInitializeAction() {

            bus.post(InitializeAction())
        }

        // API CALL
        private const val DEFAULT_TAG = "#DefaultApiCallTag"

        // PROGRESS UPDATE
        /**
         * The constant DEFAULT_PROGESS_UPDATE_FREQUENCY_IN_MILLIS.
         */
        private const val DEFAULT_PROGESS_UPDATE_FREQUENCY_IN_MILLIS = 250L
    }
}