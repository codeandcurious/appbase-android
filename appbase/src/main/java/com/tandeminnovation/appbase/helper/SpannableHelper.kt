/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.helper

import android.content.Context
import android.content.res.ColorStateList
import android.text.Spannable
import android.text.TextUtils
import android.text.style.*

/**
 * The type Spannable helper.
 */
object SpannableHelper {

    /**
     * Gets spannable.
     *
     * @param text the text
     * @return the spannable
     */
    fun getSpannable(text: String?): Spannable? {
        var result: Spannable? = null
        if (text != null && text.isNotEmpty()) {

            result = Spannable.Factory.getInstance().newSpannable(text)
        }

        return result
    }

    /**
     * Gets underline spannable.
     *
     * @param spannable the spannable
     * @return the underline spannable
     */
    fun getUnderlineSpannable(spannable: Spannable?): Spannable? {

        if (spannable != null) {

            val underlineSpan = UnderlineSpan()
            spannable.setSpan(underlineSpan, 0, spannable.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        }

        return spannable
    }

    /**
     * Gets bullet spannable.
     *
     * @param spannable the spannable
     * @param gapWidth  the gap width
     * @return the bullet spannable
     */
    fun getBulletSpannable(spannable: Spannable?, gapWidth: Int): Spannable? {

        if (spannable != null) {

            val bulletSpan = BulletSpan(gapWidth)
            spannable.setSpan(bulletSpan, 0, spannable.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        }

        return spannable
    }

    /**
     * Gets bold spannable.
     *
     * @param spannable the spannable
     * @return the bold spannable
     */
    fun getBoldSpannable(spannable: Spannable?): Spannable? {

        if (spannable != null) {

            val styleSpan = StyleSpan(android.graphics.Typeface.BOLD)
            spannable.setSpan(styleSpan, 0, spannable.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        }

        return spannable
    }

    /**
     * Gets type face spannable.
     *
     * @param context   the context
     * @param fontPath  the font path
     * @param spannable the spannable
     * @return the type face spannable
     */
    fun getTypeFaceSpannable(context: Context, fontPath: String, spannable: Spannable?): Spannable? {

        if (spannable != null) {

            val typefaceSpan = CustomTypefaceSpan("serif", AssetsHelper.getTypeFace(context, fontPath)!!)
            spannable.setSpan(typefaceSpan, 0, spannable.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        }

        return spannable
    }

    /**
     * Gets image spannable.
     *
     * @param context   the context
     * @param imageResx the image resx
     * @param spannable the spannable
     * @return the image spannable
     */
    fun getImageSpannable(context: Context, imageResx: Int, spannable: Spannable?): Spannable? {

        if (spannable != null) {

            val imageSpan = ImageSpan(context, imageResx, ImageSpan.ALIGN_BASELINE)
            spannable.setSpan(imageSpan, 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        }

        return spannable
    }

    /**
     * Gets color spannable.
     *
     * @param context   the context
     * @param color     the color
     * @param spannable the spannable
     * @return the color spannable
     */
    fun getColorSpannable(context: Context, color: Int, spannable: Spannable?): Spannable? {

        if (spannable != null) {

            val colorSpan = ForegroundColorSpan(color)
            spannable.setSpan(colorSpan, 0, spannable.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        }

        return spannable
    }

    /**
     * Gets color state list spannable.
     *
     * @param context   the context
     * @param color     the color
     * @param spannable the spannable
     * @return the color state list spannable
     */
    fun getColorStateListSpannable(context: Context, color: ColorStateList, spannable: Spannable?): Spannable? {

        if (spannable != null) {

            val colorSpan = TextAppearanceSpan(null, 0, 0, color, null)
            spannable.setSpan(colorSpan, 0, spannable.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        }

        return spannable
    }

    /**
     * Concat char sequence.
     *
     * @param spans the spans
     * @return the char sequence
     */
    fun concat(vararg spans: CharSequence): CharSequence {

        return TextUtils.concat(*spans)
    }
}