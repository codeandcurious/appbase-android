/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.helper

import android.content.Context
import android.text.format.DateFormat
import java.math.BigInteger
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * The type Date helper.
 */
object DateHelper {

    const val DEFAULT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"//"yyyy-MM-dd HH:mm:ss";
    private val SECOND_MILLIS: Long = 1000
    private val MINUTE_MILLIS = 60 * SECOND_MILLIS
    private val HOUR_MILLIS = 60 * MINUTE_MILLIS
    private val DAY_MILLIS = 24 * HOUR_MILLIS
    private val WEEK_MILLIS = 7 * DAY_MILLIS
    private val MONTH_MILLIS = 4 * WEEK_MILLIS
    private val YEAR_MILLIS = 12 * MONTH_MILLIS

    val defaultDateFormat by lazy { SimpleDateFormat(DEFAULT_DATE_FORMAT, Locale.getDefault()) }

    /**
     * Gets current date without time.
     *
     * @return the current date without time
     */
    val currentDateWithoutTime: Date?
        get() = getDateWithoutTime(Date())

    /**
     * Parse date date.
     *
     * @param dateStr the date str
     * @param format  the format
     * @param locale  the locale
     * @return the date
     * @throws ParseException the parse exception
     */
    @Throws(ParseException::class)
    @JvmOverloads
    fun parseDate(dateStr: String, format: String = DEFAULT_DATE_FORMAT, locale: Locale = Locale.getDefault()): Date {
        return SimpleDateFormat(format, locale).parse(dateStr)
    }

    /**
     * Format date string.
     *
     * @param date   the date
     * @param format the format
     * @return the string
     */
    fun formatDate(date: Date, format: String): String {
        return SimpleDateFormat(format, Locale.getDefault()).format(date)
    }

    /**
     * Returns the date (not time) as a string formatted to the current locale
     *
     * @param date the date to be formatted
     * @return String containing the formatted date
     */
    fun formatDate(date: Date): String {
        return defaultDateFormat.format(date)
    }

    /**
     * Gets date plus days.
     *
     * @param baseDate  the base date
     * @param daysToAdd the days to add
     * @return the date plus days
     */
    fun getDateChangedByDays(baseDate: Date, daysToAdd: Int): Date {
        return Calendar.getInstance().apply {
            time = baseDate
            add(Calendar.DAY_OF_YEAR, daysToAdd)
        }.time
    }

    /**
     * Gets date plus months.
     *
     * @param baseDate    the base date
     * @param monthsToAdd the months to add
     * @return the date plus months
     */
    fun getDateChangedByMonths(baseDate: Date, monthsToAdd: Int): Date {
        return Calendar.getInstance().apply {
            time = baseDate
            add(Calendar.MONTH, monthsToAdd)
        }.time
    }

    /**
     * Gets date locale string.
     *
     * @param context the context
     * @param date    the date
     * @return the date locale string
     */
    fun getDateLocaleString(context: Context, date: Date): String? {
        return DateFormat.getDateFormat(context).format(date)
    }

    /**
     * Gets long date locale string.
     *
     * @param context the context
     * @param date    the date
     * @return the long date locale string
     */
    fun getLongDateLocaleString(context: Context, date: Date): String? {
        return DateFormat.getLongDateFormat(context).format(date)
    }

    fun getLongDateLocaleString(date: Date?, locale: Locale): String? {
        return SimpleDateFormat.getDateInstance(SimpleDateFormat.LONG, locale).format(date)
    }

    /**
     * Gets time string.
     *
     * @param context the context
     * @param date    the date
     * @return the time string
     */
    fun getTimeString(context: Context, date: Date?): String? {
        return DateFormat.getTimeFormat(context).format(date)
    }

    /**
     * Gets date without time.
     *
     * @param date the date
     * @return the date without time
     */
    fun getDateWithoutTime(date: Date): Date? {
        val calendar = Calendar.getInstance().apply {
            time = date
        }
        return GregorianCalendar(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).time
    }


    /**
     * Gets year.
     *
     * @param date the date
     * @return the year
     */
    fun getYear(date: Date): Int {
        return Calendar.getInstance().apply { time = date }.get(Calendar.YEAR)
    }

    /**
     * Gets day of month.
     *
     * @param date the date
     * @return the day of month
     */
    fun getDayOfMonth(date: Date): Int {
        return Calendar.getInstance().apply { time = date }.get(Calendar.DAY_OF_MONTH)
    }

    /**
     * Gets day of year.
     *
     * @param date the date
     * @return the day of year
     */
    fun getDayOfYear(date: Date): Int {
        return Calendar.getInstance().apply { time = date }.get(Calendar.DAY_OF_YEAR)
    }

    /**
     * Gets day of week.
     *
     * @param date the date
     * @return the day of week
     */
    fun getDayOfWeek(date: Date): Int {
        return Calendar.getInstance().apply { time = date }.get(Calendar.DAY_OF_WEEK)
    }

    /**
     * Gets month.
     *
     * @param date the date
     * @return the month
     */
    fun getMonth(date: Date): Int {
        return Calendar.getInstance().apply { time = date }.get(Calendar.MONTH)
    }

    /**
     * Gets short month name.
     *
     * @param date the date
     * @return the short month name
     */
    fun getShortMonthName(date: Date): String {
        return Calendar.getInstance().apply { time = date }.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.getDefault())
    }

    /**
     * Gets long month name.
     *
     * @param date the date
     * @return the long month name
     */
    fun getLongMonthName(date: Date): String {
        return Calendar.getInstance().apply { time = date }.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault())
    }

    /**
     * Are equals boolean.
     *
     * @param context the context
     * @param date1   the date 1
     * @param date2   the date 2
     * @return the boolean
     */
    fun areEquals(context: Context, date1: Date, date2: Date): Boolean {

        return getLongDateLocaleString(context, date1).equals(getLongDateLocaleString(context, date2), ignoreCase = true)
    }

    /**
     * Are equals boolean.
     *
     * @param context       the context
     * @param date1         the date 1
     * @param date2         the date 2
     * @param calendarValue the calendar value
     * @return the boolean
     */
    fun areEquals(context: Context, date1: Date, date2: Date, calendarValue: Int): Boolean {
        val previousCalendar = Calendar.getInstance().apply { time = date1 }
        val nextCalendar = Calendar.getInstance().apply { time = date2 }

        return previousCalendar.get(calendarValue) == nextCalendar.get(calendarValue)
    }

    /**
     * Is same month boolean.
     *
     * @param date1 the date 1
     * @param date2 the date 2
     * @return the boolean
     */
    fun isSameMonth(date1: Date, date2: Date): Boolean {
        val previousCalendar = Calendar.getInstance().apply { time = date1 }
        val nextCalendar = Calendar.getInstance().apply { time = date2 }

        return previousCalendar.get(Calendar.YEAR) == nextCalendar.get(Calendar.YEAR) && previousCalendar.get(Calendar.MONTH) == nextCalendar.get(Calendar.MONTH)
    }

    /**
     * Is same day boolean.
     *
     * @param date1 the date 1
     * @param date2 the date 2
     * @return the boolean
     */
    fun isSameDay(date1: Date, date2: Date): Boolean {
        val previousCalendar = Calendar.getInstance().apply { time = date1 }
        val nextCalendar = Calendar.getInstance().apply { time = date2 }

        return previousCalendar.get(Calendar.YEAR) == nextCalendar.get(Calendar.YEAR) && previousCalendar.get(Calendar.DAY_OF_YEAR) == nextCalendar.get(Calendar.DAY_OF_YEAR)
    }

    /**
     * Is between boolean.
     *
     * @param startDate   the start date
     * @param betweenDate the between date
     * @param endDate     the end date
     * @return the boolean
     */
    fun isBetween(startDate: Date, betweenDate: Date, endDate: Date): Boolean {

        return betweenDate.after(startDate) && betweenDate.before(endDate)
    }

    /**
     * Is before boolean.
     *
     * @param startDate the start date
     * @param endDate   the end date
     * @return the boolean
     */
    fun isBefore(startDate: Date, endDate: Date): Boolean {

        return startDate.before(endDate)
    }

    fun getRelativeTimeSpanString(time: Long): String? {
//    if (time < 1000000000000L) {
//        time *= 1000
//    }
//
        val now = System.currentTimeMillis()
        if (time > now || time <= 0) {
            return null
        }
        val diff = now - time
        return when {
            diff < MINUTE_MILLIS -> "agora mesmo"
            diff < 2 * MINUTE_MILLIS -> "há ${diff / MINUTE_MILLIS} minuto"
            diff < 50 * MINUTE_MILLIS -> "há ${diff / MINUTE_MILLIS} minutos"
            diff < 2 * HOUR_MILLIS -> "há ${diff / HOUR_MILLIS} hora"
            diff < 24 * HOUR_MILLIS -> "há ${diff / HOUR_MILLIS} horas"
            diff < 2 * DAY_MILLIS -> "há ${diff / DAY_MILLIS} dia"
            diff < 7 * DAY_MILLIS -> "há ${diff / DAY_MILLIS} dias"
            diff < 1 * WEEK_MILLIS -> "há ${diff / WEEK_MILLIS} semana"
            diff < 4 * WEEK_MILLIS -> "há ${diff / WEEK_MILLIS} semanas"
            diff < 2 * MONTH_MILLIS -> "há ${diff / MONTH_MILLIS} mês"
            diff < 12 * MONTH_MILLIS -> "há ${diff / MONTH_MILLIS} meses"
            diff < 2 * YEAR_MILLIS -> "há ${diff / YEAR_MILLIS} ano"
            else -> "há ${diff / YEAR_MILLIS} anos"
        }
    }
}