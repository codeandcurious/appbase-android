/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.repository

import android.content.Context
import com.tandeminnovation.appbase.base.PreferenceDataRepositoryBase
import com.tandeminnovation.appbase.helper.PreferencesHelper

/**
 * Created by firetrap on 23/05/2017.
 */
class AuthenticationPersistenceRepository private constructor() : PreferenceDataRepositoryBase() {

    /**
     * Gets access token key.
     *
     * @return the access token key
     */
    protected val accessTokenKey: String
        get() = ACCESS_TOKEN

    override fun setup(context: () -> Context) {
        this.getContext = context
    }

    /**
     * Save access token.
     *
     * @param accessToken the access token
     */
    fun saveAccessToken(accessToken: String?) {
        if (!accessToken.isNullOrEmpty()) {

            PreferencesHelper.setPreferences(getContext.invoke(), accessTokenKey, accessToken)
        } else {

            clearAccessToken()
        }
    }

    /**
     * Load access token string.
     *
     * @return the string
     */
    fun loadAccessToken(): String? {

        return PreferencesHelper.getPreferences(getContext.invoke(), accessTokenKey, (null as String?)!!)
    }

    /**
     * Clear access token.
     */
    fun clearAccessToken() {

        PreferencesHelper.removePreferences(getContext.invoke(), accessTokenKey)
    }

    companion object {

        private const val ACCESS_TOKEN = "AccessToken"

        var instance: AuthenticationPersistenceRepository = AuthenticationPersistenceRepository()
    }
}
