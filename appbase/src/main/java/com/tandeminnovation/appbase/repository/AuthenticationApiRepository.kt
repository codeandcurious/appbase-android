/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.repository

import com.tandeminnovation.appbase.api.ApiResponseWrapper
import com.tandeminnovation.appbase.api.AuthenticationApi
import com.tandeminnovation.appbase.base.ApiRepositoryBase

import org.json.JSONException

import java.io.IOException

/**
 * Created by firetrap on 23/05/2017.
 */
class AuthenticationApiRepository private constructor() : ApiRepositoryBase() {

    override fun setup() {}

    /**
     * Gets access token.
     *
     * @param tokenUrl     the token url
     * @param clientSecret the client secret
     * @param clientId     the client id
     * @param scope        the scope
     * @return the access token
     * @throws IOException   the io exception
     * @throws JSONException the json exception
     */
    @Throws(IOException::class, JSONException::class)
    fun getAccessToken(tokenUrl: String, clientSecret: String, clientId: String, scope: String): ApiResponseWrapper {

        return AuthenticationApi().getAccessToken(tokenUrl, clientSecret, clientId, scope)
    }

    companion object {

        var instance: AuthenticationApiRepository = AuthenticationApiRepository()
    }
}
