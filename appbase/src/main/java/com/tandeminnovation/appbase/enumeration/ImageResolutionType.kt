/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.enumeration

import com.tandeminnovation.appbase.base.EnumValueBase

enum class ImageResolutionType(val value: Int) : EnumValueBase {

    Res1080p(1),
    Res720p(2),
    Res480p(3),
    Res240p(4),
    Res100p(5),
    Thumbnail(6),
    Thumbnail200(7),
    Thumbnail300(8)
}
