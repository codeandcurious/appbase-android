/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tandeminnovation.appbase.enumeration

import com.tandeminnovation.appbase.base.EnumValueBase
import com.tandeminnovation.appbase.helper.JsonHelper
import org.json.JSONArray
import org.json.JSONObject
enum class ActiveStatus(val value: String) : EnumValueBase {
    Inactive("Inactive"),
    Test("Test"),
    Active("Active");
    override fun toString(): String {
        return this.value.toString()
    }
    fun toJson(json: JSONObject, key: String) {
        json.put(key, value)
    }
    companion object {
        fun putJsonArray(jsonObject: JSONObject, key: String, enumArray: MutableList<ActiveStatus>) {
            jsonObject.put(key, toJsonArray(enumArray))
        }
        fun toJsonArray(enumArray: MutableList<ActiveStatus>): JSONArray {
            val jsonArray = JSONArray()
            enumArray.forEach {
                jsonArray.put(it.value)
            }
            return jsonArray
        }
        fun fromJsonArray(jsonObject: JSONObject, key: String): MutableList<ActiveStatus>? {
            var result: MutableList<ActiveStatus>? = null
            val keyList = JsonHelper.parseStringList(jsonObject, key)
            if (keyList != null) {
                result = mutableListOf()
                keyList.forEach { enumKey: String ->
                    valueOfKey(enumKey)?.let { result.add(it) }
                }
            }
            return result
        }
        fun fromJson(jsonKey: JSONObject, key: String): ActiveStatus? {
            return valueOfKey(JsonHelper.parseString(jsonKey, key))
        }
        fun valueOfKey(value: String): ActiveStatus? {
            return values().find { it.value == value }
        }
    }
}



