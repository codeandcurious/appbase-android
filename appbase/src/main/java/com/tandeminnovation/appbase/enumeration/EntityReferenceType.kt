/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.enumeration

import com.tandeminnovation.appbase.base.EnumValueBase

enum class EntityReferenceType(val value: Int) : EnumValueBase {

    Poi(10),
    PoiGroup(11),
    PoiCluster(12),
    Event(21),
    EventGroup(22),
    EventCluster(23),
    Item(31),
    ItemGroup(32),
    ItemCluster(33),
    Ar(41),
    ArGroup(42),
    ArCluster(43),
    NewsItem(51),
    NewsGroup(52),
    NewsCluster(53),
    Beacon(61),
    BeaconGroup(62),
    BeaconCluster(63)
}
