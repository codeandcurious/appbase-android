package com.tandeminnovation.appbase.enumeration

enum class MediaLocation {
    Camera,
    FileExplorer,
    Both
}