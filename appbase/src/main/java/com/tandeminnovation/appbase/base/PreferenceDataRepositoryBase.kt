/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.base

import android.content.Context

/**
 * Created by Rui on 18/02/2015.
 */
abstract class PreferenceDataRepositoryBase : RepositoryBase() {

    protected lateinit var getContext: () -> Context
    protected lateinit var getPassword: () -> String

    open fun setup(context: () -> Context) {
        this.getContext = context
    }

    open fun setup(context: () -> Context, password: () -> String) {
        this.getContext = context
        this.getPassword = password
    }
}
