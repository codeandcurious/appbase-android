/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.base

import com.tandeminnovation.appbase.base.ApiCallActionBase.ApiCallActionType

abstract class ApiCallHandlerBase<TArg, TArg2, TResult> {

    /**
     *
     * @param arg TArg
     * @param arg2 TArg2
     * @return TResult
     */
    abstract fun getFromCache(arg: TArg, arg2: TArg2): TResult

    /**
     *
     * @param arg TArg
     * @param arg2 TArg2
     * @param result TResult?
     */
    abstract fun sendResult(arg: TArg, arg2: TArg2, result: TResult?)

    /**
     *
     * @param arg TArg
     * @param arg2 TArg2
     * @return Boolean
     */
    abstract fun getFromApi(arg: TArg, arg2: TArg2): Boolean

    /**
     *
     * @param result TResult?
     * @return Boolean
     */
    fun shouldSend(result: TResult?): Boolean {

        return result != null
    }

    /**
     *
     * @param arg TArg
     * @param arg2 TArg2
     * @param actionType ApiCallActionType
     */
    fun execute(arg: TArg, arg2: TArg2, actionType: ApiCallActionType) {

        var result: TResult? = null

        when (actionType) {
            ApiCallActionBase.ApiCallActionType.Cache -> {
                result = getFromCache(arg, arg2)

                if (shouldSend(result)) {

                    sendResult(arg, arg2, result)
                }
            }
            ApiCallActionBase.ApiCallActionType.CacheOrApi -> {

                result = getFromCache(arg, arg2)
                if (shouldSend(result)) {

                    sendResult(arg, arg2, result)
                } else {

                    getFromApi(arg, arg2)
                }
            }

            ApiCallActionBase.ApiCallActionType.ApiOrCache ->

                if (!getFromApi(arg, arg2)) {

                    result = getFromCache(arg, arg2)

                    if (shouldSend(result)) {

                        sendResult(arg, arg2, result)
                    }
                }
            ApiCallActionBase.ApiCallActionType.Api ->

                getFromApi(arg, arg2)
            ApiCallActionBase.ApiCallActionType.Both -> {

                result = getFromCache(arg, arg2)

                if (shouldSend(result)) {

                    sendResult(arg, arg2, result)
                }

                getFromApi(arg, arg2)
            }
        }
    }
}
