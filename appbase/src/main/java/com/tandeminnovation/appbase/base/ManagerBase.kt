/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.base

import com.tandeminnovation.appbase.eventbus.action.InitializeAction
import com.tandeminnovation.appbase.eventbus.action.OnErrorAction
import com.tandeminnovation.appbase.eventbus.event.OnErrorEvent
import com.tandeminnovation.appbase.eventbus.event.OnInitializedEvent
import com.tandeminnovation.appbase.helper.*
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.io.UnsupportedEncodingException
import java.net.URLEncoder
import java.security.InvalidKeyException
import java.security.NoSuchAlgorithmException
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec

/**
 * Created by Rui on 18/02/2015.
 */
abstract class ManagerBase {

    protected var isInitialized = false

    protected lateinit var encryptionKey: String

    open fun setup() {
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    open fun handleInitializeAction(initializeAction: InitializeAction) {
        sendOnInitializedEvent()
        isInitialized = true
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    protected open fun handleOnErrorAction(onErrorAction: OnErrorAction) {

        val throwable: Throwable? = onErrorAction.throwable
        throwable?.let { nrError(throwable.message, "ManagerBase", it) }
    }

    protected open fun sendOnErrorEvent(action: ActionBase, data: Any): OnErrorEvent {

        val onErrorEvent = OnErrorEvent(action, data)
        BusHelper.post(onErrorEvent)
        return onErrorEvent
    }

    protected open fun sendOnInitializedEvent() {

        BusHelper.post(OnInitializedEvent())
    }

    protected open fun checkInitialization(callerTag: String?) {

        while (!isInitialized) {

            try {

                Thread.sleep(10)
            } catch (ex: InterruptedException) {

                nrError("initialization wait thread interrupted",
                    if (callerTag == null) {
                        "ManagerBase"
                    } else {
                        callerTag
                    },  ex
                )
            }
        }
    }

    protected fun xorEncrypt(input: String): String {
        if (input.isEmpty()) return input
        var result = StringBuilder()
        var key = StringBuilder(encryptionKey)
        if (input.length < key.length) {

            key = StringBuilder(key.substring(0, input.length))
        } else {

            while (key.length < input.length) {

                key.append("A")
            }
        }
        val keyArray = key.toString().toCharArray()
        for (i in 0 until input.length) {

            val asciiValue1 = input[i].toInt()
            val asciiValue2 = keyArray[i].toInt()

            val xorValue = asciiValue1 xor asciiValue2
            result.append(xorValue.toString()).append(".")
        }
        result = StringBuilder(result.substring(0, result.length - 1))

        return result.toString()
    }

    @Throws(NoSuchAlgorithmException::class, UnsupportedEncodingException::class, InvalidKeyException::class)
    protected fun encode(key: String, data: String): String {
        val sha256Hmac = Mac.getInstance("HmacSHA256")
        val secretKey = SecretKeySpec(key.toByteArray(charset("UTF-8")), "HmacSHA256")
        sha256Hmac.init(secretKey)

        val result = Base64Helper.encode(sha256Hmac.doFinal(data.toByteArray(charset("UTF-8"))))

        return URLEncoder.encode(result, "UTF-8")
    }
}