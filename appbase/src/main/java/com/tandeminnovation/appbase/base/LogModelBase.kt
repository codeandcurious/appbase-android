package com.tandeminnovation.appbase.base

import android.os.Parcel
import android.os.Parcelable
import com.tandeminnovation.appbase.helper.JsonHelper
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class LogModelBase : ModelBase {

    // members
    var url: String? = null
    var httpCode: Int? = null
    var returnCode: String? = null
    var returnMessage: String? = null
    var requestStartDate: java.util.Date? = null
    var requestEndDate: java.util.Date? = null
    var requestDiffTime: Long? = null
    var parseStartDate: java.util.Date? = null
    var parseEndDate: java.util.Date? = null
    var parseDiffTime: Long? = null
    var id: Long = 0

    constructor() : super()

    @Throws(JSONException::class)
    constructor(json: JSONObject) : super() {
        this.fromJson(json)
    }

    constructor(`in`: Parcel) : super() {
        this.readFromParcel(`in`)
    }

    @Throws(JSONException::class)
    override fun fromJson(json: JSONObject) {

        if (JsonHelper.hasKey(json, JSON_URL)) {

            this.url = JsonHelper.parseNullableString(json, JSON_URL)
        }

        if (JsonHelper.hasKey(json, JSON_HTTP_CODE)) {

            this.httpCode = JsonHelper.parseNullableInt(json, JSON_HTTP_CODE)
        }

        if (JsonHelper.hasKey(json, JSON_RETURN_CODE)) {

            this.returnCode = JsonHelper.parseNullableString(json, JSON_RETURN_CODE)
        }

        if (JsonHelper.hasKey(json, JSON_RETURN_MESSAGE)) {

            this.returnMessage = JsonHelper.parseNullableString(json, JSON_RETURN_MESSAGE)
        }

        if (JsonHelper.hasKey(json, JSON_REQUEST_START_DATE)) {

            this.requestStartDate = JsonHelper.parseNullableDate(json, JSON_REQUEST_START_DATE)
        }

        if (JsonHelper.hasKey(json, JSON_REQUEST_END_DATE)) {

            this.requestEndDate = JsonHelper.parseNullableDate(json, JSON_REQUEST_END_DATE)
        }

        if (JsonHelper.hasKey(json, JSON_REQUEST_DIFF_TIME)) {

            this.requestDiffTime = JsonHelper.parseNullableLong(json, JSON_REQUEST_DIFF_TIME)
        }

        if (JsonHelper.hasKey(json, JSON_PARSE_START_DATE)) {

            this.parseStartDate = JsonHelper.parseNullableDate(json, JSON_PARSE_START_DATE)
        }

        if (JsonHelper.hasKey(json, JSON_PARSE_END_DATE)) {

            this.parseEndDate = JsonHelper.parseNullableDate(json, JSON_PARSE_END_DATE)
        }

        if (JsonHelper.hasKey(json, JSON_PARSE_DIFF_TIME)) {

            this.parseDiffTime = JsonHelper.parseNullableLong(json, JSON_PARSE_DIFF_TIME)
        }

        if (JsonHelper.hasKey(json, JSON_ID)) {

            this.id = JsonHelper.parseLong(json, JSON_ID)
        }
    }

    @Throws(JSONException::class)
    override fun toJson(): JSONObject {
        val obj = JSONObject()

        obj?.put(JSON_URL, url?.let { JsonHelper.format(it) })

        obj?.put(JSON_HTTP_CODE, httpCode?.let { JsonHelper.format(it) })

        obj?.put(JSON_RETURN_CODE, returnCode?.let { JsonHelper.format(it) })

        obj?.put(JSON_RETURN_MESSAGE, returnMessage?.let { JsonHelper.format(it) })

        obj?.put(JSON_REQUEST_START_DATE, requestStartDate?.let { JsonHelper.format(it) })

        obj?.put(JSON_REQUEST_END_DATE, requestEndDate?.let { JsonHelper.format(it) })

        obj?.put(JSON_REQUEST_DIFF_TIME, requestDiffTime?.let { JsonHelper.format(it) })

        obj?.put(JSON_PARSE_START_DATE, parseStartDate?.let { JsonHelper.format(it) })

        obj?.put(JSON_PARSE_END_DATE, parseEndDate?.let { JsonHelper.format(it) })

        obj?.put(JSON_PARSE_DIFF_TIME, parseDiffTime?.let { JsonHelper.format(it) })

        obj.put(JSON_ID, id?.let { JsonHelper.format(it) })

        return obj
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {

        super.writeToParcel(dest, flags)
    }

    override fun readFromParcel(`in`: Parcel) {

        super.readFromParcel(`in`)
    }

    companion object {

        // json attributes
        protected const val JSON_URL = "url"
        protected const val JSON_HTTP_CODE = "httpCode"
        protected const val JSON_RETURN_CODE = "returnCode"
        protected const val JSON_RETURN_MESSAGE = "returnMessage"
        protected const val JSON_REQUEST_START_DATE = "requestStartDate"
        protected const val JSON_REQUEST_END_DATE = "requestEndDate"
        protected const val JSON_REQUEST_DIFF_TIME = "requestDiffTime"
        protected const val JSON_PARSE_START_DATE = "parseStartDate"
        protected const val JSON_PARSE_END_DATE = "parseEndDate"
        protected const val JSON_PARSE_DIFF_TIME = "parseDiffTime"
        protected const val JSON_ID = "id"


        fun fromJsonObject(jsonObject: JSONObject): LogModelBase {

            val result = LogModelBase()
            result.fromJson(jsonObject)

            return result
        }

        fun toJsonObject(model: LogModelBase): JSONObject {

            return model.toJson()
        }

        @Throws(JSONException::class)
        fun toJsonArray(models: MutableList<LogModelBase>?): JSONArray {
            val result = JSONArray()
            if (models != null) {

                for (i in models.indices) {

                    result.put(models[i].toJson())
                }
            }

            return result
        }

        @Throws(JSONException::class)
        fun fromJsonArray(array: JSONArray?): MutableList<LogModelBase> {

            val models = mutableListOf<LogModelBase>()
            if (array != null) {

                for (i in 0 until array.length()) {

                    models.add(LogModelBase(array.getJSONObject(i)))
                }
            }

            return models
        }

        @JvmField
        val CREATOR: Parcelable.Creator<LogModelBase> = object : Parcelable.Creator<LogModelBase> {

            override fun createFromParcel(`in`: Parcel): LogModelBase {

                return LogModelBase(`in`)
            }

            override fun newArray(size: Int): Array<LogModelBase?> {

                return arrayOfNulls(size)
            }
        }
    }
}
