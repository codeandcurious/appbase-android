/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import com.tandeminnovation.appbase.eventbus.event.OnErrorEvent
import com.tandeminnovation.appbase.listener.OnFragmentBackPressed

abstract class CommonDialogFragmentBase : DialogFragment(), OnFragmentBackPressed {

    private var mIsStateSaved: Boolean = false

    protected abstract val mainLayoutRes: Int

    protected val appCompatActivity: AppCompatActivity
        get() = activity as AppCompatActivity

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        mIsStateSaved = savedInstanceState != null
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val result = inflater.inflate(mainLayoutRes, container, false)
        mIsStateSaved = savedInstanceState != null

        return result
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)

        if (!view.isInEditMode) {

            setup(savedInstanceState)
        }
    }

    /**
     * firetrap
     * The activity is handling the onBackPressed event but it's notifying the fragment so they can handle
     * and override. If none of this children overrides the function it will perform the default behaviour
     * which is pop the last back stack entry
     */
    override fun onBackPressed() {

        appCompatActivity.supportFragmentManager.popBackStack()
    }

//    abstract fun handleOnErrorEvent(event: OnErrorEvent)

    protected abstract fun setup(savedInstanceState: Bundle?)

    companion object {

        private const val TAG = "CommonDialogFragmentBase"
    }
}