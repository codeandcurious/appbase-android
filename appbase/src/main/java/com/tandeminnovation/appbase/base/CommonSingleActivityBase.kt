/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.base

import com.tandeminnovation.appbase.extension.whenNotNull
import com.tandeminnovation.appbase.helper.FragmentHelper

abstract class CommonSingleActivityBase : CommonActivityBase() {

    protected var fragmentHelper = FragmentHelper.instance

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 1) {

            notifyFragments()
        } else {

            this.finish()
        }
    }

    private fun notifyFragments() {
        whenNotNull(supportFragmentManager.fragments.last()) {

            when (it) {

                is CommonFragmentBase -> it.onBackPressed()
                is CommonDialogFragmentBase -> it.onBackPressed()
                is CommonBottomSheetDialogFragmentBase -> it.onBackPressed()
            }
        }
    }

    companion object {

        private const val TAG = "CommonSingleActivityBase"
    }
}
