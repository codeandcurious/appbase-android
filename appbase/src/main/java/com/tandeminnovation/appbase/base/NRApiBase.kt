/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.base

import android.content.Context
import com.tandeminnovation.appbase.helper.NRApiPreferencesHelper
import java.util.*

abstract class NRApiBase : ApiBase {

    var deviceId: String? = null
        private set
    var language: String = Locale.getDefault().language
        private set

    private val userLanguage: String
        get() = Locale.getDefault().language

    constructor(context: Context) {
        this.deviceId = NRApiPreferencesHelper.getDeviceId(context)
    }

    constructor(context: Context, language: String) : this(NRApiPreferencesHelper.getDeviceId(context), language)

    constructor(deviceId: String?) {
        this.deviceId = deviceId
    }

    constructor(deviceId: String?, language: String) {
        this.deviceId = deviceId
        this.language = language
    }
}
