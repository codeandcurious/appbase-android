/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.manager

import com.tandeminnovation.appbase.api.ApiResponseWrapper
import com.tandeminnovation.appbase.base.ManagerBase
import com.tandeminnovation.appbase.repository.AuthenticationApiRepository
import com.tandeminnovation.appbase.repository.AuthenticationCacheRepository
import com.tandeminnovation.appbase.repository.AuthenticationPersistenceRepository
import org.json.JSONException
import java.io.IOException

/**
 * Created by firetrap on 23/05/2017.
 */
open class AuthenticationManager private constructor() : ManagerBase() {
    /**
     * The Authentication api repository.
     */
    private var authenticationApiRepository = AuthenticationApiRepository.instance
    private var authenticationPersistenceRepository = AuthenticationPersistenceRepository.instance
    private var authenticationCacheRepository = AuthenticationCacheRepository.instance


    private var tokenUrl: String = String()
    private var clientSecret: String = String()
    private var clientId: String = String()
    private var scope: String = String()

    override fun setup() {

        super.setup()

//        BusHelper.register(LoginAction::class.java, this, RxBusMode.Background) {
//
//            checkInitialization(TAG)
//            handleLoginAction(it)
//        }
    }

    open fun setup(tokenUrl: String, clientSecret: String, clientId: String, scope: String) {
        super.setup()
        this.tokenUrl = tokenUrl
        this.clientSecret = clientSecret
        this.clientId = clientId
        this.scope = scope
    }

    @Throws(IOException::class, JSONException::class)
    open fun getAccessTokenFromApi(): ApiResponseWrapper {

        return authenticationApiRepository.getAccessToken(tokenUrl, clientSecret, clientId, scope)
    }

    open fun saveAccessToken(accessToken: String?) {

        authenticationCacheRepository.accessToken = accessToken
        authenticationPersistenceRepository.saveAccessToken(accessToken)
    }

    open fun loadAccessToken(): String? {
        var result: String? = authenticationCacheRepository.accessToken
        if (result == null) {

            result = authenticationPersistenceRepository.loadAccessToken()
            authenticationCacheRepository.accessToken = result
        }

        return result
    }


    companion object {

        /**
         * The constant TAG.
         */
        const val TAG = "AuthenticationManager"

        var instance: AuthenticationManager = AuthenticationManager()
    }
}
