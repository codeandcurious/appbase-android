package com.tandeminnovation.appbase.manager

import com.tandeminnovation.appbase.base.LogModelBase
import com.tandeminnovation.appbase.base.MemoryCacheRepositoryBase

abstract class LogManagerBase {

    private lateinit var mCacheRepository: MemoryCacheRepositoryBase
    private lateinit var mPersistenceRepository: MemoryCacheRepositoryBase

    fun setup(cacheRepository: MemoryCacheRepositoryBase, persistenceRepository: MemoryCacheRepositoryBase) {
        this.mCacheRepository = cacheRepository
        this.mPersistenceRepository = persistenceRepository
    }

    abstract fun saveLog(logModelBase: LogModelBase)

    companion object {
        const val TAG = "LogManagerBase"
    }
}