package com.tandeminnovation.appbase.listener

interface OnFragmentBackPressed {

    fun onBackPressed()
}