/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.listener

import android.view.View
import java.util.*

/**
 * Created by firetrap on 10/10/2017.
 * Composite pattern applied to onclick listeners
 */
class CompositeOnclickListener : View.OnClickListener {
    /**
     * The Listeners.
     */
    private val listeners: MutableList<View.OnClickListener>

    /**
     * Instantiates a new Composite listener.
     */
    init {
        listeners = ArrayList()
    }

    /**
     * Add on click listener.
     *
     * @param listener the listener
     */
    fun addOnClickListener(listener: View.OnClickListener) {
        listeners.add(listener)
    }

    override fun onClick(v: View) {
        for (listener in listeners) {

            listener.onClick(v)
        }
    }

    fun getListeners(): List<View.OnClickListener> {
        return listeners
    }
}