/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.adapter

import android.annotation.SuppressLint
import android.os.Parcelable
import android.util.Log
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.tandeminnovation.appbase.helper.DebugHelper

abstract class FragmentAdapter(private val mFragmentManager: FragmentManager) : androidx.viewpager.widget.PagerAdapter() {

    private var mFragmentTransaction: FragmentTransaction? = null
    private var mCurrentPrimaryItem: Fragment? = null

    /**
     * Return the Fragment associated with a specified position.
     */
    abstract fun getItem(position: Int): Fragment

    override fun startUpdate(container: ViewGroup) {
        if (container.id == View.NO_ID) {

            throw IllegalStateException("ViewPager with adapter " + this + " requires a view id")
        }
    }

    @SuppressLint("CommitTransaction")
    override fun instantiateItem(viewGroup: ViewGroup, position: Int): Any {
        if (mFragmentTransaction == null) {

            mFragmentTransaction = mFragmentManager.beginTransaction()
        }
        val itemId = getItemId(position)
        // Do we already have this fragment?
        val name = makeFragmentName(viewGroup.id, itemId)
        var fragment: Fragment? = mFragmentManager.findFragmentByTag(name)
        if (fragment != null) {

            if (DebugHelper.isDebuggable(viewGroup.context)) {

                Log.v(TAG, "Attaching item #$itemId: f=$fragment")
            }
            mFragmentTransaction!!.attach(fragment)
        } else {

            fragment = getItem(position)
            if (DebugHelper.isDebuggable(viewGroup.context)) {

                Log.v(TAG, "Adding item #$itemId: f=$fragment")
            }
            mFragmentTransaction!!.add(viewGroup.id, fragment,
                    makeFragmentName(viewGroup.id, itemId))
        }
        if (fragment !== mCurrentPrimaryItem) {

            fragment.setMenuVisibility(false)
            fragment.userVisibleHint = false
        }

        return fragment
    }

    @SuppressLint("CommitTransaction")
    override fun destroyItem(viewGroup: ViewGroup, position: Int, `object`: Any) {
        if (mFragmentTransaction == null) {

            mFragmentTransaction = mFragmentManager.beginTransaction()
        }
        if (DebugHelper.isDebuggable(viewGroup.context)) {

            Log.v(TAG, "Detaching item #" + getItemId(position) + ": f=" + `object` + " v=" + (`object` as Fragment).view)
        }
        mFragmentTransaction!!.detach(`object` as Fragment)
    }

    override fun setPrimaryItem(container: ViewGroup, position: Int, `object`: Any) {
        val fragment = `object` as Fragment
        if (fragment !== mCurrentPrimaryItem) {

            if (mCurrentPrimaryItem != null) {

                mCurrentPrimaryItem!!.setMenuVisibility(false)
                mCurrentPrimaryItem!!.userVisibleHint = false
            }

            fragment.setMenuVisibility(true)
            fragment.userVisibleHint = true
            mCurrentPrimaryItem = fragment
        }
    }

    override fun finishUpdate(container: ViewGroup) {
        if (mFragmentTransaction != null) {

            mFragmentTransaction!!.commitAllowingStateLoss()
            mFragmentTransaction = null
            mFragmentManager.executePendingTransactions()
        }
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {

        return (`object` as Fragment).view === view
    }

    override fun saveState(): Parcelable? {

        return null
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {}

    /**
     * Return a unique identifier for the item at the given position.
     *
     *
     *
     * The default implementation returns the given position.
     * Subclasses should override this method if the positions of items can change.
     *
     * @param position Position within this adapter
     * @return Unique identifier for the item at position
     */
    fun getItemId(position: Int): Long {

        return position.toLong()
    }

    companion object {

        private val TAG = "FragmentPagerAdapter"

        private fun makeFragmentName(viewId: Int, id: Long): String {

            return "android:switcher:$viewId:$id"
        }
    }
}