/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.adapter

import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import android.view.View
import android.view.ViewGroup
import com.tandeminnovation.appbase.helper.LayoutHelper
import android.view.View.MeasureSpec



/**
 * The type View pager adapter.
 */
/**
 * Instantiates a new View pager adapter.
 *
 * @param layoutIds the layout ids
 */
open class ViewPagerAdapter(val layoutIds: List<Int>) : androidx.viewpager.widget.PagerAdapter() {

//    protected var layoutIds: List<Int>? = null

//    init {
//
//        this.layoutIds = layoutIds
//    }

    override fun getCount(): Int {

        return this.layoutIds.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        // ViewPager
        val viewPager = container as ViewPager
        val layoutId = this.layoutIds[position]
        val view = LayoutHelper.inflateView(container.getContext(), layoutId)
        viewPager.addView(view, 0)

        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, view: Any) {

        container.removeView(view as View)
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {

        return view === `object`
    }


}
