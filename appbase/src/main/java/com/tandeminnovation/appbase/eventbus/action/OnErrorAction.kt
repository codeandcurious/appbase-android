/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.eventbus.action

import com.tandeminnovation.appbase.base.ActionBase

class OnErrorAction : ActionBase {

    var exception: Exception? = null
    var throwable: Throwable? = null

    constructor(exception: Exception?) : super() {
        this.exception = exception
    }

    constructor(throwable: Throwable?) : super() {
        this.throwable = throwable
    }

    constructor(exception: Exception?, throwable: Throwable?) : super() {
        this.exception = exception
        this.throwable = throwable
    }
}
