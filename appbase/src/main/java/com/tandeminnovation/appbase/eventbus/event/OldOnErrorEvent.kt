/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.eventbus.event

import com.tandeminnovation.appbase.base.ActionBase
import com.tandeminnovation.appbase.base.EventBase

class OldOnErrorEvent : EventBase {

    var error: Int? = null
    var message: String? = null

    constructor(action: ActionBase, errorCode: Int) : super(action) {
        this.error = errorCode
    }

    constructor(action: ActionBase, errorCode: Int, message: String) : super(action) {
        this.error = errorCode
        this.message = message
    }

    companion object {

        val CouldNotSaveDataLocally = 100
        val JsonParse = 101
        val ConnectionTimeout = 102
        val ClientProtocol = 103
        val IO = 104
        val Initialize = 105
        val XmlParse = 106
        val CsvParse = 107
        val UnknownException = 108
    }
}
