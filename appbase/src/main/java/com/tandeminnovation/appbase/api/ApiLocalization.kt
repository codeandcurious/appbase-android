/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.api

import com.tandeminnovation.appbase.helper.JsonHelper

import org.json.JSONException
import org.json.JSONObject

/**
 * Created by firetrap on 24/05/2017.
 */
class ApiLocalization {

    var defaultLanguage: String? = null
    var languages: List<String>? = null

    /**
     * 
     * @constructor
     */
    constructor() {}

    /**
     *
     * @param defaultLanguage String
     * @param languages List<String>
     * @constructor
     */
    constructor(defaultLanguage: String, languages: List<String>) {
        this.defaultLanguage = defaultLanguage
        this.languages = languages
    }

    /**
     *
     * @param jsonObject JSONObject
     * @constructor
     */
    @Throws(JSONException::class)
    constructor(jsonObject: JSONObject) {

        fromJson(jsonObject)
    }

    /**
     *
     * @param json JSONObject
     * @throws JSONException
     */
    @Throws(JSONException::class)
    fun fromJson(json: JSONObject) {

        if (JsonHelper.hasKey(json, JSON_DEFAULT_LANGUAGE)) {

            defaultLanguage = JsonHelper.parseString(json, JSON_DEFAULT_LANGUAGE)
        }

        if (JsonHelper.hasKey(json, JSON_LANGUAGES)) {

            languages = JsonHelper.parseStringList(json, JSON_LANGUAGES)
        }
    }

    companion object {

        private const val JSON_DEFAULT_LANGUAGE = "defaultLanguage"
        private const val JSON_LANGUAGES = "languages"
    }
}