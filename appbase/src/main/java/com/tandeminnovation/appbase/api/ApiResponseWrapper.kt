/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.api

import com.tandeminnovation.appbase.helper.JsonHelper

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class ApiResponseWrapper {

    var apiError: ApiError? = null
    var data: Any? = null
    var apiLocalization: ApiLocalization? = null
    var apiPaging: ApiPaging? = null
    var httpCode: Int? = null
    var etag: String? = null
    var apiResponseParser: ApiResponseParser<*>? = null

    /**
     *
     * @constructor
     */
    constructor()

    /**
     *
     * @param data Any
     * @param APIError ApiError
     * @param ApiLocalization ApiLocalization
     * @param ApiPaging ApiPaging
     * @param httpCode Int?
     * @param etag String?
     * @constructor
     */
    @JvmOverloads constructor(data: Any?, APIError: ApiError?, ApiLocalization: ApiLocalization?, ApiPaging: ApiPaging?, httpCode: Int? = null, etag: String? = null) {
        this.apiError = APIError
        this.data = data
        this.apiLocalization = ApiLocalization
        this.apiPaging = ApiPaging
        this.httpCode = httpCode
        this.etag = etag
    }

    /**
     *
     * @param jsonObject JSONObject
     * @param apiResponseParser ApiResponseParser<*>
     * @param httpCode Int?
     * @param etag String?
     * @constructor
     */
    @Throws(JSONException::class)
    @JvmOverloads constructor(jsonObject: JSONObject?, apiResponseParser: ApiResponseParser<Any>?, httpCode: Int? = null, etag: String? = null) {
        this.apiResponseParser = apiResponseParser
        this.fromJson(jsonObject)
        this.httpCode = httpCode
        this.etag = etag
    }

    /**
     *
     * @param json JSONObject
     * @throws JSONException
     */
    @Throws(JSONException::class)
    fun fromJson(json: JSONObject?) {
        if (json != null) {

            if (JsonHelper.hasKey(json, JSON_DATA)) {

                val intervention = json.get(JSON_DATA)
                if (intervention is JSONArray) {

                    val jsonArray = json.getJSONArray(JSON_DATA)
                    data = apiResponseParser?.fromJsonArray(jsonArray)
                } else if (intervention is JSONObject) {

                    val jsonObject = json.getJSONObject(JSON_DATA)
                    data = apiResponseParser?.fromJsonObject(jsonObject)
                }
            }
            if (JsonHelper.hasKey(json, JSON_ERROR)) {

                apiError = ApiError(json.getJSONObject(JSON_ERROR))
            }
            if (JsonHelper.hasKey(json, JSON_LOCALIZATION)) {

                apiLocalization = ApiLocalization(json.getJSONObject(JSON_LOCALIZATION))
            }
            if (JsonHelper.hasKey(json, JSON_PAGING)) {

                apiPaging = ApiPaging(json.getJSONObject(JSON_PAGING))
            }
        }
    }

    companion object {

        private const val JSON_DATA = "data"
        private const val JSON_ERROR = "error"
        private const val JSON_LOCALIZATION = "localization"
        private const val JSON_PAGING = "paging"
    }
}