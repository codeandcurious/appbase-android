/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.api

import com.tandeminnovation.appbase.helper.JsonHelper

import org.json.JSONException
import org.json.JSONObject

class ApiPaging {

    var pageNumber: Long? = null
    var pageSize: Long? = null
    var pageCount: Long? = null
    var totalRecordCount: Long? = null

    /**
     *
     * @constructor
     */
    constructor() {}

    /**
     *
     * @param pageNumber Long?
     * @param pageSize Long?
     * @param pageCount Long?
     * @param totalRecordCount Long?
     * @constructor
     */
    constructor(pageNumber: Long?, pageSize: Long?, pageCount: Long?, totalRecordCount: Long?) {
        this.pageNumber = pageNumber
        this.pageSize = pageSize
        this.pageCount = pageCount
        this.totalRecordCount = totalRecordCount
    }

    /**
     *
     * @param jsonObject JSONObject
     * @constructor
     */
    @Throws(JSONException::class)
    constructor(jsonObject: JSONObject) {

        fromJson(jsonObject)
    }

    /**
     *
     * @param json JSONObject
     * @throws JSONException
     */
    @Throws(JSONException::class)
    fun fromJson(json: JSONObject) {

        if (JsonHelper.hasKey(json, JSON_PAGE_NUMBER)) {

            pageNumber = JsonHelper.parseLong(json, JSON_PAGE_NUMBER)
        }

        if (JsonHelper.hasKey(json, JSON_PAGE_SIZE)) {

            pageSize = JsonHelper.parseLong(json, JSON_PAGE_SIZE)
        }

        if (JsonHelper.hasKey(json, JSON_PAGE_COUNT)) {

            pageCount = JsonHelper.parseLong(json, JSON_PAGE_COUNT)
        }

        if (JsonHelper.hasKey(json, JSON_TOTAL_RECORD_COUNT)) {

            totalRecordCount = JsonHelper.parseLong(json, JSON_TOTAL_RECORD_COUNT)
        }
    }

    companion object {

        private const val JSON_PAGE_NUMBER = "pageNumber"
        private const val JSON_PAGE_SIZE = "pageSize"
        private const val JSON_PAGE_COUNT = "pageCount"
        private const val JSON_TOTAL_RECORD_COUNT = "totalRecordCount"
    }
}
