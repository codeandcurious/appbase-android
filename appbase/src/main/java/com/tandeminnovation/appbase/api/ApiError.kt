/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.api

import com.tandeminnovation.appbase.helper.JsonHelper

import org.json.JSONException
import org.json.JSONObject

/**
 * Created by firetrap on 24/05/2017.
 */
class ApiError {

    var userMessage: String? = null
    var errorMessage: String? = null
    var errorCode: String? = null

    /**
     * Instantiates a new Api error.
     *
     * @param userMessage  the user message
     * @param errorMessage the error message
     * @param errorCode    the error code
     */
    constructor(userMessage: String, errorMessage: String, errorCode: String) {
        this.userMessage = userMessage
        this.errorMessage = errorMessage
        this.errorCode = errorCode
    }

    /**
     * Instantiates a new Api error.
     *
     * @param jsonObject the json object
     * @throws JSONException the json exception
     */
    @Throws(JSONException::class)
    constructor(jsonObject: JSONObject) {

        fromJson(jsonObject)
    }


    /**
     * From json.
     *
     * @param json the json
     * @throws JSONException the json exception
     */
    @Throws(JSONException::class)
    fun fromJson(json: JSONObject) {

        if (JsonHelper.hasKey(json, JSON_USER_MESSAGE)) {

            userMessage = JsonHelper.parseString(json, JSON_USER_MESSAGE)
        }

        if (JsonHelper.hasKey(json, JSON_ERROR_MESSAGE)) {

            errorMessage = JsonHelper.parseString(json, JSON_ERROR_MESSAGE)
        }

        if (JsonHelper.hasKey(json, JSON_ERROR_CODE)) {

            errorCode = JsonHelper.parseString(json, JSON_ERROR_CODE)
        }
    }

    companion object {

        private const val JSON_USER_MESSAGE = "userMessage"
        private const val JSON_ERROR_MESSAGE = "errorMessage"
        private const val JSON_ERROR_CODE = "errorCode"
        private const val JSON_OFFENDING = "offending"
    }
}