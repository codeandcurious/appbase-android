package com.tandeminnovation.appbase.api

import com.tandeminnovation.appbase.base.EnumValueBase

enum class BancoBestResponseEnum constructor(val value: String) : EnumValueBase {

    EmptyToken("EGEN002"),
    ErrorAccessToken("EAUT_018"),
    ErrorAccessTokenRequired("EGEN012"),
    ErrorAccountAlreadyWithDebitCard("ECBD_012"),
    ErrorAccountCardBlockedOperation("ECCR_017"),
    ErrorAccountCardFieldRequired("ECDB_002"),
    ErrorAccountCardNotFromThisClient("ECCR_009"),
    ErrorAccountCardNotFromThisClient1("ECDB_010"),
    ErrorAccountCardNotFromThisClient2("EMBN_001"),
    ErrorAccountCardRequired("ECCR_002"),
    ErrorAccountCardState("ECCR_039"),
    ErrorAccountCreditCardRequired("ECCR_003"),
    ErrorAccountDoesNotExist("EACC003"),
    ErrorAccountNameRequired("EACC006"),
    ErrorAccountNameTooManyChars("EACC007"),
    ErrorAccountNotActive("EACC004"),
    ErrorAccountNotFound("EACC002"),
    ErrorAccountNumberFieldRequired("ECCR_041"),
    ErrorAmountUnauthorized("EMBN_018"),
    ErrorAppRequired("EAUT_002"),
    ErrorAuthenticationMaxAttempts("EGEN010"),
    ErrorAutoExpensesFieldRequired("ECCR_051"),
    ErrorBalanceConsult("BAL01"),
    ErrorCantChangePassword("ELOG004"),
    ErrorCardInsufficientBalance("ECCR_019"),
    ErrorCardMbnetCancelled("EMBN_015"),
    ErrorCardMbnetNotFound("EMBN_014"),
    ErrorCardNotFromThisAccountCard("EMBN_002"),
    ErrorCardNumberFieldRequired("EMBN_005"),
    ErrorCellphoneNotFound("ELOG001"),
    ErrorChangeTypeFieldRequired("ECCR_028"),
    ErrorChangingPassword("ELOG011"),
    ErrorCheckingAccountNotFound("ECCR_040"),
    ErrorClientWithoutSMSToken("ELOG012"),
    ErrorCountryFieldRequired("ECCR_024"),
    ErrorCountryFieldRequired1("ECDB_006"),
    ErrorCreditCardNotFromThisClient("ECCR_018"),
    ErrorCreditCardState("ECCR_020"),
    ErrorCreditCardTypeFieldRequired("ECCR_042"),
    ErrorDBEntryNotFound("EAUT_008"),
    ErrorDebitAccountNumberNotFromThisClient("ECDB_011"),
    ErrorDebitCardNumberFieldRequired("ECDB_001"),
    ErrorDecryptingToken("EAUT_015"),
    ErrorDeviceIdNotFound("EAUT_019"),
    ErrorDeviceIdRequired("EAUT_013"),
    ErrorDeviceNotFound("EAUT_020"),
    ErrorDiaryAmountRequired1("EMBN_016"),
    ErrorDiaryAmountRequired2("EMBN_017"),
    ErrorEmptyBeneficiaryName("TRA01"),
    ErrorEmptyEntity("PAY01"),
    ErrorEmptyIBAN("TRA05"),
    ErrorEmptyPaymentAmount("PAY04"),
    ErrorEmptyReference("PAY02"),
    ErrorEmptyTransferAmount("TRA02"),
    ErrorEmptyTransferDescription("TRA04"),
    ErrorEndDateBeforeStartDate("EACC012"),
    ErrorEndDateBeforeStartDate1("ECCR_006"),
    ErrorEndDateBeforeStartDate2("ECCR_036"),
    ErrorEndDateFieldRequired("ECCR_035"),
    ErrorEndDateOverThreeMonthsAgo("ECCR_038"),
    ErrorExceededSMSTokenAttempts("API05"),
    ErrorExpiredKey("EGEN009"),
    ErrorExpiredSMSToken("API04"),
    ErrorExtractAmountIsZero("ECCR_016"),
    ErrorFamilyMembersFieldRequired("ECCR_047"),
    ErrorFieldChangeType("ECCR_033"),
    ErrorFiscalResidenceYearsFieldRequired("ECCR_046"),
    ErrorGeneratingToken("API06"),
    ErrorGeneratingTokenKey("EAUT_004"),
    ErrorGuestTokenRequired("EGEN011"),
    ErrorHabitationalCodeFieldRequired("ECCR_045"),
    ErrorHomeExpensesFieldRequired("ECCR_050"),
    ErrorInactiveAccountCard("ECBD_020"),
    ErrorInactiveToken("API09"),
    ErrorInactiveTransaction("ECBD_014"),
    ErrorIncomeFieldMustBeNumber("ECCR_032"),
    ErrorIncomeFieldRequired("ECCR_031"),
    ErrorIncorrectCellphone("ELOG013"),
    ErrorIncorrectValues("ECCR_022"),
    ErrorInput("EAUT_012"),
    ErrorInvalidAccountCardForAccount("ECCR_059"),
    ErrorInvalidAccountName("EACC008"),
    ErrorInvalidAccountNumber("EACC005"),
    ErrorInvalidAmount("ECCR_010"),
    ErrorInvalidCard("EMBN_004"),
    ErrorInvalidContractType("ECCR_058"),
    ErrorInvalidCredentials1("EAUT_011"),
    ErrorInvalidCredentials2("EAUT_003"),
    ErrorInvalidCredentials3("EAUT_031"),
    ErrorInvalidCreditCardType("ECCR_055"),
    ErrorInvalidExtractAmount("ECCR_015"),
    ErrorInvalidExtractState("ECCR_014"),
    ErrorInvalidHabitationalCode("ECCR_057"),
    ErrorInvalidIBANSize("TRA06"),
    ErrorInvalidInput("EAUT_007"),
    ErrorInvalidKey("EGEN008"),
    ErrorInvalidPaymentAmount("PAY06"),
    ErrorInvalidPaymentMode("ECCR_056"),
    ErrorInvalidPercentageField("ECCR_054"),
    ErrorInvalidReference("PAY03"),
    ErrorInvalidReferenceSize("PAY05"),
    ErrorInvalidSMSToken("API03"),
    ErrorInvalidToken("API01"),
    ErrorInvalidTransactionId("API02"),
    ErrorInvalidTransactionNumber("ECCR_013"),
    ErrorInvalidTransferAmount("TRA03"),
    ErrorInvalidUserId("API10"),
    ErrorInvalidUsername("ELOG008"),
    ErrorInvalidValues("ECDB_004"),
    ErrorInvalidVirtualCardLimit("EMBN_010"),
    ErrorInvalidVirtualCardNumber("EMBN_011"),
    ErrorInvalidVirtualCardPhysicalNumber("EMBN_013"),
    ErrorInvalidVirtualCardType("EMBN_006"),
    ErrorInvalidVirtualExpirationDate("EMBN_009"),
    ErrorLoginWithUserPassRequired("EAUT_030"),
    ErrorMbnetCardIdRequired("EMBN_021"),
    ErrorMonthlyRevenueFieldRequired("ECCR_048"),
    ErrorMissingFields("ELOG003"),
    ErrorMissingObjectId("EGEN006"),
    ErrorMovementsConsult("MOV01"),
    ErrorMustChangePassword("EGEN005"),
    ErrorMustFillCardName("ECBD_017"),
    ErrorMustFillCardOwner("ECBD_018"),
    ErrorMustSelectAccount("ECBD_015"),
    ErrorMustSelectCardType("ECBD_016"),
    ErrorNewCardFieldRequired1("ECCR_026"),
    ErrorNewCardFieldRequired2("ECDB_008"),
    ErrorNoMovements("EACC010"),
    ErrorNoRelationBetweenAccountAndBenefeciary("ECBD_019"),
    ErrorObjectIdRequired("EAUT_001"),
    ErrorOtherRevenueFieldRequired("ECCR_049"),
    ErrorPassOrTokenRequired("EAUT_016"),
    ErrorPasswordMustHaveSixChars("ELOG007"),
    ErrorPasswordOnlySupportNumbers("ELOG010"),
    ErrorPayment("PAY07"),
    ErrorPaymentModeFieldRequired("ECCR_044"),
    ErrorPercentageFieldMustBeNumber("ECCR_053"),
    ErrorPercentageFieldRequired("ECCR_052"),
    ErrorPermissionDenied("API08"),
    ErrorPhysicalCardNumberFieldRequired("EMBN_012"),
    ErrorPlafondFieldMustBeNumber("ECCR_030"),
    ErrorPlafondFieldRequired("ECCR_029"),
    ErrorReasonFieldRequired1("ECCR_023"),
    ErrorReasonFieldRequired2("ECDB_005"),
    ErrorReasonFieldTooManyChars1("ECCR_027"),
    ErrorReasonFieldTooManyChars2("ECDB_009"),
    ErrorRecordNameFieldRequired("ECCR_043"),
    ErrorRepeatedTransaction("ECBD_013"),
    ErrorSameAccountName("EACC009"),
    ErrorSamePassword("ELOG009"),
    ErrorSavingToken("EAUT_006"),
    ErrorSequentialPassword("ELOG005"),
    ErrorStartDateEndDate1("EACC011"),
    ErrorStartDateEndDate2("ECCR_005"),
    ErrorStartDateFieldRequired("ECCR_034"),
    ErrorStartDateIsAfterToday("ECCR_037"),
    ErrorStartDateOverOneYearAgo1("EACC013"),
    ErrorStartDateOverOneYearAgo2("ECCR_007"),
    ErrorStatusFieldRequired1("ECCR_021"),
    ErrorStatusFieldRequired2("ECDB_003"),
    ErrorTemporaryError("EGEN007"),
    ErrorTemporaryError1("EMBN_019"),
    ErrorTokenAndAppFieldsRequired("EAUT_010"),
    ErrorTokenDisabled("EAUT_014"),
    ErrorTokenWithoutAccounts("API07"),
    ErrorTransaction1("ECCR_001"),
    ErrorTransaction2("EMBN_003"),
    ErrorTransactionFieldRequired("ECCR_012"),
    ErrorUnexpected("API99"),
    ErrorUpdatedDataField("EMBN_022"),
    ErrorUrgentFieldRequired1("ECCR_025"),
    ErrorUrgentFieldRequired2("ECDB_007"),
    ErrorUserNotFound("EACC001"),
    ErrorUserRequired("EAUT_017"),
    ErrorValidation("ELOG002"),
    ErrorVirtualCardLimit("EMBN_008"),
    ErrorVirtualCardNameRequired("EMBN_007"),
    ErrorVirtualCardNotFound("EMBN_020"),
    ErrorWrongPassword("ELOG006"),
    ErrorZeroAmount("ECCR_011"),
    InactiveToken("EGEN003"),
    InvalidToken("EGEN004"),
    NextConnectionError("NRCONERR"),
    NextGenericError("NRGENERR"),
    NextJailbreakRootError("NRJAILBREAKROOTERR"),
    NextSSLPinningError("NRSSLPinningErr"),
    Success("0"),
    TechnicalError("EGEN001"),
    ErrorNeedsStrongAuthentication("EGEN015"),
    UnknownEnum("UnknownEnum"),

    TechnicalError1("1"),
    ErrorInvalidLogin("11"),
    ErrorValidationCode("12"),
    ErrorValidationCode1("13"),
    FaceTouchActivated("20"),
    ErrorFaceTouchCancelled("51"),
    ErrorInvalidToken1("94"),
    TechnicalError2("96"),
    TechnicalError3("99"),


    IsinNotFilled("EFUN009"),
    UPOrAmountIsInferiorToSubscriptionValue("0029FUN0006P"),
    SelectedFundIsCloseForPurchase("0029FUN0116P"),

    DepositAmountOverBalance("EDPS_013"),


    ErrorObjAmountMissing("EOBJ_001"),
    ErrorObjDateMissing("EOBJ_002"),
    ErrorObjTermLessThanThreeDays("EOBJ_003"),
    ErrorObjAmountMustBeFilledWithDigits1("EOBJ_005"),
    ErrorObjAmountMustBeFilledWithDigits2("EOBJ_013"),
    ErrorObjAmountMustBeFilledWithDigits3("EOBJ_017"),
    ErrorObjAmountMustBeFilledWithDigits4("EOBJ_031"),
    ErrorObjReinforceAmountIsMandatory1("EOBJ_009"),
    ErrorObjReinforceAmountIsMandatory2("EOBJ_010"),
    ErrorObjReinforceAmountIsMandatory3("EOBJ_012"),
    ErrorObjNameIsMandatory("EOBJ_014"),
    ErrorObjIconIsMandatory("EOBJ_015"),
    ErrorObjReinforcementDayIsMandatory("EOBJ_016"),
    ErrorObjReinforceAmountCantBeHigherThanBalance("EOBJ_019"),
    ErrorObjMobilizeAmountHigherThanObjAvailableAmount("EOBJ_030"),
    ErrorObjTermMustBeLessThan2Years("EOBJ_040"),
    ErrorObjReinforceDayMustBeOnTerm("EOBJ_041"),
    ErrorObjInvalidIcon("EOBJ_043"),
    ErrorObjInvalidName("EOBJ_044"),
    ErrorObjInvalidReinforcementDay("EOBJ_045"),
    ErrorObjFinished("EOBJ_046"),
    ErrorObjInvalidPlanChanged("EOBJ_048"),
    ErrorObjAmountMustBeHigherThan("EOBJ_050"),
    ErrorObjReinforcementAmountRange("EOBJ_009");

    fun clarification(): String {

        return when (TechnicalError) {

            ErrorInvalidToken -> ""
            ErrorInvalidTransactionId -> ""
            ErrorInvalidSMSToken -> ""
            ErrorFieldChangeType -> "O campo 'change_type' deverá estar preenchido com os valores 'T' ou 'D'"
            ErrorExpiredSMSToken -> ""
            ErrorExceededSMSTokenAttempts -> ""
            ErrorGeneratingToken -> ""
            ErrorTokenWithoutAccounts -> ""
            ErrorPermissionDenied -> ""
            ErrorInactiveToken -> ""
            ErrorInvalidUserId -> ""
            ErrorUnexpected -> ""
            ErrorBalanceConsult -> ""
            ErrorMovementsConsult -> ""
            ErrorEmptyEntity -> ""
            ErrorEmptyReference -> ""
            ErrorInvalidReference -> ""
            ErrorEmptyPaymentAmount -> ""
            ErrorInvalidReferenceSize -> ""
            ErrorInvalidPaymentAmount -> ""
            ErrorPayment -> ""
            ErrorEmptyBeneficiaryName -> ""
            ErrorEmptyTransferAmount -> ""
            ErrorInvalidTransferAmount -> ""
            ErrorEmptyTransferDescription -> ""
            ErrorEmptyIBAN -> ""
            ErrorInvalidIBANSize -> ""
            Success -> ""
            ErrorUserNotFound -> ""
            ErrorAccountNotFound -> ""
            ErrorAccountDoesNotExist -> ""
            ErrorAccountNotActive -> ""
            ErrorInvalidAccountNumber -> ""
            ErrorAccountNameRequired -> ""
            ErrorAccountNameTooManyChars -> ""
            ErrorInvalidAccountName -> ""
            ErrorSameAccountName -> ""
            ErrorNoMovements -> ""
            ErrorStartDateEndDate1 -> "Data de início e fim terão de ser preenchidas simultaneamente"
            ErrorEndDateBeforeStartDate -> "Data de fim não deverá ser inferior à data de início"
            TechnicalError -> ""
            EmptyToken -> ""
            InactiveToken -> ""
            InvalidToken -> ""
            ErrorLoginWithUserPassRequired -> "Para criar o Login Token é necessário primeiro fazer Login com User e Password"
            NextGenericError -> ""
            NextConnectionError -> ""
            NextJailbreakRootError -> ""
            NextSSLPinningError -> ""
            ErrorStartDateOverOneYearAgo1 -> "Data de início não deverá ser inferior à data de hoje menos 365 dias"
            ErrorObjectIdRequired -> "O campo 'object_id' deverá estar preenchido"
            ErrorAppRequired -> "O campo app' é de preenchimento obrigatório"
            ErrorSavingToken -> "Ocorreu um erro na gravação do token na base de dados"
            ErrorInvalidInput -> "Ocorreu um erro, valide por favor todos os inputs e tente novamente"
            ErrorDeviceIdNotFound -> "O utilizador não tem registado o device indicado"
            ErrorInvalidAccountCardForAccount -> "Na conta à ordem seleccionada o cliente já tem uma conta cartão com um cartão de que é titular"
            ErrorUpdatedDataField -> "O campo 'updated_data' apenas aceita os valores 'Y', 'N' ou '' (nada)"
            ErrorAccessToken -> ""
            ErrorAccessTokenRequired -> ""
            ErrorAccountAlreadyWithDebitCard -> ""
            ErrorAccountCardBlockedOperation -> ""
            ErrorAccountCardFieldRequired -> ""
            ErrorAccountCardNotFromThisClient -> ""
            ErrorAccountCardNotFromThisClient1 -> ""
            ErrorAccountCardNotFromThisClient2 -> ""
            ErrorAccountCardRequired -> ""
            ErrorAccountCardState -> ""
            ErrorAccountCreditCardRequired -> ""
            ErrorAccountNumberFieldRequired -> ""
            ErrorAmountUnauthorized -> ""
            ErrorAuthenticationMaxAttempts -> ""
            ErrorAutoExpensesFieldRequired -> ""
            ErrorCantChangePassword -> ""
            ErrorCardInsufficientBalance -> ""
            ErrorCardMbnetCancelled -> ""
            ErrorCardMbnetNotFound -> ""
            ErrorCardNotFromThisAccountCard -> ""
            ErrorCardNumberFieldRequired -> ""
            ErrorCellphoneNotFound -> ""
            ErrorChangeTypeFieldRequired -> ""
            ErrorChangingPassword -> ""
            ErrorCheckingAccountNotFound -> ""
            ErrorClientWithoutSMSToken -> ""
            ErrorCountryFieldRequired -> ""
            ErrorCountryFieldRequired1 -> ""
            ErrorCreditCardNotFromThisClient -> ""
            ErrorCreditCardState -> ""
            ErrorCreditCardTypeFieldRequired -> ""
            ErrorDBEntryNotFound -> ""
            ErrorDebitAccountNumberNotFromThisClient -> ""
            ErrorDebitCardNumberFieldRequired -> ""
            ErrorDecryptingToken -> ""
            ErrorDeviceIdRequired -> ""
            ErrorDeviceNotFound -> ""
            ErrorDiaryAmountRequired1 -> ""
            ErrorDiaryAmountRequired2 -> ""
            ErrorEndDateBeforeStartDate1 -> ""
            ErrorEndDateBeforeStartDate2 -> ""
            ErrorEndDateFieldRequired -> ""
            ErrorEndDateOverThreeMonthsAgo -> ""
            ErrorExpiredKey -> ""
            ErrorExtractAmountIsZero -> ""
            ErrorFamilyMembersFieldRequired -> ""
            ErrorFiscalResidenceYearsFieldRequired -> ""
            ErrorGeneratingTokenKey -> ""
            ErrorGuestTokenRequired -> ""
            ErrorHabitationalCodeFieldRequired -> ""
            ErrorHomeExpensesFieldRequired -> ""
            ErrorInactiveAccountCard -> ""
            ErrorInactiveTransaction -> ""
            ErrorIncomeFieldMustBeNumber -> ""
            ErrorIncomeFieldRequired -> ""
            ErrorIncorrectCellphone -> ""
            ErrorIncorrectValues -> ""
            ErrorInput -> ""
            ErrorInvalidAmount -> ""
            ErrorInvalidCard -> ""
            ErrorInvalidContractType -> ""
            ErrorInvalidCredentials1 -> ""
            ErrorInvalidCredentials2 -> ""
            ErrorInvalidCredentials3 -> ""
            ErrorInvalidCreditCardType -> ""
            ErrorInvalidExtractAmount -> ""
            ErrorInvalidExtractState -> ""
            ErrorInvalidHabitationalCode -> ""
            ErrorInvalidKey -> ""
            ErrorInvalidPaymentMode -> ""
            ErrorInvalidPercentageField -> ""
            ErrorInvalidTransactionNumber -> ""
            ErrorInvalidUsername -> ""
            ErrorInvalidValues -> ""
            ErrorInvalidVirtualCardLimit -> ""
            ErrorInvalidVirtualCardNumber -> ""
            ErrorInvalidVirtualCardPhysicalNumber -> ""
            ErrorInvalidVirtualCardType -> ""
            ErrorInvalidVirtualExpirationDate -> ""
            ErrorMbnetCardIdRequired -> ""
            ErrorMonthlyRevenueFieldRequired -> ""
            ErrorMissingFields -> ""
            ErrorMissingObjectId -> ""
            ErrorMustChangePassword -> ""
            ErrorMustFillCardName -> ""
            ErrorMustFillCardOwner -> ""
            ErrorMustSelectAccount -> ""
            ErrorMustSelectCardType -> ""
            ErrorNewCardFieldRequired1 -> ""
            ErrorNewCardFieldRequired2 -> ""
            ErrorNoRelationBetweenAccountAndBenefeciary -> ""
            ErrorOtherRevenueFieldRequired -> ""
            ErrorPassOrTokenRequired -> ""
            ErrorPasswordMustHaveSixChars -> ""
            ErrorPasswordOnlySupportNumbers -> ""
            ErrorPaymentModeFieldRequired -> ""
            ErrorPercentageFieldMustBeNumber -> ""
            ErrorPercentageFieldRequired -> ""
            ErrorPhysicalCardNumberFieldRequired -> ""
            ErrorPlafondFieldMustBeNumber -> ""
            ErrorPlafondFieldRequired -> ""
            ErrorReasonFieldRequired1 -> ""
            ErrorReasonFieldRequired2 -> ""
            ErrorReasonFieldTooManyChars1 -> ""
            ErrorReasonFieldTooManyChars2 -> ""
            ErrorRecordNameFieldRequired -> ""
            ErrorRepeatedTransaction -> ""
            ErrorSamePassword -> ""
            ErrorSequentialPassword -> ""
            ErrorStartDateEndDate2 -> ""
            ErrorStartDateFieldRequired -> ""
            ErrorStartDateIsAfterToday -> ""
            ErrorStartDateOverOneYearAgo2 -> ""
            ErrorStatusFieldRequired1 -> ""
            ErrorStatusFieldRequired2 -> ""
            ErrorTemporaryError -> ""
            ErrorTemporaryError1 -> ""
            ErrorTokenAndAppFieldsRequired -> ""
            ErrorTokenDisabled -> ""
            ErrorTransaction1 -> ""
            ErrorTransaction2 -> ""
            ErrorTransactionFieldRequired -> ""
            ErrorUrgentFieldRequired1 -> ""
            ErrorUrgentFieldRequired2 -> ""
            ErrorUserRequired -> ""
            ErrorValidation -> ""
            ErrorVirtualCardLimit -> ""
            ErrorVirtualCardNameRequired -> ""
            ErrorVirtualCardNotFound -> ""
            ErrorWrongPassword -> ""
            ErrorZeroAmount -> ""
            UnknownEnum -> ""
            TechnicalError1 -> ""
            ErrorInvalidLogin -> ""
            ErrorValidationCode -> ""
            ErrorValidationCode1 -> ""
            FaceTouchActivated -> ""
            ErrorFaceTouchCancelled -> ""
            ErrorInvalidToken1 -> ""
            TechnicalError2 -> ""
            TechnicalError3 -> ""
            else -> ""
        }
    }

    fun isPushNotification(): Boolean {
        return when (this) {

            ErrorAccountCardBlockedOperation,
            ErrorAmountUnauthorized,
            ErrorAppRequired,
            ErrorAutoExpensesFieldRequired,
            ErrorDecryptingToken,
            ErrorDeviceIdNotFound,
            ErrorDeviceIdRequired,
            ErrorDeviceNotFound,
            ErrorDiaryAmountRequired1,
            ErrorDiaryAmountRequired2,
            ErrorEmptyBeneficiaryName,
            ErrorEmptyEntity,
            ErrorEmptyIBAN,
            ErrorEmptyPaymentAmount,
            ErrorEmptyReference,
            ErrorEmptyTransferAmount,
            ErrorEmptyTransferDescription,
            ErrorEndDateBeforeStartDate,
            ErrorEndDateBeforeStartDate1,
            ErrorEndDateBeforeStartDate2,
            ErrorEndDateFieldRequired,
            ErrorEndDateOverThreeMonthsAgo,
            ErrorExceededSMSTokenAttempts,
            ErrorExpiredKey,
            ErrorExpiredSMSToken,
            ErrorFamilyMembersFieldRequired,
            ErrorFieldChangeType,
            ErrorFiscalResidenceYearsFieldRequired,
            ErrorGeneratingToken,
            ErrorGeneratingTokenKey,
            ErrorGuestTokenRequired,
            ErrorInactiveAccountCard,
            ErrorInactiveToken,
            ErrorInactiveTransaction,
            ErrorIncomeFieldMustBeNumber,
            ErrorIncomeFieldRequired,
            ErrorIncorrectCellphone,
            ErrorIncorrectValues,
            ErrorInput,
            ErrorInvalidAccountName,
            ErrorInvalidAccountNumber,
            ErrorInvalidAmount,
            ErrorInvalidCard,
            ErrorInvalidCredentials1,
            ErrorInvalidCredentials2,
            ErrorInvalidCredentials3,
            ErrorInvalidIBANSize,
            ErrorInvalidKey,
            ErrorInvalidPaymentAmount,
            ErrorInvalidReference,
            ErrorInvalidSMSToken,
            ErrorInvalidToken,
            ErrorInvalidTransferAmount,
            ErrorInvalidValues,
            ErrorMissingFields,
            ErrorMissingObjectId,
            ErrorMovementsConsult,
            ErrorMustChangePassword,
            ErrorMustSelectAccount,
            ErrorMustSelectCardType,
            ErrorObjectIdRequired,
            ErrorPasswordMustHaveSixChars,
            ErrorPasswordOnlySupportNumbers,
            ErrorPayment,
            ErrorPaymentModeFieldRequired,
            ErrorPercentageFieldMustBeNumber,
            ErrorPercentageFieldRequired,
            ErrorPermissionDenied,
            ErrorPlafondFieldMustBeNumber,
            ErrorRecordNameFieldRequired,
            ErrorSequentialPassword,
            ErrorUnexpected,
            ErrorValidation,
            ErrorVirtualCardNameRequired,
            ErrorVirtualCardNotFound,
            ErrorWrongPassword,
            ErrorZeroAmount,
            InactiveToken,
            InvalidToken,
            TechnicalError,
            UnknownEnum,
            TechnicalError1,
            ErrorInvalidLogin,
            ErrorValidationCode,
            ErrorValidationCode1,
            FaceTouchActivated,
            ErrorFaceTouchCancelled,
            ErrorInvalidToken1,
            TechnicalError2,
            TechnicalError3 -> true
            else -> false
        }
    }

    override fun toString(): String {

        return this.value.toString()
    }


    companion object {

        var apiReturnValue: String? = null

        fun valueOfKey(value: String): EnumValueBase? {

            BancoBestResponseEnum.apiReturnValue = value
            return values().find { it.value == value } ?: UnknownEnum
        }
    }
}