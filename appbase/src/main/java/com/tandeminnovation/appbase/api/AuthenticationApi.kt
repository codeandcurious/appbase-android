/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.api

import com.tandeminnovation.appbase.base.ApiBase
import com.tandeminnovation.appbase.entity.NameValuePair
import com.tandeminnovation.appbase.helper.Base64Helper
import com.tandeminnovation.appbase.helper.JsonHelper
import com.tandeminnovation.appbase.helper.StreamHelper
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.text.MessageFormat

class AuthenticationApi : ApiBase() {

    override var apiBaseUrl: String
        get() = ""
        set(value) {}

    /**
     *
     * @param tokenUrl String
     * @param clientSecret String
     * @param clientId String
     * @param scope String
     * @return ApiResponseWrapper
     * @throws IOException
     * @throws JSONException
     */
    @Throws(IOException::class, JSONException::class)
    fun getAccessToken(tokenUrl: String, clientSecret: String, clientId: String, scope: String): ApiResponseWrapper {
        var accessString: String? = null
        val contentType = "application/x-www-form-urlencoded".toMediaTypeOrNull()
        val content = MessageFormat.format("grant_type=client_credentials&scope={0}", scope)

        val requestBody = RequestBody.create(contentType, content)
        val headerContentType = NameValuePair("Content-Type", "application/x-www-form-urlencoded")
        val headerAuth = NameValuePair("Authorization", "basic " + Base64Helper.encode(MessageFormat.format("{0}:{1}", clientId, clientSecret).toByteArray()))

        val httpResponse = httpHelper.post(tokenUrl, requestBody, headerContentType, headerAuth)
        val stream = httpResponse.stream
        val resultJson = StreamHelper.inputStreamToString(stream)
        if (!resultJson.isNullOrEmpty()) {

            accessString = JsonHelper.parseString(JSONObject(resultJson), "access_token")
        }

        return ApiResponseWrapper(accessString, null, null, null, httpResponse.code, httpResponse.etag)
    }
}
