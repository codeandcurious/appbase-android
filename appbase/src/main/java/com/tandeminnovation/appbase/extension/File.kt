package com.tandeminnovation.appbase.extension

import java.io.File

/**
 * Created by firetrap on 2019-06-21
 */


fun String.uniqueIdentifier(): String {
    val file = File(this)

    return "${file.absolutePath}${file.lastModified()}"
}