/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.extension

import java.text.MessageFormat
import java.text.Normalizer
import java.util.*

private const val HEX_CHARS = "0123456789ABCDEF"
private const val CASE_REGULAR_EXPRESSION = "(.)(\\p{Upper})"

/**
 *
 * @receiver String
 * @return ByteArray
 */
fun String.hexStringToByteArray(): ByteArray {
    val result = ByteArray(length / 2)
    for (i in 0 until length step 2) {

        val firstIndex = HEX_CHARS.indexOf(this[i])
        val secondIndex = HEX_CHARS.indexOf(this[i + 1])
        val octet = firstIndex.shl(4).or(secondIndex)

        result[i.shr(1)] = octet.toByte()
    }

    return result
}

/**
 * Normalize string.
 *
 * @param text the text
 * @return the string
 */
fun String.normalize(text: String): String {

    return Normalizer.normalize(text, Normalizer.Form.NFD).replace("[^\\p{ASCII}]".toRegex(), "")
}

/**
 * Gets camel case name.
 *
 * @param name the name
 * @return the camel case name
 */
fun String.getCamelCaseName(name: String): String {

    return String.format("%s%s", name.substring(0, 1).toLowerCase(), name.substring(1))
}

/**
 * Gets snake case name.
 *
 * @param name the name
 * @return the snake case name
 */
fun String.getSnakeCaseName(name: String): String {

    return name.replace(CASE_REGULAR_EXPRESSION.toRegex(), "$1_$2").toLowerCase()
}

/**
 * Gets screaming snake case name.
 *
 * @param name the name
 * @return the screaming snake case name
 */
fun String.getScreamingSnakeCaseName(name: String): String {

    return name.replace(CASE_REGULAR_EXPRESSION.toRegex(), "$1_$2").toUpperCase()
}

/**
 * Gets pascal case name.
 *
 * @param name the name
 * @return the pascal case name
 */
fun String.getPascalCaseName(name: String): String {

    return String.format("%s%s", name.substring(0, 1).toUpperCase(), name.substring(1)).replace(" ", "")
}

/**
 * Gets humanized name.
 *
 * @param name the name
 * @return the humanized name
 */
fun String.getHumanizedName(name: String): String {

    return name.replace(CASE_REGULAR_EXPRESSION.toRegex(), "$1 $2")
}

/**
 * Format string.
 *
 * @param string the format string
 * @param pN           the p n
 * @return the string
 */
fun String.format(string: String, vararg pN: Any): String {

    return MessageFormat.format(string, *pN)
}

/**
 * Capitalize string.
 * @param locale the locale
 * @return a copy of this string having its first letter uppercased, or the original string,
 * if it's empty or already starts with an upper case letter.
 */
fun String.capitalize(locale: Locale): String {
    return if (isNotEmpty() && this[0].isLowerCase()) substring(0, 1).toUpperCase(locale) + substring(1) else this
}

/**
 * True when a valid surrogate pair starts at the given `index` in the given `string`.
 * Out-of-range indexes return false.
 */
internal fun String.validSurrogatePairAt(string: CharSequence, index: Int): Boolean {
    return (index >= 0
            && index <= string.length - 2
            && Character.isHighSurrogate(string[index])
            && Character.isLowSurrogate(string[index + 1]))
}

