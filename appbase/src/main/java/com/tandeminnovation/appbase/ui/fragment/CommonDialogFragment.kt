/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.appbase.ui.fragment

import android.app.Dialog
import android.view.Window
import android.view.WindowManager
import androidx.fragment.app.DialogFragment

public class CommonDialogFragment : DialogFragment() {

    override fun setupDialog(dialog: Dialog, style: Int) {
        when (style) {
            STYLE_NO_INPUT -> {
                dialog.window!!.addFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            }
            // fall through...
            STYLE_NO_FRAME, STYLE_NO_TITLE -> dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        }
    }

    companion object {

        private const val TAG = "CommonDialogFragment"
    }
}

