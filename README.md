![tandem-innovation](tandem-innovation.png)

# android-kotlin-appbase
Appbase is a set of core libraries made in kotlin to help developing fast apps.
The main usage it's extending our base classes and using our helpers or extensions.

Latest version: 1.0.1 (15/10/2018) minApi: 21

Gradle setup
------------
**Project gradle:**
```
repositories { 
      maven { url "https://dl.bintray.com/tandem/com.tandeminnovation.android" }
    }
```
**Module gradle:**
```
dependencies {
    implementation 'com.tandeminnovation.android:appbase:1.0.0'
}
```
Helpers
------------
* [AlarmHelper](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/AlarmHelper.kt)
* [AnimationHelper](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/AnimationHelper.kt)
* [AppHelper](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/AppHelper.kt)
* [AssetsHelper](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/AssetsHelper.kt)
* [Base64Helper](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/Base64Helper.kt)
* [BluetoothHelper](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/BluetoothHelper.kt)
* [BundleHelper](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/BundleHelper.kt)
* [BusHelper](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/BusHelper.kt)
* [CameraHelper](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/CameraHelper.kt)
* [ColorHelper](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/ColorHelper.kt)
* [ComparatorHelper](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/ComparatorHelper.kt)
* [ConnectionHelper](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/ConnectionHelper.kt)
* [CurrencyHelper](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/CurrencyHelper.kt)
* [CustomTypefaceSpan](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/CustomTypefaceSpan.kt)
* [DateHelper](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/DateHelper.kt)
* [DebugHelper](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/DebugHelper.kt)
* [DeviceHelper](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/DeviceHelper.kt)
* [DialogHelper](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/DialogHelper.kt)
* [DimensionHelper](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/DimensionHelper.kt)
* [FileHelper](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/FileHelper.kt)
* [FragmentHelper](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/FragmentHelper.kt)
* [HttpHelper](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/HttpHelper.kt)
* [ImageSizeHelper](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/ImageSizeHelper.kt)
* [IntentHelper](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/IntentHelper.kt)
* [JsonHelper](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/JsonHelper.kt)
* [KeyboardHelper](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/KeyboardHelper.kt)
* [LayoutHelper](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/LayoutHelper.kt)
* [LocalizationHelper](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/LocalizationHelper.kt)
* [LoggingInterceptor](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/LoggingInterceptor.kt)
* [LogHelper](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/LogHelper.kt)
* [MediaHelper](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/MediaHelper.kt)
* [NavigationHelperBase](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/NavigationHelperBase.kt)
* [NRApiPreferencesHelper](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/NRApiPreferencesHelper.kt)
* [PathHelper](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/PathHelper.kt)
* [PerformanceHelper](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/PerformanceHelper.kt)
* [PreferencesHelper](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/PreferencesHelper.kt)
* [ResourceHelper](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/ResourceHelper.kt)
* [SpannableHelper](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/SpannableHelper.kt)
* [StreamHelper](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/helper/StreamHelper.kt)


-------
Extensions
------------
* [ByteArray](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/extension/ByteArray.kt)
* [InputStream](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/extension/InputStream.kt)
* [Integer](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/extension/Integer.kt)
* [String](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/extension/String.kt)
* [TextView](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/extension/TextView.kt)

-------
Other packages
------------
* [adapter](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/adapter/)
* [api](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/api/)
* [base](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/base/)
* [delegate](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/delegate/)
* [entity](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/entity/)
* [enumeration](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/enumeration/)
* [eventbus](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/eventbus/)
* [exception](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/exception/)
* [fragment](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/fragment/)
* [listener](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/listener/)
* [manager](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/manager/)
* [repository](https://bitbucket.org/codeandcurious/com.tandeminnovation.android.appbase/src/master/appbase/src/main/java/com/tandeminnovation/appbase/repository/)

License
-------
    Copyright 2018 Tandem Innovation. All rights reserved.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.


